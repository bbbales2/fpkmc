#!/usr/bin/env python
import sys, math, re
import numpy

if len(sys.argv) != 3:
    print("Arguments: ./convertToReal.py input.txt output.txt")
    exit()

try:
    filein = open(sys.argv[1], 'r')
except IOError as e:
    print("Problem opening '" + sys.argv[1] + "'")
    exit()

deposits = []
line = filein.readline()

try:
    N = int(line.split()[0])
    grid = line.split()[1]
except Exception as e:
    print( "Problem parsing header")
    exit()

chemicals = ['H', 'O', 'N', 'C', 'S', 'P']

associationToChemicalMap = {}
sizes = {}

try:
    for line in filein.readlines():
        sline = line.split()
        
        if(len(sline) != 6):
            raise Exception
        
        [n, m, l, a, t, c] = [int(sline[0]), int(sline[1]), int(sline[2]), int(sline[3]), sline[4], int(sline[5])]

        if not a in associationToChemicalMap:
            sizes[a] = 1

            associationToChemicalMap[a] = chemicals[a % len(chemicals)]
        else:
            sizes[a] = sizes[a] + 1

        deposits.append([n, m, l, a, t, c]);
except Exception as e:
    print( "Problem parsing file body")
    exit()

filein.close()

try:
    fileout = open(sys.argv[2], 'w')
except IOError as e:
    print("Problem opening '" + sys.argv[2] + "'")
    exit()

fileout.write("CRYST1    4.085    4.085    4.085  90.00  90.00  90.00 Fm-3m           1\n")
fileout.write("         1         2         3         4         5         6         7         8\n")
fileout.write("12345678901234567890123456789012345678901234567890123456789012345678901234567890\n")
#fileout.write('Something\n')

max_height = 0

ii = 0
for e in deposits:
    ii = ii+1
    [xoff, yoff] = [0, 0] if (e[2] % 2 == 0) else [-0.5000, 0.2887]
    
    x = ((xoff + e[0] + e[1] * 1.0 / 2.0) % N)
    y = (yoff + -e[1] * math.sqrt(3) / 2)
    z = (e[2] * math.sqrt(6) / 3)
    max_height = max(e[2], max_height)

#	         	    1         2         3         4         5         6         7         8
#		   12345678901234567890123456789012345678901234567890123456789012345678901234567890
    fileout.write("ATOM{0:7d}{1:>4s}      X   1    {2:>8.3f}{3:>8.3f}{4:>8.3f}{5:>6.2f}{6:>6.2f}{7:>8d}  {8:>2s}\n".format(ii, 'Ag', x, y, z, 0.0, 0.0,e[3],e[4] ))

fileout.close()

