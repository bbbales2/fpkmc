NAME := cstoch

TRIANGLE_RUN_MODULES := src/ src/triangle src/triangle/run include
TRIANGLE_TEST_MODULES := src/ src/triangle src/triangle/test include

TRIANGLE_RUN_OBJS := $(call MAKEOBJS, $(TRIANGLE_RUN_MODULES))
TRIANGLE_TEST_OBJS := $(call MAKEOBJS, $(TRIANGLE_TEST_MODULES))
TRIANGLE_DEPS := $(call MAKEDEPSINC, $(TRIANGLE_RUN_OBJS))

triangle : triangle_run triangle_test

# link the program
triangle_run : $(TRIANGLE_RUN_OBJS)
	$(LINK) $(LFLAGS) -o $@ $^ $(LIBS)

triangle_test : $(TRIANGLE_TEST_OBJS)
	$(LINK) $(LFLAGS) -o $@ $^ $(LIBS)

triangle_clean:
	-@rm -f $(TRIANGLE_RUN_OBJS) $(TRIANGLE_TEST_OBJS) $(TRIANGLE_DEPS) triangle_run triangle_test

INCLUDES += $(call MAKEINCLUDES, $(TRIANGLE_TEST_MODULES) $(TRIANGLE_RUN_MODULES))
DEPS += $(TRIANGLE_DEPS)
clean : triangle_clean
.PHONY : triangle_clean triangle
all : triangle
test : triangle_test
