#!/usr/bin/env python

import subprocess, os

file = open('R', 'r')
R = int(file.read())
file.close()

file = open('frames', 'r')
frames = int(file.read())
file.close()

for r in range(0, R):
    os.chdir('run.{0}'.format(r))

    frame0 = open('frame.0.dat', 'r')
    frame0.readline()
    frame0inc = open('frame.0.inc', 'w')
    frame0inc.write(frame0.read())
    frame0.close()
    frame0inc.close()

    for f in range(0, frames - 1):
        cmd = 'fgrep -x -f frame.{0}.dat -v frame.{1}.dat'.format(f, f + 1)
        fhandle = open('frame.{0}.inc'.format(f + 1), 'w')
        handle = subprocess.Popen('fgrep -x -f frame.{0}.dat -v frame.{1}.dat'.format(f, f + 1).split(), stdout = fhandle)
        handle.wait()
        fhandle.close()

    for f in range(0, frames):
        os.remove('frame.{0}.dat'.format(f))

    os.chdir('../')

