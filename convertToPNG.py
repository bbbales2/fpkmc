#!/usr/bin/env python
import sys, math, re, os,numpy, select, subprocess, StringIO, tempfile

def printTacHeader(N):
    output = StringIO.StringIO()

    output.write(
        """
BEGIN_SCENE
	RESOLUTION 1024 768
CAMERA
	ZOOM 1
	ASPECTRATIO 1.0
	ANTIALIASING 0
	RAYDEPTH 20
	CENTER {3} -{10} {4}
	VIEWDIR 0.0 0.0 -1
	UPDIR 0.0 -1.0 0.0
END_CAMERA
BACKGROUND 0 0 0
LIGHT
	CENTER 0 -{4} {6}
	RAD 1.0
	COLOR 1.0 1.0 1.0

TEXDEF T0
	AMBIENT {0}
	DIFFUSE {1}
	SPECULAR {2}
	OPACITY 1.0
	PHONG PLASTIC {2} PHONG_SIZE 45
	COLOR 0.89412 0.10196 0.10980
	TEXFUNC 0

TEXDEF T1
	AMBIENT {0}
	DIFFUSE {1}
	SPECULAR {2}
	OPACITY 1.0
	PHONG PLASTIC {2} PHONG_SIZE 45
	COLOR 0.21569   0.49412   0.72157
	TEXFUNC 0

TEXDEF T2
	AMBIENT {0}
	DIFFUSE {1}
	SPECULAR {2}
	OPACITY 1.0
	PHONG PLASTIC {2} PHONG_SIZE 45
	COLOR 0.30196   0.68627   0.29020
	TEXFUNC 0

TEXDEF T3
	AMBIENT {0}
	DIFFUSE {1}
	SPECULAR {2}
	OPACITY 1.0
	PHONG PLASTIC {2} PHONG_SIZE 45
	COLOR 0.59608   0.30588   0.63922
	TEXFUNC 0

TEXDEF T4
	AMBIENT {0}
	DIFFUSE {1}
	SPECULAR {2}
	OPACITY 1.0
	PHONG PLASTIC {2} PHONG_SIZE 45
	COLOR 1.00000   0.49804   0.00000
	TEXFUNC 0

TEXDEF T5
	AMBIENT {0}
	DIFFUSE {1}
	SPECULAR {2}
	OPACITY 1.0
	PHONG PLASTIC {2} PHONG_SIZE 45
	COLOR 1.00000   1.00000   0.20000
	TEXFUNC 0

TEXDEF T6
	AMBIENT {0}
	DIFFUSE {1}
	SPECULAR {2}
	OPACITY 1.0
	PHONG PLASTIC {2} PHONG_SIZE 45
	COLOR 0.65098   0.33725   0.15686
	TEXFUNC 0

TEXDEF T7
	AMBIENT {0}
	DIFFUSE {1}
	SPECULAR {2}
	OPACITY 1.0
	PHONG PLASTIC {2} PHONG_SIZE 45
	COLOR 0.96863   0.50588   0.74902
	TEXFUNC 0

TEXDEF T8
	AMBIENT {0}
	DIFFUSE {1}
	SPECULAR {2}
	OPACITY 1.0
	PHONG PLASTIC {2} PHONG_SIZE 45
	COLOR 0.60000   0.60000   0.60000
	TEXFUNC 0

TEXDEF WALL
	AMBIENT {0}
	DIFFUSE {1}
	SPECULAR {2}
	OPACITY 0.5
	PHONG PLASTIC {2} PHONG_SIZE 45
	COLOR 0.80000   0.80000   0.80000
	TEXFUNC 0

BOX
	MIN 0 -{7} 0
	MAX -1 0 2.0
	WALL

BOX
	MIN 0 -{7} 0
	MAX {5} -{8} 2.0
	WALL

BOX
	MIN {6} -{7} 0
	MAX {5} 0 2.0
	WALL

BOX
	MIN 0 1.0 0.0
	MAX {5} 0 2.0
	WALL

""".format(0.3,
           0.5,
           0.2,
           N / 2,
           2 * N / 2,
           N,
           N + 1,
           N * math.sqrt(3) / 2,
           N * math.sqrt(3) / 2 + 1,
           N / 4,
           N * math.sqrt(3) / 4))


    return output.getvalue()

associations = dict()
    
def convertLinesToTac(N, text):
    deposits = []

    output = StringIO.StringIO()

    try:
        for line in text.split('\n'):
            sline = line.split()
        
            if(len(sline) != 6):
                continue
        
            [n, m, l, a, t, c] = [int(sline[0]), int(sline[1]), int(sline[2]), int(sline[3]), sline[4], int(sline[5])]

            if a in associations:
                pass
            else:
                associations[a] = len(associations)

            deposits.append([n, m, l, associations[a], t, c]);
    except:
        print "error parsing file"
        raise


    max_height = 0
    ii = 0
    for e in deposits:
        ii = ii+1
        [xoff, yoff] = [0, 0] if (e[2] % 2 == 0) else [-0.5000, 0.2887]
        
        x = ((xoff + e[0] + e[1] * 1.0 / 2.0) % N)
        y = (yoff + -e[1] * math.sqrt(3) / 2)
        z = (e[2] * math.sqrt(6) / 3)
        max_height = max(e[2], max_height)

        output.write("SPHERE CENTER {0:8.3f} {1:8.3f} {2:8.3f} RAD 0.5 T{3:d}\n".format(x, y, z, e[3] % 9))

    return output.getvalue()

if None:
#select.select([sys.stdin,], [], [], 0.0)[0]:
#    if len(sys.argv) < 2:
#        raise Exception("Not enough arguments. Need ./convertToPNG.py N")

#    N = int(sys.argv[1])

#    sys.stdout.write(printTacHeader(N) + convertLinesToTac(N, sys.stdin.read()))
    pass
else:
    try:
        file = open('N', 'r')
        N = int(file.read())
        file.close()
    except:
        N = 128
        pass

    file = open('R', 'r')
    R = int(file.read())
    file.close()
    
    file = open('frames', 'r')
    frames = int(file.read())
    file.close()

    for r in range(0, R):
        os.chdir('run.{0}'.format(r))

        renderString = printTacHeader(N)
        for f in range(0, frames):
            fhandle = open('frame.{0}.inc'.format(f), 'r')
            renderString = renderString + convertLinesToTac(N, fhandle.read())
            fhandle.close()
            
            [fnumber, fname] = tempfile.mkstemp()
            fhandle = open(fname, 'w')
            fhandle.write(renderString)
            fhandle.close()
        
            subprocess.call('tachyon -format PNG {0} -o association.{1}.png'.format(fname, f).split(), stdin = subprocess.PIPE, stdout = subprocess.PIPE)

            os.remove(fname)

        os.chdir('../')
