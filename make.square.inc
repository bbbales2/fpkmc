SQUARE_RUN_MODULES := src/ src/square src/square/run include
SQUARE_TEST_MODULES := src/ src/square src/square/test include

SQUARE_RUN_OBJS := $(call MAKEOBJS, $(SQUARE_RUN_MODULES))
SQUARE_TEST_OBJS := $(call MAKEOBJS, $(SQUARE_TEST_MODULES))
SQUARE_DEPS := $(call MAKEDEPSINC, $(SQUARE_RUN_OBJS))

square : square_run square_test

square_run : $(SQUARE_RUN_OBJS)
	     $(LINK) $(LFLAGS) -o $@ $^ $(LIBS)

square_test : $(SQUARE_TEST_OBJS)
	     $(LINK) $(LFLAGS) -o $@ $^ $(LIBS)

square_clean :
	-@rm -f $(SQUARE_TEST_OBJS) $(SQUARE_RUN_OBJS) $(SQUARE_DEPS) square_test square_run

INCLUDES += $(call MAKEINCLUDES, $(SQUARE_TEST_MODULES) $(SQUARE_RUN_MODULES))
DEPS += $(SQUARE_DEPS)
clean : square_clean
.PHONY : square_clean square
all : square
test : square_test