#!/usr/bin/env python

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot

import os, numpy, math

a1 = numpy.array([1, 0, 0]);
a2 = numpy.array([1 / 2, -math.sqrt(3) / 2, 0]);
a3 = numpy.array([1 / 2, -0.2887, math.sqrt(8 / 3) / 2]);

b1 = 2 * math.pi * numpy.cross(a2, a3) / numpy.dot(a1, numpy.cross(a2, a3));
b2 = 2 * math.pi * numpy.cross(a3, a1) / numpy.dot(a1, numpy.cross(a2, a3));
b3 = 2 * math.pi * numpy.cross(a1, a2) / numpy.dot(a1, numpy.cross(a2, a3));

#file = open('N', 'r')
N = 128#int(file.read())
#file.close()

file = open('R', 'r')
R = int(file.read())
file.close()

file = open('frames', 'r')
frames = int(file.read())
file.close()

file = open('kspace', 'r')
kspace = []
for line in file.readlines():
    kspace.append(map(lambda x : float(x), line.split()))
file.close()

for r in range(0, R):
    os.chdir('run.{0}'.format(r))

    sums = numpy.zeros((len(kspace)))

    running_sums = []
    for f in range(0, frames):
        handle = open('frame.{0}.inc'.format(f), 'r')

        coords = []
        for line in handle.readlines():
            n = map(lambda (x) : float(x), line.split()[0:3])
            coords.append(numpy.array(n[0] * a1 + n[1] * a2 + n[2] * a3))

        coords = numpy.matrix(coords)

        new_sums = []
        for k, old_sum in zip(kspace, sums):
            new_sums.append(sum(numpy.exp(coords * numpy.transpose(numpy.matrix(k[0] * b1 + k[1] * b2 + k[2] * b3)) * 1j))[0, 0] + old_sum)

        running_sums.append(new_sums)
        sums = new_sums
        #sums = sums + map(lambda k : sum(numpy.exp(coords * numpy.transpose(numpy.matrix(k * b3)) * 1j))[0, 0], kspace)


        #print values

    try:
        os.mkdir('xrays')
    except:
        pass

    running_sums_by_k = zip(*running_sums)
    for i, (sums, k) in enumerate(zip(running_sums_by_k, kspace)):
        values = numpy.real(numpy.conj(sums) * sums)
        matplotlib.pyplot.plot(values)
        matplotlib.pyplot.xlabel('frames')
        matplotlib.pyplot.ylabel('')
        matplotlib.pyplot.title('k = ({0}, {1}, {2})'.format(k[0], k[1], k[2]))
        #matplotlib.pyplot.ylim(0, max(values) / 10.0)
        matplotlib.pyplot.savefig('xrays/xray.{0}.png'.format(i), format='png')
        matplotlib.pyplot.clf()

    os.chdir('../')
