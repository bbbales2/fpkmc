
# edit PATH to include folders with "convert", "ffmpeg" and "ffmpeg2theora
#exec /usr/bin/env PATH=/usr/local/bin:${PATH}

# some helper programs

# extract numbers from string
# #########################################################
proc extract_numbers s {regsub -all {[^0-9]} $s ""}
###########################################################
proc stripzeros {value} {
        regsub ^0+(.+) $value \\1 retval
        return $retval
}

# color pdb file by segname
# ################################################################################
proc colorBySegname  {molID} {
    set atoms [atomselect $molID "all"]						
    set segNames [$atoms get segname]
    set Zpos [$atoms get z]
    
    #set maxZ [lindex [ lsort -decreasing $Zpos] 0]
    set maxZ 0
    
    foreach zpos [$atoms get z] {
        set maxZ [expr max($maxZ, $zpos)]
    }

    #puts $segNames

    set segNamess {}
    foreach seg $segNames {
        lappend segNamess [stripzeros $seg]
    }

    set seglist [lsort -integer -unique -decreasing $segNamess]

    puts 2
    mol modcolor 0 $molID PosZ
    puts 3
    mol scaleminmax $molID 0 0.0 $maxZ

    foreach seg $seglist {
	set cID [expr { fmod($seg,32) }]
	mol selection "segname $seg"
	mol color colorID $cID
	mol addrep $molID
	mol modstyle [expr {[molinfo $molID get numreps] -1} ] $molID vdw 0.3 12.0
    }
}
###################################################################################


set curdir [pwd]
# puts $curdir

echo [glob *.pdb]

foreach f [glob *.pdb] {
    set fileNum [ format {%04d} [extract_numbers $f] ]
    
    # add in new molecule
    mol new $curdir/$f 
    # get mol ID number
    set molID [molinfo top]
    # draw VDW style, colored by height
    mol modstyle 0 $molID vdw 0.3 12
    mol modcolor 0 $molID PosZ
    
    # get some information on size of plot
    set atoms [atomselect $molID "all"]
    set maxX [lindex [ lsort -decreasing [ $atoms get x ] ] 0]
    set maxY [lindex [ lsort -decreasing [ $atoms get y ] ] 0]
    set maxZ [lindex [ lsort -decreasing [ $atoms get z ] ] 0]
    
    # add information on max height
    set labelX 0
    set labelY [ expr {-$maxY/5} ] 
    set labelZ 0

    # add label with max height
    set labelCoord [list $labelX $labelY $labelZ]
    set title "Max. Height [expr { round($maxZ) } ] (in AU)"
    draw text $labelCoord  $title size 2 thickness 2

    # make it pretty and render to file
    axes location off
    scale by 1.7
    render TachyonInternal height$fileNum.tga convert %s height$fileNum.jpg
    # remove text labels
    draw delete all

    # color by segname
    colorBySegname $molID
    # remove height representation
    mol delrep 0 $molID
    
    # get number of grains
    set Ngrains [llength [lsort -unique [ [atomselect $molID "all"] get segname] ] ]
    set title "# of grains: $Ngrains"
    draw text $labelCoord $title size 2 thickness 2
    # render to file
    render TachyonInternal grain$fileNum.tga convert %s grain$fileNum.jpg

    # combine both figures to one (side by side)
    convert height$fileNum.jpg grain$fileNum.jpg +append -quality 75 combo$fileNum.jpg

    # remove text labels
    draw delete all

    # remove molecule
    mol delete $molID
}

# mp4 video with h264 encoding
ffmpeg -y -f image2 -r 2 -i height%04d.jpg -vcodec libx264 -acodec libfaac videoHeight.mp4
ffmpeg -y -f image2 -r 2 -i grain%04d.jpg -vcodec libx264 -acodec libfaac videoGrain.mp4
ffmpeg -y -f image2 -r 2 -i combo%04d.jpg -vcodec libx264 -acodec libfaac videoCombo.mp4


# ogg video 
ffmpeg2theora -o videoGrain.ogv videoGrain.mp4
ffmpeg2theora -o videoHeight.ogv videoHeight.mp4
ffmpeg2theora -o videoCombo.ogv videoCombo.mp4

exit
