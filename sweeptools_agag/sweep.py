#!/usr/bin/env python

import sys, bisect, os, tempfile, subprocess, multiprocessing, pickle, itertools, collections, struct, json

if os.path.isfile('diffusionsweep'):
    pfile = open('diffusionsweep')
    (N, C, frames, runs, DAgArray, FonMAgArray, DedgeAgArray, DdownAgArray, keysToResultsFiles) = pickle.loads(pfile.read())
    pfile.close()
else:
    N = 128
    C = 20
    frames = 200
    runs = 1

    DAgArray = dict(enumerate([1e0]))
    FonMAgArray = dict(enumerate([1e-8]))#, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8, 1e-9]))
    DedgeAgArray = dict(enumerate([1e-2]))
    DdownAgArray = dict(enumerate([1e0]))

    keysToResultsFiles = {}

# Do first sweep to get everything inserted into the constituent maps
def insertByValue(dictionary, values):
    for value in values:
        found = False

        for pair in dictionary.items():
            if pair[1] == value:
                found = True
                break

        if found != True:
            dictionary[len(dictionary)] = value

#insertByValue(DAgArray, [1e10 1e9 1e8 1e7 1e6 1e5 1e4])
#insertByValue(FonMAgArray, [1e2 1e3 1e4 1e5 1e6])
#insertByValue(DedgeAgArray, [1e2 1e3 1e4 1e5 1e6])
#insertByValue(DdownAgArray, [1e3 1e10])

#insertByValue(DAgArray, [1e13, 1e12, 1e11, 1e10, 1e9, 1e8])
#DAuArray = [1e13, 1e12, 1e11, 1e10, 1e9, 1e8]
#FonMAgArray = [0]
#FonSiAgArray = [1e2, 1e3]
#FonMAuArray = [0]
#FonSiAuArray = [1e2, 1e3]
#DedgeArray = [1e11, 1e10, 1e9, 1e8, 1e7, 1e6]
#DedgeAuArray = [1e11, 1e10, 1e9, 1e8, 1e7, 1e6]
#DdownArray = [1e10]

try:
    os.remove('diffusionsweep.db')
except Exception as e:
    pass

try:
    os.mkdir('outdata')
except Exception:
    pass

try:
    os.mkdir('xyz')
except Exception:
    pass

try:
    os.mkdir('images')
except Exception:
    pass

try:
    os.mkdir('stats')
except Exception:
    pass

for (DAg, FonMAg, DedgeAg, DdownAg) in itertools.product(DAgArray, FonMAgArray, DedgeAgArray, DdownAgArray):
    # Look up the result filename of this configuration of input parameters
    key = struct.pack('iiiiiiiiii', DAg, 0, FonMAg, FonMAg, 0, 0, DedgeAg, 0, DdownAg, 0)
    if key in keysToResultsFiles:
        pass
    else:
        keysToResultsFiles[key] = len(keysToResultsFiles)

pfile = open('diffusionsweep', 'w')
pfile.write(pickle.dumps((N, C, frames, runs, DAgArray, FonMAgArray, DedgeAgArray, DdownAgArray, keysToResultsFiles)))
pfile.close()

for (DAg, FonMAg, DedgeAg, DdownAg) in itertools.product(DAgArray, FonMAgArray, DedgeAgArray, DdownAgArray):
    jsonData = { "N" : N,
                 "C" : C,
                 "DAg" : DAgArray[DAg],
                 "FonMAg" : FonMAgArray[FonMAg],
                 "FonSiAg" : FonMAgArray[FonMAg],
                 "DedgeAg" : DedgeAgArray[DedgeAg],
                 "DdownAg" : DdownAgArray[DdownAg],
                 "runs" : runs,
                 "frames" : frames };

    # Look up the result filename of this configuration of input parameters
    key = struct.pack('iiiiiiiiii', DAg, 0, FonMAg, FonMAg, 0, 0, DedgeAg, 0, DdownAg, 0)
    if os.path.isdir('output/' + str(keysToResultsFiles[key])):
        # Presumably we should run some sort of verification code here to guarantee the run was successful + parameterized correctly, etc.
        pass
    else:
        jsonData["filename"] = 'output/' + str(keysToResultsFiles[key])
        print json.dumps(jsonData)
 
