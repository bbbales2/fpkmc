#!/usr/bin/env python

import matplotlib
matplotlib.use('Agg')
import sys, os, itertools, tempfile, subprocess, shutil, json, struct, math, time
import xray

import matplotlib.pyplot as plt

blanks = False
if len(sys.argv) > 2:
    blanks = True

jsonData = json.loads(sys.argv[len(sys.argv) - 1])

N = jsonData["N"]
C = jsonData["C"]
DAg = jsonData["DAg"]
DAu = 0
FonMAg = jsonData["FonMAg"]
FonSiAg = jsonData["FonSiAg"]
FonMAu = 0
FonSiAu = 0
DedgeAg = jsonData["DedgeAg"]
DedgeAu = 0
DdownAg = jsonData["DdownAg"]
DdownAu = 0
runs = jsonData["runs"]
frames = jsonData["frames"]
resultFolder = jsonData["filename"]

tempdir = tempfile.mktemp()

# LATTICE SPACING
a0 = 4.08e-10;

process = "../triangle_run --C {0} -n {1} --frames {2} -r {3} --D_Ag {4} --FonM_Ag {5} --FonSi_Ag {6} --Dedge_Ag {7} --Ddown_Ag {8} --D_Au {9} --FonM_Au {10} --FonSi_Au {11} --Dedge_Au {12} --Ddown_Au {13} {14}".format(C, N, frames, runs, DAg, FonMAg, FonSiAg, DedgeAg, DdownAg, DAu, FonMAu, FonSiAu, DedgeAu, DdownAu, tempdir)

if blanks:
    print process
    exit(0)

pipes = subprocess.Popen(process.split(' '), stdout =  subprocess.PIPE, stdin = subprocess.PIPE, stderr = subprocess.PIPE);

pipes.wait()

#asdfasdfasdfasdf
vmdscript = "mol modstyle 0 0 VDW\nscale by 1.2\naxes location off\nrender TachyonInternal {0} convert %s {1}\nquit"

originWorkingDirectory = os.getcwd()
os.chdir(tempdir);

rFile = open(tempdir + "/R")
R = int(rFile.read())
rFile.close()

try:
    os.mkdir("xrays")
except:
    pass

subprocess.call("tar -czf out.tgz *", shell = True)

os.makedirs(originWorkingDirectory + "/" + resultFolder)

shutil.move("out.tgz", originWorkingDirectory + "/" + resultFolder)

os.chdir(originWorkingDirectory)

shutil.rmtree(tempdir)

exit(0)

import time

def wait_timeout(proc, seconds):
    """Wait for a process to finish, or raise exception after timeout"""
    start = time.time()
    end = start + seconds
    interval = min(seconds / 1000.0, .25)

    while True:
        result = proc.poll()
        if result is not None:
            return result
        if time.time() >= end:
            proc.kill()
        time.sleep(interval)

for r in range(0, R):
    os.chdir("run." + str(r));

    xrayFileName = 'xrays/xray.{0}.png'.format(r)
    xrayVals = []
    for f in range(0, frames):
        fname1 = "frame." + str(f) + ".dat"
        fname2 = 'frame.{0}.pdb'.format(f)
        
        subprocess.call('{0}/../convertToPDB.py {1} {2}'.format(originWorkingDirectory, fname1, fname2), shell = True)

        xrayVals.append(xray.computeFromFile(fname1, (1, 1, 0.1)))


    vmdLog = open("vmd.log", "w")
    vmderrLog = open("vmderr.log", "w")

    vmd = subprocess.Popen("vmd -dispdev text -e {0}/doit.tcl".format(originWorkingDirectory).split(" "), stdin = subprocess.PIPE, stdout = vmdLog, stderr = vmderrLog);

    wait_timeout(vmd, frames * 30)
    vmdLog.close()
    vmderrLog.close()


    vmd.wait()

    os.chdir("../")

    plt.plot(xrayVals)
    plt.xlabel('Frame #');
    plt.ylabel('I * Unknown Constant')
    plt.savefig(xrayFileName, format='png')

    plt.clf()    

try:
    os.mkdir("comps")
except Exception as e:
    pass

try:
    os.mkdir("dists")
except Exception as e:
    pass

class Atom:
    def __init__(self, x, y, z, assoc, element):
        self.x = x
        self.y = y
        self.z = z

        self.assoc = assoc
        self.element = element

def process(string):
    args = string.split()

    e = [int(args[0]), int(args[1]), int(args[2])]

    [xoff, yoff] = [0, 0] if (e[2] % 2 == 0) else [-0.5000, 0.2887]
    
    x = (xoff + e[0] + e[1] * 1.0 / 2.0)
    y = (yoff + -e[1] * math.sqrt(3) / 2)
    z = (e[2] * math.sqrt(6) / 3)
    #[xoff, yoff] = [0, 0] if (e[2] % 2 == 0) else [-1.0 / 2.0, -(1 - 1.0 / math.sqrt(3.0))]

    #x = (xoff + e[0] * 1.0 / 2.0 + e[1])
    #y = (yoff + e[0] * math.sqrt(3.0) / 2.0)
    #z = (e[2] * math.sqrt(6) / 3)

    a = int(args[3])
    t = args[4]

    return Atom(x, y, z, a, t)

for f in range(0, frames):
    distances = []
    percentsAg = []

    for r in range(0, R):
        try:
            handle = open('run.{0}/frame.{1}.dat'.format(r, f), 'r');

            args = handle.readline()

            N = int(args.split()[0])

            atomslist = map(process, handle)

            atomsByAssociation = {}

            for atom in atomslist:
                if not atom.assoc in atomsByAssociation:
                    atomsByAssociation[atom.assoc] = list()

                atomsByAssociation[atom.assoc].append(atom)

            comByAssociation = {}
            silverAtomsByAssociation = {}
            totalAtomsByAssociation = {}
            for association in atomsByAssociation:
                comxx = 0;
                comyx = 0;

                comxy = 0;
                comyy = 0;

                comxz = 0;
                comyz = 0;

                atoms = atomsByAssociation[association]

                total = 0
                silvers = 0
                for atom in atoms:
                    comxx += N * math.sin(atom.x * 2 * math.pi / N)
                    comyx += N * math.cos(atom.x * 2 * math.pi / N)

                    comxy += N * math.sin(atom.y * 2 * math.pi / N)
                    comyy += N * math.cos(atom.y * 2 * math.pi / N)
                    
                    comxz += N * math.sin(atom.z * 2 * math.pi / N)
                    comyz += N * math.cos(atom.z * 2 * math.pi / N)

                    if atom.element == 'Ag':
                        silvers += 1

                    total += 1

                comxx /= len(atoms)
                comyx /= len(atoms)

                comxy /= len(atoms)
                comyy /= len(atoms)

                comxz /= len(atoms)
                comyz /= len(atoms)

                comx = N * (math.atan2(-comxx, -comyx) / math.pi + 1) / 2
                comy = N * (math.atan2(-comxy, -comyy) / math.pi + 1) / 2
                comz = N * (math.atan2(-comxz, -comyz) / math.pi + 1) / 2
                
                comByAssociation[association] = [comx, comy, comz]
                silverAtomsByAssociation[association] = silvers
                totalAtomsByAssociation[association] = total
                
            com1iter = comByAssociation.__iter__()

            distanceByAssociation = {}

            for (key1, key2) in itertools.permutations(comByAssociation, 2):
                comfirst = comByAssociation[key1]
                comsecond = comByAssociation[key2]
            
                def getDistance(n1, n2, N):
                    if n2 > n1:
                        if (n2 - n1) < (N - n2 + n1):
                            return n1 - n2;
                        else:
                            return (N - n2 + n1);
                    else:
                        if (n1 - n2) < (N - n1 + n2):
                            return n1 - n2;
                        else:
                            return -(N - n1 + n2);
                    
                tmp = math.sqrt(getDistance(comfirst[0], comsecond[0], N) * getDistance(comfirst[0], comsecond[0], N) +
                                getDistance(comfirst[1], comsecond[1], N) * getDistance(comfirst[1], comsecond[1], N) +
                                getDistance(comfirst[2], comsecond[2], N) * getDistance(comfirst[2], comsecond[2], N))

                try:
                    distanceByAssociation[key1] = min(distanceByAssociation[key1], tmp)
                except Exception:
                    distanceByAssociation[key1] = tmp


            distances.extend(distanceByAssociation.values())
            percentsAg.extend(map(lambda x, y : float(x) / float(y), silverAtomsByAssociation.values(), totalAtomsByAssociation.values()))

            handle.close()
        except Exception as e:
            print e
            
            raise

    if len(distances) == 0:
        distances.append(0)

    plt.hist(distances, 41, histtype = 'step')
    plt.xlabel('Distance between center of masses (lattice spacings)');
    plt.ylabel('Count')
    ymin, ymax = plt.ylim()
    plt.ylim(0, ymax)
    plt.xlim(0, N / math.sqrt(2))    
    plt.savefig('dists/dist.{0}.png'.format(f), format='png')

    plt.clf()

    plt.hist(percentsAg, 41, histtype = 'step')
    plt.xlabel('% of particles in deposit that are Ag');
    plt.ylabel('Count')
    ymin, ymax = plt.ylim()
    plt.ylim(0, ymax)
    plt.xlim(0, 1)
    plt.savefig('comps/comp.{0}.png'.format(f), format='png')

    plt.clf()

subprocess.call("tar -czf out.tgz *", shell = True)

os.makedirs(originWorkingDirectory + "/" + resultFolder)

shutil.move("out.tgz", originWorkingDirectory + "/" + resultFolder)

os.chdir(originWorkingDirectory)

shutil.rmtree(tempdir)

