SQUARE2D_RUN_MODULES := src/ src/square2d src/square2d/run include
SQUARE2D_TEST_MODULES := src/ src/square2d src/square2d/test include

SQUARE2D_RUN_OBJS := $(call MAKEOBJS, $(SQUARE2D_RUN_MODULES))
SQUARE2D_TEST_OBJS := $(call MAKEOBJS, $(SQUARE2D_TEST_MODULES))
SQUARE2D_DEPS := $(call MAKEDEPSINC, $(SQUARE2D_TEST_OBJS) $(SQUARE2D_RUN_OBJS))

square2d : square2d_run square2d_test

square2d_run : $(SQUARE2D_RUN_OBJS)
	$(LINK) $(LFLAGS) -o $@ $^ $(LIBS)

square2d_test : $(SQUARE2D_TEST_OBJS)
	$(LINK) $(LFLAGS) -o $@ $^ $(LIBS)

square2d_clean :
	-@rm -f $(SQUARE2D_TEST_OBJS) $(SQUARE2D_RUN_OBJS) $(SQUARE2D_DEPS) square2d_test square2d_run

INCLUDES += $(call MAKEINCLUDES, $(SQUARE2D_TEST_MODULES) $(SQUARE2D_RUN_MODULES))
DEPS += $(SQUARE2D_DEPS)
clean : square2d_clean
.PHONY : square2d_clean square2d
all : square2d
test : square2d_test