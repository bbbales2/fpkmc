TRIANGLE2D_RUN_MODULES := src/ src/triangle2d src/triangle2d/run include
TRIANGLE2D_TEST_MODULES := src/ src/triangle2d src/triangle2d/test include
TRIANGLE2D_TESTCDF_MODULES := src/ src/triangle2d src/triangle2d/testcdf include

TRIANGLE2D_RUN_OBJS := $(call MAKEOBJS, $(TRIANGLE2D_RUN_MODULES))
TRIANGLE2D_TEST_OBJS := $(call MAKEOBJS, $(TRIANGLE2D_TEST_MODULES))
TRIANGLE2D_TESTCDF_OBJS := $(call MAKEOBJS, $(TRIANGLE2D_TESTCDF_MODULES))
TRIANGLE2D_DEPS := $(call MAKEDEPSINC, $(TRIANGLE2D_RUN_OBJS) $(TRIANGLE2D_TEST_OBJS) $(TRIANGLE2D_TESTCDF_OBJS))

triangle2d : triangle2d_run triangle2d_test triangle2d_testcdf

triangle2d_run : $(TRIANGLE2D_RUN_OBJS)
	$(LINK) $(LFLAGS) -o $@ $^ $(LIBS)

triangle2d_test : $(TRIANGLE2D_TEST_OBJS)
	$(LINK) $(LFLAGS) -o $@ $^ $(LIBS)

triangle2d_testcdf : $(TRIANGLE2D_TESTCDF_OBJS)
	$(LINK) $(LFLAGS) -o $@ $^ $(LIBS)

triangle2d_clean:
	-@rm -f $(TRIANGLE2D_RUN_OBJS) $(TRIANGLE2D_TEST_OBJS) $(TRIANGLE2D_TESTCDF_OBJS) $(TRIANGLE2D_DEPS) triangle2d_run triangle2d_test

INCLUDES += $(call MAKEINCLUDES, $(TRIANGLE2D_TEST_MODULES) $(TRIANGLE2D_TESTCDF_MODULES) $(TRIANGLE2D_RUN_MODULES))
DEPS += $(TRIANGLE2D_DEPS)
clean : triangle2d_clean
.PHONY : triangle2d_clean triangle2d
all : triangle2d
test : triangle2d_test triangle2d_testcdf
