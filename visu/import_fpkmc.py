# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

#
#
#  Authors           : Clemens Barth (Blendphys@root-1.de), ...
#
#  Homepage(Wiki)    : http://development.root-1.de/Atomic_Blender.php
#  Tracker           : ... soon
#
#  Start of project              : 2011-12-01 by Clemens Barth
#  First publication in Blender  : 2011-12-18
#  Last modified                 : 2012-06-07
#
#  Acknowledgements: Thanks to ideasman, meta_androcto, truman, kilon,
#  dairin0d, PKHG, Valter, etc
#

bl_info = {
    "name": "FPKMC",
    "description": "Load FPKMC data",
    "author": "Ben",
    "version": (0.0),
    "blender": (2,6),
    "location": "File -> Import -> FPKMC (.fpkmc)",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export"
}

import os
import io
import bpy
import bpy_extras
import copy

class Index:
    def __init__(self):
        self.index = [0.0, 0.0, 0.0]

    @classmethod
    def fromIndex(cls, arg):
        index = cls()
        index.index = copy.deepcopy(arg.index)
        return index

    def __hash__(self):
        return int(self.index[0] + self.index[1] + self.index[2])

    def __eq__(self, other):
        return self.index[0] == other.index[0] and \
            self.index[1] == other.index[1] and \
            self.index[2] == other.index[2]

    def __getattr__(self, string):
        if(string == "x"):
            return self.index[0]
        elif(string == "y"):
            return self.index[1]
        elif(string == "z"):
            return self.index[2]

        return None

    def __setattr__(self, string, value):
        if(string == "x"):
            self.index[0] = float(value)
        elif(string == "y"):
            self.index[1] = float(value)
        elif(string == "z"):
            self.index[2] = float(value)
        else:
            self.__dict__[string] = value

        return None

    def copy(self):
        return Index.fromIndex(self)

    def __repr__(self):
        return "( " + str(self.x) + " , " + str(self.y) + " , " + str(self.z) + " )"

class Action:
    t = 0.0

    def fromTokens(self, tokens):
        self.index = Index()
        self.index.x = float(tokens[0])
        self.index.y = float(tokens[1])
        self.index.z = float(tokens[2])

        self.index.x = (self.index.x + self.index.y / 2.0) % 32.0
        return tokens[3:]

    def __repr__(self):
        return repr(self.index)

def parse_line(string):
    tokens = string.split(" ")
    
    if tokens[1] == 'DEPOSIT':
        obj = Deposit.fromTokens(tokens[2 : ])
    elif tokens[1] == 'REACTION':
        obj = Reaction.fromTokens(tokens[2 :])
    elif tokens[1] == 'HOP':
        obj = Propagate.fromTokens(tokens[2 :])    
    elif tokens[1] == 'EDGE_PROPAGATE':
        obj = Propagate.fromTokens(tokens[2 :])    
    elif tokens[1] == 'EARLY_PROPAGATE':
        obj = Propagate.fromTokens(tokens[2 :])    
    elif tokens[1] == 'PROPAGATE':
        obj = Propagate.fromTokens(tokens[2 :])    
    else:
        print("ERROR")
        exit()
        #return None
    

    obj.t = float(tokens[0])

    return obj

class Reaction(Action):
    @classmethod
    def fromTokens(cls, tokens):
        reaction = cls()
        super(Reaction, reaction).fromTokens(tokens);
        return reaction

    def buildAtom(self):
        atom = Atom()
        atom.t = self.t
        atom.index = self.index.copy()

        bpy.ops.mesh.primitive_uv_sphere_add(size = (0.5), location = (atom.index.x, atom.index.y, atom.index.z))
        atom.obj = bpy.context.active_object
        bpy.ops.material.new()
        atom.obj.active_material = bpy.data.materials[-1]

        atom.obj.active_material.diffuse_intensity = 0.8
        atom.obj.active_material.keyframe_insert(data_path="diffuse_intensity", frame=0.0)

        atom.obj.layers[1] = True
        atom.obj.layers[0] = False
        atom.obj.keyframe_insert(data_path="layers", frame=0.0, index=0)
        atom.obj.keyframe_insert(data_path="layers", frame=0.0, index=1)
        atom.obj.location = atom.index.index
        atom.obj.keyframe_insert(data_path="location", frame=0.0, index=0)
        atom.obj.keyframe_insert(data_path="location", frame=0.0, index=1)
        atom.obj.keyframe_insert(data_path="location", frame=0.0, index=2)

        atom.obj.layers[0] = True
        atom.obj.layers[1] = False
        atom.obj.keyframe_insert(data_path="layers", frame=self.t, index=0)
        atom.obj.keyframe_insert(data_path="layers", frame=self.t, index=1)
        atom.obj.location = atom.index.index
        atom.obj.keyframe_insert(data_path="location", frame=self.t, index=0)
        atom.obj.keyframe_insert(data_path="location", frame=self.t, index=1)
        atom.obj.keyframe_insert(data_path="location", frame=self.t, index=2)

        return atom;

    def __repr__(self):
        return "Reaction at: " + repr(self.index) + " at t = " + str(self.t)

class Deposit(Action):
    @classmethod
    def fromTokens(cls, tokens):
        deposit = cls()
        super(Deposit, deposit).fromTokens(tokens);
        return deposit

    def __repr__(self):
        return "Deposit at: " + repr(self.index) + " at t = " + str(self.t)

class Propagate(Action):
    @classmethod
    def fromTokens(cls, tokens):
        propagate = cls()
        tokens = super(Propagate, propagate).fromTokens(tokens);

        propagate.output = Index()
        propagate.output.x = float(tokens[0])
        propagate.output.y = float(tokens[1])
        propagate.output.z = float(tokens[2])

        propagate.output.x = (propagate.output.x + propagate.output.y / 2.0) % 32.0
        return propagate

    def advance(self, atom):
        atom.t = self.t
        atom.index = self.output.copy()

        atom.obj.layers[0] = True
        atom.obj.layers[1] = False
        atom.obj.keyframe_insert(data_path="layers", frame=atom.t, index=0)
        atom.obj.keyframe_insert(data_path="layers", frame=atom.t, index=1)
        atom.obj.location = atom.index.index
        atom.obj.keyframe_insert(data_path="location", frame=atom.t, index=0)
        atom.obj.keyframe_insert(data_path="location", frame=atom.t, index=1)
        atom.obj.keyframe_insert(data_path="location", frame=atom.t, index=2)

    def __repr__(self):
        return "Propagate from: " + repr(self.index) + " to " + repr(self.output) + " at t = " + str(self.t)

#class EdgePropagate(Action):
#    dosomething

#class EarlyPropagate(Action):
#    dosomething

#class Hop(Action):
#    dosomething

class Atom:
    t = 0.0

# This is the class for the file dialog.
class io_import_fpkmc(bpy.types.Operator, bpy_extras.io_utils.ImportHelper):
    bl_idname = "io_import.fpkmc"
    bl_label  = "Import FPKMC (*.fpkmc)"
    bl_options = {'PRESET'}
    
    filename_ext = ".fpkmc"
    #filter_glob  = bpy.types.StringProperty(default="*.fpkmc")
    #, options={'HIDDEN'},

    def execute(self, context):
        
        # This is in order to solve this strange 'relative path' thing.
        filename = bpy.path.abspath(self.filepath)

        data_file = open(filename, "r");

        N = int(data_file.readline())

        actions = []
        for line in data_file:
            actions.append(parse_line(line))
        
        atoms = {}
        deposits = []
        for action in actions:
            if type(action) == Reaction:
                #print("Atom inserted at " + str(action.index))
                atoms[action.index] = action.buildAtom()
            else:
                #print("Accessing Atom at " + str(action.index))
                atom = atoms[action.index]

                del atoms[action.index]

                if type(action) == Deposit:
                    atom.t = action.t 
                    atom.obj.active_material.diffuse_intensity = 0.0
                    atom.obj.active_material.keyframe_insert(data_path="diffuse_intensity", frame=atom.t)
                    deposits.append(atom)
                else:
                    action.advance(atom)
                    #if type(action) == Propagate:
                    #    del atoms[action.index]
                    #elif type(action) == EdgePropagate:
                    #    dosomething
                    #elif type(action) == EarlyPropagate:
                    #    dosomething
                    #elif type(action) == Hop:
                    #    dosomething

                    atoms[atom.index] = atom

        for atom in atoms:
            for fcurve in atom.obj.animation_data.action.fcurves:
                for keyframe in fcurve.keyframe_points:
                    keyframe.interpolation = 'CONSTANT'

            for fcurve in atom.obj.active_material.animation_data.action.fcurves:
                for keyframe in fcurve.keyframe_points:
                    keyframe.interpolation = 'CONSTANT'
                    
        for atom in deposits:
            for fcurve in atom.obj.animation_data.action.fcurves:
                for keyframe in fcurve.keyframe_points:
                    keyframe.interpolation = 'CONSTANT'

            for fcurve in atom.obj.active_material.animation_data.action.fcurves:
                for keyframe in fcurve.keyframe_points:
                    keyframe.interpolation = 'CONSTANT'
                    
                #bpy.ops.graph.select_all_toggle(invert=False)       
        #bpy.ops.graph.interpolation_type(type='CONSTANT')

        data_file.close()

        return {'FINISHED'}
        

# The entry into the menu 'file -> import'
def menu_func(self, context):
    self.layout.operator(io_import_fpkmc.bl_idname, text="FPKMC (.fpkmc)")

def register():
    bpy.utils.register_class(io_import_fpkmc)
    bpy.types.INFO_MT_file_import.append(menu_func)
    
def unregister():
    bpy.utils.unregister_class(io_import_fpkmc)
    bpy.types.INFO_MT_file_import.remove(menu_func)

if __name__ == "__main__":
    register()
