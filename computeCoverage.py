#!/usr/bin/env python

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot
import sys, math, re, os,numpy, select, subprocess, StringIO, tempfile

associations = dict()
    
def readAtoms(N, text):
    deposits = []

    output = StringIO.StringIO()

    try:
        for line in text.split('\n'):
            sline = line.split()
        
            if(len(sline) != 6):
                continue
        
            [n, m, l, a, t, c] = [int(sline[0]), int(sline[1]), int(sline[2]), int(sline[3]), sline[4], int(sline[5])]

            if a in associations:
                pass
            else:
                associations[a] = len(associations)

            deposits.append([n, m, l, associations[a], t, c]);
    except:
        print "error parsing file"
        raise

    return deposits

file = open('N', 'r')
N = int(file.read())
file.close()

file = open('R', 'r')
R = int(file.read())
file.close()

file = open('frames', 'r')
frames = int(file.read())
file.close()

for r in range(0, R):
    os.chdir('run.{0}'.format(r))

    file = open('times', 'r')
    times = map(float, file.read().split())
    file.close()

    coverage = []
    mono = []

    atoms = []
    for f in range(0, frames):
        fhandle = open('frame.{0}.inc'.format(f), 'r')
        atoms = atoms + readAtoms(N, fhandle.read())
        fhandle.close()
        coverage.append(reduce(lambda x, y: x + y, map(lambda x : 1 if x[2] == 0 else 0, atoms)) / float(N * N))
        
        mono.append(len(atoms) / float(N * N))

    matplotlib.pyplot.plot(times, coverage)
    matplotlib.pyplot.xlabel('time')
    matplotlib.pyplot.ylabel('coverge % of first monolayer')
    matplotlib.pyplot.title('coverage % of first monolayer vs. times')
    matplotlib.pyplot.savefig('coverage.png', format='png')
    matplotlib.pyplot.clf()

    matplotlib.pyplot.plot(times, mono)
    matplotlib.pyplot.xlabel('time')
    matplotlib.pyplot.ylabel('monolayers')
    matplotlib.pyplot.title('monolayers vs. times')
    matplotlib.pyplot.savefig('monolayers.png', format='png')
    matplotlib.pyplot.clf()
        
    os.chdir('../')
