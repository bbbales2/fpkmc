#!/usr/bin/env python

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot

import os, numpy, math

a1 = numpy.array([1, 0, 0]);
a2 = numpy.array([1 / 2, -math.sqrt(3) / 2, 0]);
a3 = numpy.array([1 / 2, -0.2887, math.sqrt(8 / 3) / 2]);

b1 = 2 * math.pi * numpy.cross(a2, a3) / numpy.dot(a1, numpy.cross(a2, a3));
b2 = 2 * math.pi * numpy.cross(a3, a1) / numpy.dot(a1, numpy.cross(a2, a3));
b3 = 2 * math.pi * numpy.cross(a1, a2) / numpy.dot(a1, numpy.cross(a2, a3));

file = open('N', 'r')
N = int(file.read())
file.close()

file = open('R', 'r')
R = int(file.read())
file.close()

file = open('frames', 'r')
frames = int(file.read())
file.close()

class Atom:
    def __init__(self, x, y, z, assoc, element):
        self.x = x
        self.y = y
        self.z = z

        self.assoc = assoc
        self.element = element

def process(string):
    args = string.split()

    e = [int(args[0]), int(args[1]), int(args[2])]

    [xoff, yoff] = [0, 0] if (e[2] % 2 == 0) else [-0.5000, 0.2887]
    
    x = (xoff + e[0] + e[1] * 1.0 / 2.0)
    y = (yoff + -e[1] * math.sqrt(3) / 2)
    z = (e[2] * math.sqrt(6) / 3)
    #[xoff, yoff] = [0, 0] if (e[2] % 2 == 0) else [-1.0 / 2.0, -(1 - 1.0 / math.sqrt(3.0))]

    #x = (xoff + e[0] * 1.0 / 2.0 + e[1])
    #y = (yoff + e[0] * math.sqrt(3.0) / 2.0)
    #z = (e[2] * math.sqrt(6) / 3)

    a = int(args[3])
    t = args[4]

    return Atom(x, y, z, a, t)


for r in range(0, R):
    os.chdir('run.{0}'.format(r))

    file = open('times', 'r')
    times = map(float, file.read().split())
    file.close()

    heights = []

    height = numpy.zeros((N, N))
    for f in range(0, frames):
        handle = open('frame.{0}.inc'.format(f), 'r')

        for line in handle.readlines():
            coord = map(lambda x: int(x), line.split()[0:3])
            height[coord[0], coord[1]] = max(height[coord[0], coord[1]], coord[2] * math.sqrt(8 / 3) / 2.0 + 0.288);

        flat = []
        for c in height:
            for b in c:
                flat.append(b)

        heights.append(flat)

    try:
        os.mkdir('roughness')
    except:
        pass

    means = []
    rmss = []
    variances = []
    minimums = []
    maximums = []
    skewnesses = []
    kurtosises = []
    
    for f, height in enumerate(heights):
        if f == 0:
            first = 0
            second = 0
            third = 0
            fourth = 0
            minimum = 0
            maximum = max(height)

        number = len(height)
        first = sum(numpy.abs(height))
        second = sum(map( lambda x : x * x, height ))
        third = sum(map( lambda x : x * x * x, height ))
        fourth = sum(map( lambda x : x * x * x * x, height ))
        minimum = min(minimum, min(height))
        maximum = max(maximum, max(height))

        mean = float(first) / float(number)
        means.append(float(first) / float(number))
        rms = math.sqrt(second / float(number))
        variances.append(math.sqrt(rms * rms - mean * mean))
        rmss.append(math.sqrt(second / float(number)))
        minimums.append(minimum)
        maximums.append(maximum)
        
        skewnesses.append(float(third) / (number * rms * rms * rms))
        kurtosises.append(float(fourth) / (number * rms * rms * rms * rms))

    matplotlib.pyplot.plot(times, means)
    matplotlib.pyplot.xlabel('time')
    matplotlib.pyplot.ylabel('mean')
    matplotlib.pyplot.title('mean vs. times')
    matplotlib.pyplot.savefig('mean.png', format='png')
    matplotlib.pyplot.clf()

    matplotlib.pyplot.plot(times, rmss)
    matplotlib.pyplot.xlabel('time')
    matplotlib.pyplot.ylabel('rms')
    matplotlib.pyplot.title('rms vs. times')
    matplotlib.pyplot.savefig('rms.png', format='png')
    matplotlib.pyplot.clf()

    matplotlib.pyplot.plot(times, numpy.sqrt(variances))
    matplotlib.pyplot.xlabel('time')
    matplotlib.pyplot.ylabel('standard deviation')
    matplotlib.pyplot.title('standard deviation vs. times')
    matplotlib.pyplot.savefig('stddev.png', format='png')
    matplotlib.pyplot.clf()

    matplotlib.pyplot.plot(times, minimums)
    matplotlib.pyplot.xlabel('time')
    matplotlib.pyplot.ylabel('minimum')
    matplotlib.pyplot.title('minimum vs. times')
    matplotlib.pyplot.savefig('minimum.png', format='png')
    matplotlib.pyplot.clf()

    matplotlib.pyplot.plot(times, maximums)
    matplotlib.pyplot.xlabel('time')
    matplotlib.pyplot.ylabel('maximum')
    matplotlib.pyplot.title('maximum vs. times')
    matplotlib.pyplot.savefig('maximum.png', format='png')
    matplotlib.pyplot.clf()

    matplotlib.pyplot.plot(times, skewnesses)
    matplotlib.pyplot.xlabel('time')
    matplotlib.pyplot.ylabel('skewness')
    matplotlib.pyplot.title('skewness vs. times')
    matplotlib.pyplot.savefig('skewness.png', format='png')
    matplotlib.pyplot.clf()

    matplotlib.pyplot.plot(times, kurtosises)
    matplotlib.pyplot.xlabel('time')
    matplotlib.pyplot.ylabel('kurtosis')
    matplotlib.pyplot.title('kurtosis vs. times')
    matplotlib.pyplot.savefig('kurtosis.png', format='png')
    matplotlib.pyplot.clf()

    os.chdir('../')
