#ifndef TINYSSA_HPP__
#define TINYSSA_HPP__

#include <cmath>
#include <cassert>
#include <tuple>
#include <map>
#include <list>
#include <functional>

#include "RNG.hpp"

template<typename argT1, typename... argsT>
class TinySSA
{
private:
  class Reaction
  {
  public:
    std::function<double ()> calcPropensity;
    std::function<void ()> calcChange;

    //double lastReactionTime;

    //std::vector<int> changes;

    Reaction(std::function<double ()> calcPropensity, std::function<void ()> calcChange) : calcPropensity(calcPropensity), calcChange(calcChange)//, lastReactionTime(0.0)
    {
    }
  };

  typedef std::list<Reaction> ReactionListT;
  typedef std::map<double, typename ReactionListT::iterator> EventsMapT;

public:
  std::tuple<argT1, argsT...> states;
  //std::vector< std::vector<typename ReactionListT::iterator> > dependencies;
  double lastReactionTime;

  ReactionListT reactions;
  EventsMapT events;

  TinySSA(argT1 state0_, argsT ...states_) : states(state0_, states_...), lastReactionTime(0.0)
  {
  }

  //Disallow
  TinySSA(const TinySSA &tinySsa) = delete;
  TinySSA &operator=(const TinySSA &tinySsa) = delete;

  //Back to allowing!
  void addfunction(std::function<double ()> calcPropensity, std::function<void ()> calcChange)
  {
    reactions.push_front(Reaction(calcPropensity, calcChange));

    typename ReactionListT::iterator it = reactions.begin();

    events.insert(typename EventsMapT::value_type(0.0, it));
  }

  void recomputeNextTime(RNG &rng)
  {
    /*if(target != reactions.end())
      {
	assert(0);
	
	//events.insert(typename EventsMapT::value_type(target->lastReactionTime + -log(rng.getDouble()) / target->calcPropensity(), target));
	}*/
    //else
    //{
	events.clear();

	for(typename ReactionListT::iterator it = reactions.begin(); it != reactions.end(); it++)
	  {
	    events.insert(typename EventsMapT::value_type(lastReactionTime + -log(rng.getDouble()) / it->calcPropensity(), it));
	  }
	//}
  }

  double getNextTime()
  {
    if(events.begin() != events.end())
      return events.begin()->first;
    
    return 0.0;
  }

  void doNextReaction(RNG &rng)
  {
    typename EventsMapT::iterator event = events.begin();

    if(event == events.end())
      {
	return;
      }
    
    //event->second->lastReactionTime = event->first;
    lastReactionTime = event->first;
    
    event->second->calcChange();

    /*for(std::vector<int>::iterator it = event.changes.begin(); it != event.changes.end(); it++)
      {
	for(std::vector<ReactionListT::iterator>::iterator it2 = users[*it].begin(); it2 != users[*it].end(); it2++)
	  {
	    recomputeNextTime(rng, *it2);
	  }
      }

      events.erase(event);*/

    //std::cout << "RxnTime = " << /*event->second->*/lastReactionTime << ", " << std::get<0>(states) << std::endl;
    
    recomputeNextTime(rng);
  }

  void reset(argT1 state0_, argsT ...states_)
  {
    states = std::make_tuple(state0_, states_...);

    lastReactionTime = 0;

    events.clear();
    //std::cout << &states << std::endl;
  }
  
  //friend std::ostream &operator<<(std::ostream &stream, const TinySSA<triggleTuple, argT1, argsT> &tinySsa);
};

//template<typename triggerTuple, typename argT1, typename... argsT>
//std::ostream &operator<<(std::ostream &stream, const TinySSA<triggleTuple, argT1, argsT...> &tinySsa)
//{
//
//}

#endif
