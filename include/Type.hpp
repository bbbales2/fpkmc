#ifndef TYPE_HPP__
#define TYPE_HPP__

#include <iostream>

enum class Type
{
  Ag,
  Au,
  Si,
  NONE //This type should help to serve as a debug tool. If something tries to compute with a none value
    // Then it should throw errors (because there should be no rates, operator<<, etc
};

inline std::ostream &operator<<(std::ostream &ostream,  const Type &type)
{
  if(type == Type::Ag)
    ostream << "Ag";
  else if(type == Type::Au)
    ostream << "Au";
  else if(type == Type::Si)
    ostream << "Si";
  else
    ASSERT(0, "Trying to stream out a non-existance type!");

  return ostream;
}

inline std::istream &operator>>(std::istream &istream, Type &type)
{
  std::string typeI;

  istream >> typeI;

  if(typeI == "Ag")
    type = Type::Ag;
  else if(typeI == "Au")
    type = Type::Au;
  else if(typeI == "Si")
    type = Type::Si;
  else
    ASSERT(0, "Trying to stream in a non-existance type!");

  return istream;
}

#endif
