#ifndef DEPOSITGRID_HPP__
#define DEPOSITGRID_HPP__

#include <stdlib.h>
//#include <malloc.h>
#include <map>
#include <set>
#include <algorithm>
#include <iostream>

#include "util.hpp"

#include "GridType.hpp"

#include "Deposit.hpp"

#include "Walker.hpp"
#include "Type.hpp"

//DepositGrid does two things
// 1. Keep track of how many particles have been deposited
// 2. For each position on the global grid, record a number indicating how far from the nearest
//      deposit is away.
//    a. Once deposits are deposited, there is no way for them to move, so it is really easy to keep this updated
//    b. Particles only see other things in some radius defined by RMAX. Equivalently, deposits only need to tell
//         gridpoints within some certain radius (RMAX + 2 here) that they exist

template<GridType GridT>
class DepositGrid
{
public:
  class Distance
  {
  public:
    int dn, dm;

    Distance() : dn(0), dm(0)
    {
    }

    Distance(int dn, int dm) : dn(dn), dm(dm)
    {
    }

    bool operator<(const Distance &other) const
    {
      return (std::max(dn, dm) < std::max(other.dn, other.dm)) ||
	(std::max(dn, dm) == std::max(other.dn, other.dm) && (dn + dm) < (other.dn + other.dm));
    }

    bool operator==(const Distance &other) const
    {
      return dn == other.dn && dm == other.dm;
    }

    bool operator>(const Distance &other) const
    {
      return !(*this < other) && !(*this == other);
    }

    bool operator<=(const Distance &other) const
    {
      return *this < other || *this == other;
    }

    bool operator>=(const Distance &other) const
    {
      return *this > other || *this == other;
    }
  };

public:
  template<typename T>
  class Layer
  {
  public:
    int N;
    T defaultV;
    T *data;

    Layer(int N, T defaultV) : N(N), defaultV(defaultV)
    {
      data = new T[N * N];      

      clear();
    }

    Layer(const Layer<T> &other) : Layer(other.N, other.defaultV)
    {
      *this = other;
    }

    ~Layer()
    {
      delete [] data;
    }

    void clear()
    {
      for(int i = 0; i < N * N; i++)
	data[i] = defaultV;
    }

    Layer<T> &operator=(const Layer<T> &other)
    {
      ASSERT(other.N == N, "Layer::operator=: N values not equal");

      for(int i = 0; i < N * N; i++)
	data[i] = other.data[i];

      return *this;
    }

    T &operator()(int n, int m)
    {
      return data[m * N + n];
    }
  };

  class LayerInfo
  {
  public:
    Layer<Distance> distanceLayer;
    Layer<Distance> edgeLayer;
    Layer<Type> typeLayer;
    Layer<Association> associationLayer;
    int size;
  
    LayerInfo(int N, int RMAX) : distanceLayer(N, Distance(std::max(RMAX + 2, 1), std::max(RMAX + 2, 1))),
				 edgeLayer(N, Distance(0, 0)),
				 typeLayer(N, Type::NONE),
				 associationLayer(N, Association(Association::None)),
				 size(0)
    { 
    }
  };

  typedef std::map<int, LayerInfo> LayerMap;

  int N, RMAX;

  LayerMap layerMap;
public:

  DepositGrid(int N, int RMAX) : N(N), RMAX(RMAX)
  {
    clear();
  }

  ~DepositGrid()
  {
  }

  void clear()
  {
    layerMap.clear();

    layerMap.insert(typename LayerMap::value_type(0, LayerInfo(N, RMAX)));
  }

  //Interpret h == -1 as "return accumulative size on all layers"
  //  Otherwise, return size of layer level h
  int size(int h = -1) const
  {
    int _size = 0;

    if(h == -1)
      {
	for(typename LayerMap::const_iterator it = layerMap.begin(); it != layerMap.end(); it++)
	  {
	    _size += it->second.size;
	  }
      }
    else
      {
	typename LayerMap::const_iterator it = layerMap.find(h);

	if(it == layerMap.end())
	  {
	    _size = -1;
	  }
	else
	  {
	    _size = it->second.size;
	  }
      }

    return _size;
  }

  //This depends on the gridType
  Distance rebuildEdge(Layer<Distance> &dLayer, Index index);

  //This is usually expensive!
  void rebuildEdgeLayer(int h)
  {
    typename LayerMap::iterator it = layerMap.find(h);

    ASSERT(it != layerMap.end(), "Requesting a layer rebuild on a layer that doesn't exist. Does this make sense?");
    
    Layer<Distance> &dLayer = it->second.distanceLayer,
      &eLayer = it->second.edgeLayer;

    for(int i = 0; i < N; i++)
      {
	for(int j = 0; j < N; j++)
	  {
	    eLayer(i, j) = rebuildEdge(dLayer, Index(i, j, h));
	  }
      }
  }

  //I've never been really comfortable with this function
  //
  //It returns, for a 2D index, the first value of h at which this value will fit
  // NOTE: It can return a height at which the atom will not be supported!
  // Also, depending on the grid, 'up' in the +h direction is not necessarily 'up' in the traditional z cartesian direction
  int getHeight(Index index)
  {
    int n = adjust(index.getn(), N),
      m = adjust(index.getm(), N),
      h = -1;

    for(typename LayerMap::reverse_iterator it = layerMap.rbegin(); it != layerMap.rend(); it++)
      {
	Distance distance = it->second.distanceLayer(n, m);

	if(distance.dn != 0 || distance.dm != 0)
	  {
	    h = (*it).first;
	  }
	else
	  {
	    break;
	  }
      }

    if(h == -1)
      {
	h = layerMap.rbegin()->first + 1;
      }

    return h;
  }    

  //This returns a reference to a set containing the relative points required to support a walker/deposit at the given index
  const std::set<Index> &getFloorStencil(Index index);

  //Add a new deposit
  // Update distance map
  // Update edge map *occasionally*. This is expensive... Cannot afford to update each time
  void insert(Walker walker)
  {
    Index index = walker.index;

    int n = index.getn(),
      m = index.getm(),
      h = index.getl();

    //std::cout << isAvailable(index) << std::endl;
    //std::cout << !check(index) << std::endl;

    ASSERT(isAvailable(index) && !check(index), "Errrororororg");

    if(DEBUG)
      std::cout << "DepositGrid: Depositing at " << n << ", " << m << ", " << h << std::endl;

    typename LayerMap::iterator it = layerMap.find(h);

    if(it == layerMap.end())
      it = layerMap.insert(typename LayerMap::value_type(h, LayerInfo(N, RMAX))).first;

    //Not quite right...
    //ASSERT(it.second, "DepositGrid::insert: Failed to insert new LayerInfo");

    it->second.size++;

    Layer<Type> &typeLayer = it->second.typeLayer;

    typeLayer(n, m) = walker.type;

    Layer<Distance> &dLayer = it->second.distanceLayer;

    Layer<Association> &associationLayer = it->second.associationLayer;

    associationLayer(n, m) = walker.association;

    for(int i = n - (RMAX + 2); i < n + (RMAX + 2); i++)
      {
	int nn = adjust(i, N);
	int dn = std::abs(cyclicDistance(nn, n, N));

	for(int j = m - (RMAX + 2); j < m + (RMAX + 2); j++)
	  {
	    int mm = adjust(j, N);
	    Distance distance(dn, std::abs(cyclicDistance(mm, m, N)));
	      
	    dLayer(nn, mm) = std::min(dLayer(nn, mm), distance);
	  }
      }
    
    Layer<Distance> &eLayer = it->second.edgeLayer;

    const std::set<Index> &iset = getFloorStencil(Index(n, m, h));

    for(std::set<Index>::const_iterator it = iset.begin(); it != iset.end(); it++)
      {
	Index index = Index(n, m, h) + (*it);
	eLayer(adjust(index.getn(), N), adjust(index.getm(), N)) = rebuildEdge(dLayer, index);
      }

    if(it->second.size > 0 && (it->second.size % (N * N / 4)) == 0)
    {
	rebuildEdgeLayer(h);
    }
	}

  Association getAssociation(Index index)
  {
    typename LayerMap::iterator it = layerMap.find(index.getl());

    if(index.getl() < 0)
      return Association::None;

    if(it == layerMap.end())
      {
	return Association::None;
      }
    
    Layer<Association> &aLayer = it->second.associationLayer;

    return aLayer(adjust(index.getn(), N), adjust(index.getm(), N));
  }

  Distance getDistanceToDeposit(Index index)
  {
    typename LayerMap::iterator it = layerMap.find(index.getl());

    if(index.getl() < 0)
      return Distance(RMAX + 2, RMAX + 2);

    if(it == layerMap.end())
      {
	if(DEBUG)
	  std::cout << "DepositGrid::getDistanceToDeposit: Inserting new layer " << index.getl() << std::endl;

	it = layerMap.insert(typename LayerMap::value_type(index.getl(), LayerInfo(N, RMAX))).first;
	
	//Not quite right...
	//ASSERT(it.second, "DepositGrid::getDistanceToDeposit: Failed to insert new LayerInfo");
      }
    
    Layer<Distance> &dLayer = it->second.distanceLayer;

    return dLayer(adjust(index.getn(), N), adjust(index.getm(), N));
  }

  Distance getDistanceToEdge(Index index)
  {
    if(index.getl() < 0)
      {
	return Distance(std::max(RMAX + 2, 1), std::max(RMAX + 2, 1));
      }
    else
      {
	typename LayerMap::iterator it = layerMap.find(index.getl());

	if(it == layerMap.end())
	  {
	    if(DEBUG)
	      std::cout << "DepositGrid::getDistanceToDeposit: Inserting new layer " << index.getl() << std::endl;
	    
	    it = layerMap.insert(typename LayerMap::value_type(index.getl(), LayerInfo(N, RMAX))).first;
	    
	    //Not quite right...
	    //ASSERT(it.second, "DepositGrid::getDistanceToDeposit: Failed to insert new LayerInfo");
	  }
    
	Layer<Distance> &eLayer = it->second.edgeLayer;

	return eLayer(adjust(index.getn(), N), adjust(index.getm(), N));
      }
  }

  //Check if the spot at Index is available to deposit in (include support checks)
  bool isAvailable(Index index);

  //Check if there is a deposit at Index (return true if so)
  bool check(Index index)
  {
    Distance distance = getDistanceToDeposit(index);
    
    if(distance.dn == 0 && distance.dm == 0)
      {
	return true;
      }
    else
      {
	return false;
      }
  }

  //Get the type of deposit at given location
  Type checkType(Index index)
  {
    typename LayerMap::iterator it = layerMap.find(index.getl());

    if(index.getl() < 0)
      return Type::NONE;

    //NOTE: Will insert non-existant layers in 2d code maybe?
    if(it == layerMap.end())
      {
	if(DEBUG)
	  std::cout << "DepositGrid::checkType: Inserting new layer " << index.getl() << std::endl;

	it = layerMap.insert(typename LayerMap::value_type(index.getl(), LayerInfo(N, RMAX))).first;
      }
    
    Layer<Type> &typeLayer = it->second.typeLayer;

    return typeLayer(adjust(index.getn(), N), adjust(index.getm(), N));
  }

  Type checkFloorType(Index index)
  {
    if(index.getl() <= 0)
      return Type::Si;
    else
      return Type::Ag;
  }

  //Get the association of deposit at given location
  Association checkAssociation(Index index)
  {
    typename LayerMap::iterator it = layerMap.find(index.getl());

    if(index.getl() < 0)
      return Association(0);

    //NOTE: Will insert non-existant layers in 2d code maybe?
    if(it == layerMap.end())
      {
	if(DEBUG)
	  std::cout << "DepositGrid::checkType: Inserting new layer " << index.getl() << std::endl;

	it = layerMap.insert(typename LayerMap::value_type(index.getl(), LayerInfo(N, RMAX))).first;
      }
    
    Layer<Association> &associationLayer = it->second.associationLayer;

    return associationLayer(adjust(index.getn(), N), adjust(index.getm(), N));
  }
};

#endif
