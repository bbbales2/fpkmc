#ifndef CDF_HPP__
#define CDF_HPP__

#include <pmmintrin.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include <vector>

#include "Walker.hpp"
#include "RNG.hpp"
#include "GridType.hpp"

static const double PI = 3.1415926535897932384626433832795028841;

template<GridType GridT>
class CDF
{
  void initialize(int r);

    typedef std::map<double, std::vector<double> > EPEix0TableT;

public:
  CDF(double w, int RMAX);

  ~CDF();

  double computeFirstExit(int r, RNG &rng, double tol = 1e-3, bool cache = true);
  double computeFirstExit(int r, double rn, double tol = 1e-3, bool cache = true);
  int computeExitPosition(double t, int r, RNG &rng, bool early = false);
};

template<>
class CDF<Square>
{
private:
  double h, w;

  const int RMAX, T, NMAX; //T works good at 1000, NMAX needs to be a multiple of 2 for the aligned matrix-vector multiply

  typedef std::map<double, std::vector<double> > EPEix0TableT;

  struct Workspace {
    double *evals,
      *evecs,
      *Eix0,
      *dens,
      *t,
      *Fp,
      *Fpp,
      *PEix0,
      *EPEix0;
    
      EPEix0TableT EPEix0Table;
  };

  Workspace *workspaces;

  void initialize(int r);
  void evaluate(double t, int _n, Workspace &workspace);
  double evaluateF(double t, int r, Workspace &workspace);
  double evaluateFp(double t, int r, Workspace &workspace);
  double evaluateFpp(double t, int r, Workspace &workspace);

public:
  CDF(const CDF<Square> &other);
  CDF(double w, int RMAX);
  ~CDF();

  double computeFirstExit(int r, RNG &rng, double tol = 1e-3, bool cache = true);
  double computeFirstExit(int r, double rn, double tol = 1e-3, bool cache = true);

  Index computeExitPosition(double t, int r, RNG &rng, bool early = false);
};

template<>
class CDF<Triangle>
{
private:
  double h, w;

  const int RMAX;

public:
  struct ode_params
  {
    int n;
    double *A;
  };

private:
  struct Workspace {
    std::map<double, double *> x;
    std::map<double, double> F;

    double *A,
      *factors,
      t,
      h;
    
    ode_params *params;

    gsl_odeiv2_step *s;
    gsl_odeiv2_control *c;
    gsl_odeiv2_evolve *e;
    gsl_odeiv2_system sys;
  };

  Workspace *workspaces;

  void initialize(int r);
  void evaluate(double F, double t, int n, Workspace &workspace);

public:
  CDF(double w, int RMAX);
  CDF(const CDF<Triangle> &other);
  ~CDF();

  double computeFirstExit(int r, RNG &rng);
  double computeFirstExit(int r, double rn);

  Index computeExitPosition(double t, int r, RNG &rng, bool early = false);
};

template<>
class CDF<Square2D>
{
private:
  double h, w;

  const int RMAX, T, NMAX; //T works good at 1000, NMAX needs to be a multiple of 2 for the aligned matrix-vector multiply

  typedef std::map<double, std::vector<double> > EPEix0TableT;

  class Workspace {
  public:
    double *evals,
      *evecs,
      *Eix0,
      *dens,
      *t,
      *Fp,
      *Fpp,
      *PEix0,
      *EPEix0;
    
      EPEix0TableT EPEix0Table;
  };

  Workspace *workspaces;

  void initialize(int r);
  void evaluate(double t, int _n, Workspace &workspace);
  double evaluateF(double t, int r, Workspace &workspace);
  double evaluateFp(double t, int r, Workspace &workspace);
  double evaluateFpp(double t, int r, Workspace &workspace);

public:
  CDF(const CDF<Square2D> &other);
  CDF(double w, int RMAX);
  ~CDF();

  double computeFirstExit(int r, RNG &rng, double tol = 1e-3, bool cache = true);
  double computeFirstExit(int r, double rn, double tol = 1e-3, bool cache = true);

  Index computeExitPosition(double t, int r, RNG &rng, bool early = false);
};

template<>
class CDF<Triangle2D>
{
private:
  double h, w;

  const int RMAX;

public:
  struct ode_params
  {
    int n;
    double *A;
  };

private:
  struct Workspace {
    std::map<double, double *> x;
    std::map<double, double> F;

    double *A,
      *factors,
      t,
      h;
    
    ode_params *params;

    gsl_odeiv2_step *s;
    gsl_odeiv2_control *c;
    gsl_odeiv2_evolve *e;
    gsl_odeiv2_system sys;
  };

  Workspace *workspaces;

  void initialize(int r);
  void evaluate(double F, double t, int n, Workspace &workspace);

public:
  CDF(double w, int RMAX);
  CDF(const CDF<Triangle2D> &other);
  ~CDF();

  double computeFirstExit(int r, RNG &rng);
  double computeFirstExit(int r, double rn);

  Index computeExitPosition(double t, int r, RNG &rng, bool early = false);
};

#endif
