#ifndef EDGECDF_HPP__
#define EDGECDF_HPP__

#include <pmmintrin.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include <vector>

#include "Walker.hpp"
#include "RNG.hpp"
#include "GridType.hpp"

template<GridType GridT>
class EdgeCDF
{
  /* public:
  EdgeCDF(double w, double wcorner, int RMAX);
  ~EdgeCDF();

  double computeFirstExit(int r, int corner, RNG &rng);
  double computeFirstExit(int r, int corner, double rn);

  int computeExitPosition(double t, int r, int corner, RNG &rng, bool early = false);*/
};

template<>
class EdgeCDF<Square2D>
{
private:
  double w, wcorner;

  const int RMAX;

public:
  struct ode_params
  {
    int n;
    double *A;
  };

private:
  struct Workspace {
    std::map<double, double *> x;
    std::multimap<double, double> F;

    double *A,
      t,
      h;
    
    ode_params *params;

    gsl_odeiv2_step *s;
    gsl_odeiv2_control *c;
    gsl_odeiv2_evolve *e;
    gsl_odeiv2_system sys;
  };

  std::vector< std::vector< Workspace > > workspaces;

  void initialize(int r, int corner);
  void evaluate(double F, double t, int n, Workspace &workspace);

public:
  EdgeCDF(const EdgeCDF<Square2D> &other);
  EdgeCDF(double w, double wcorner, int RMAX);
  ~EdgeCDF();

  double computeFirstExit(int r, int corner, RNG &rng);
  double computeFirstExit(int r, int corner, double rn);

  int computeExitPosition(double t, int r, int corner, RNG &rng, bool early = false);
};

template<>
class EdgeCDF<Square>
{
private:
  double w, wcorner;

  const int RMAX;

public:
  struct ode_params
  {
    int n;
    double *A;
  };

private:
  struct Workspace {
    std::map<double, double *> x;
    std::multimap<double, double> F;

    double *A,
      t,
      h;
    
    ode_params *params;

    gsl_odeiv2_step *s;
    gsl_odeiv2_control *c;
    gsl_odeiv2_evolve *e;
    gsl_odeiv2_system sys;
  };

  std::vector< std::vector< Workspace > > workspaces;

  void initialize(int r, int corner);
  void evaluate(double F, double t, int n, Workspace &workspace);

public:
  EdgeCDF(const EdgeCDF<Square> &other);
  EdgeCDF(double w, double wcorner, int RMAX);
  ~EdgeCDF();

  double computeFirstExit(int r, int corner, RNG &rng);
  double computeFirstExit(int r, int corner, double rn);

  int computeExitPosition(double t, int r, int corner, RNG &rng, bool early = false);
};

template<>
class EdgeCDF<Triangle2D>
{
private:
  double w, wcorner;

  const int RMAX;

public:
  struct ode_params
  {
    int n;
    double *A;
  };

private:
  struct Workspace {
    std::map<double, double *> x;
    std::map<double, double> F;

    double *A,
      t,
      h;
    
    ode_params *params;

    gsl_odeiv2_step *s;
    gsl_odeiv2_control *c;
    gsl_odeiv2_evolve *e;
    gsl_odeiv2_system sys;
  };

  std::vector< std::vector< Workspace > > workspaces;

  Workspace initialize(int r, int corner);
  void evaluate(double F, double t, int n, Workspace &workspace);

public:
  EdgeCDF(double w, double wcorner, int RMAX);
  EdgeCDF(const EdgeCDF<Triangle2D> &other);
  ~EdgeCDF();

  double computeFirstExit(int r, int corner, RNG &rng);
  double computeFirstExit(int r, int corner, double rn);

  int computeExitPosition(double t, int r, int corner, RNG &rng, bool early = false);
};

template<>
class EdgeCDF<Triangle>
{
private:
  double w, wcorner;

  const int RMAX;

public:
  struct ode_params
  {
    int n;
    double *A;
  };

private:
  struct Workspace {
    std::map<double, double *> x;
    std::map<double, double> F;

    double *A,
      t,
      h;
    
    ode_params *params;

    gsl_odeiv2_step *s;
    gsl_odeiv2_control *c;
    gsl_odeiv2_evolve *e;
    gsl_odeiv2_system sys;
  };

  std::vector< std::vector< Workspace > > workspaces;

  Workspace initialize(int r, int corner);
  void evaluate(double F, double t, int n, Workspace &workspace);

public:
  EdgeCDF(double w, double wcorner, int RMAX);
  EdgeCDF(const EdgeCDF<Triangle> &other);
  ~EdgeCDF();

  double computeFirstExit(int r, int corner, RNG &rng);
  double computeFirstExit(int r, int corner, double rn);

  int computeExitPosition(double t, int r, int corner, RNG &rng, bool early = false);
};

#endif
