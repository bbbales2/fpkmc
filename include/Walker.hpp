#ifndef WALKER_HPP_
#define WALKER_HPP_

#include <float.h>

#include "util.hpp"
#include "defines.hpp"

#include <float.h>
#include <iostream>

#include "Event.hpp"
#include "Deposit.hpp"
#include "Index.hpp"
#include "Type.hpp"
#include "Association.hpp"

//This is simply a container for the walker information
//  It acts more like a C-style struct than a C++ class. Members public, etc.
//  Event is an iterator to the events object in a KMC instance
class Walker
{
public:
  Index index;
  double t;

  Type type;

  int r, edgeR;

  double exitTime, forceTime;

  bool attached, deposited;
  EventMap::iterator event;

  Association association;

  Walker(const Walker &other) : Walker(other.index, other.t, other.type, other.r, other.edgeR)
  {
    *this = other;
  }

  Walker(Index index, double t, Type type, int r = 0, int edgeR = 0) : index(index), t(t), type(type), r(r), edgeR(edgeR), exitTime(t), forceTime(DBL_MAX), attached(false), deposited(false), association(Association::None)
  {
  }

  Walker &operator=(const Walker &other)
  {
    index = other.index;
    t = other.t;
    
    type = other.type;
    
    r = other.r;
    edgeR = other.edgeR;

    exitTime = other.exitTime;
    forceTime = other.forceTime;

    attached = other.attached;
    deposited = other.deposited;
    
    //This seems like a very bad idea
    event =  other.event;

    association = other.association;

    return *this;
  }

  bool operator<(const Walker &other) const
  {
    return index < other.index;
  }

  bool operator==(const Walker &other) const
  {
    return index == other.index;
  }

  bool operator!=(const Walker &other) const
  {
    return !(*this == other);
  }

  void reset(int RMAX)
  {
    r = RMAX;
    edgeR = RMAX;
    forceTime = DBL_MAX;
  }

  friend std::ostream &operator<<(std::ostream &stream, const Walker &walker);
};

inline std::ostream &operator<<(std::ostream &stream, const Walker &walker)
{
  return stream << "[" << walker.type << " @ " << walker.index << ", r = " << walker.r << ", edgeR = " << walker.edgeR << ", t = " << walker.t << ", exitTime = " << walker.exitTime << ", forceTime = " << walker.forceTime << ", attached = " << ((walker.attached) ? "true" : "false") << "]";
}

#endif
