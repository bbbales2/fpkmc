#ifndef RNG_HPP__
#define RNG_HPP__

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <gsl/gsl_rng.h>

//This is absolutely a wrapper around the Gnu Scientific Libraries Mersenne Twister code

#include "defines.hpp"

class RNG
{
private:
    gsl_rng * r;

public:
  RNG(int s = 0)
  {
    const gsl_rng_type * T;
     
    T = gsl_rng_mt19937;
    r = gsl_rng_alloc(T);
    gsl_rng_set(r, s);
  }

  RNG(const RNG &other)
  {
    const gsl_rng_type * T;

    T = gsl_rng_mt19937;
    r = gsl_rng_alloc(T);

    *this = other;
  }

  ~RNG()
  {
    gsl_rng_free (r);
  }

  RNG &operator=(const RNG &other)
  {
    gsl_rng_memcpy(r, other.r);

    return *this;
  }

  void reseed(int s = 0)
  {
    gsl_rng_set(r, s);
  }

  double getDouble()
  {
    return gsl_rng_uniform_pos(r);
  }

  unsigned long getInt()
  {
    return gsl_rng_get(r);
  }
};

#endif
