#ifndef GRIDTYPE_HPP__
#define GRIDTYPE_HPP__

enum GridType
  {
    Triangle,
    Triangle2D,
    Square,
    Square2D
  };

inline std::ostream &operator<<(std::ostream &stream, GridType gridType)
{
  std::string type;

  if(gridType == Triangle)
    type = "Triangle";
  else if(gridType == Triangle2D)
    type = "Triangle2D";
  else if(gridType == Square)
    type = "Square";
  else if(gridType == Square2D)
    type = "Square2D";

  return stream << type;
}

inline std::istream &operator>>(std::istream &stream, GridType &gridType)
{
  std::string type;

  stream >> type;

  if(type == "Triangle")
    gridType = Triangle;
  else if(type == "Triangle2D")
    gridType = Triangle2D;
  else if(type == "Square")
    gridType = Square;
  else if(type == "Square2D")
    gridType = Square2D;

  return stream;
}

#endif
