#ifndef EVENT_HPP__
#define EVENT_HPP__

// The event types must be defined here so that both the KMC file and the Walker file can get to them

#include <map>
#include <functional>
#include <map>

typedef std::multimap<double, std::function<void ()> > EventMap;
typedef EventMap::value_type Event;

#endif
