#ifndef DEFINES_HPP__
#define DEFINES_HPP__

#define DEBUG false

//Subgridded on 16x16 blocks
//#define BLOCKSIZE 16 //Current findR checks a block and all of the nearest neighbor blocks for walkers when computing radii
//  This means that the maximum radius of a walker must be (BLOCKSIZE / 2 - 1) to prevent collision with other walkers
//
//DepositGrid also depends on this value
//  Of note, it caps the value of RMAX at 255 or something like that (it uses chars as ints)
//#define RMAX 7

//Maximum number of edgeDiffusions to permit between any reaction/diffusion/deposit combination of events

#define MAXESTEPS 1000

#endif
