#ifndef INDEX_HPP__
#define INDEX_HPP__

#include <stdint.h>
#include <iostream>

class Index
{
  int n, m, l;
public:
  Index(int n = 0, int m = 0, int l = 0) : n(n), m(m), l(l)
  {
  }

  Index(const Index &o)
  {
    n = o.n;
    m = o.m;
    l = o.l;
  }

  ~Index()
  {
  }

  Index &operator=(const Index &other)
  {
    n = other.n;
    m = other.m;
    l = other.l;

    return *this;
  }

  bool operator==(const Index &other) const
  {
    return n == other.n && m == other.m && l == other.l;
  }

  bool operator<(const Index &other) const
  {
    return (l < other.l) || (l == other.l && ((m < other.m) || (m == other.m && n < other.n)));
  }

  Index operator*(const int other)
  {
    return Index(n * other, m * other, l * other);
  }

  Index operator+(const Index other)
  {
    return Index(n + other.n, m + other.m, l + other.l);
  }

  Index operator-(const Index other)
  {
    return Index(n - other.n, m - other.m, l - other.l);
  }

  Index &operator*=(const int other)
  {
    return *this = *this * other;
  }

  Index &operator+=(const Index other)
  {
    return *this = *this + other;
  }

  Index &operator-=(const Index other)
  {
    return *this = *this + other;
  }

  int getn() const
  {
    return n;
  }

  int getm() const
  {
    return m;
  }

  int getl() const
  {
    return l;
  }

  friend std::ostream &operator<<(std::ostream &stream, const Index &index);
};

#endif
