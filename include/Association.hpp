#ifndef ASSOCIATION_HPP__
#define ASSOCIATION_HPP__

#include <iostream>

class Association
{
private:
  static int associationSeed;

public:
  int association;
  static Association None;

  Association()
  {
    takeSeed();
  }

  Association(int association) : association(association)
  {
  }

  void takeSeed()
  {
    association = associationSeed++;
  }

  void force(int associationForced)
  {
      association = associationForced;
  }

  void adopt(Association _association)
  {
    if(_association == None)
      {
      }
    else if(*this == None)
      {
	*this = _association;
      }
  }

  bool operator<(const Association &other) const
  {
    if(association < other.association)
      return true;
    else
      return false;
  }

  bool operator>(const Association &other) const
  {
      if (association > other.association)
	  return true;
      else
	  return false;
  }

  bool operator==(const Association &other)
  {
    return (association == other.association) ? true : false;
  }

  bool operator!=(const Association &other)
  {
    return (association != other.association) ? true : false;
  }

  friend std::ostream &operator<<(std::ostream &ostream, const Association &association);
  friend std::istream &operator>>(std::istream &istream, Association &association);
};

inline std::ostream &operator<<(std::ostream &ostream, const Association &association)
{
  return ostream << association.association;
}

inline std::istream &operator>>(std::istream &istream, Association &association)
{
  istream >> association.association;

  association.associationSeed = std::max(association.associationSeed, association.association);

  return istream;
}

#endif
