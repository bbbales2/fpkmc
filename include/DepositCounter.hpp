#ifndef DEPOSITCOUNTER_HPP__
#define DEPOSITCOUNTER_HPP__

#include <map>
#include <algorithm>

#include "Index.hpp"
#include "DepositGrid.hpp"

template<GridType GridT>
class DepositCounter
{
private:

public:
  int Nmax;

  class DepositInfo
  {
  private:
    Type type;
    Index index;
    Association association;

  public:
    DepositInfo(Type type, Index index, Association association) : type(type), index(index), association(association)
    {
    }

    bool operator<(const DepositInfo &other) const
    {
      if(index < other.index)
	return true;
      else
	if(index == other.index)
	  if(type < other.type)
	    return true;

      return false;
    }

    Type getType() const
    {
      return type;
    }

    Index getIndex() const
    {
      return index;
    }

    Association getAssociation() const
    {
      return association;
    }
  };

  std::map<DepositInfo, int> counter;

public:
  DepositCounter() : Nmax(0)
  {
  }

  ~DepositCounter()
  {
  }

  DepositCounter &operator=(const DepositCounter &oDepositCounter)
  {
    counter = oDepositCounter.counter;

    Nmax = std::max(Nmax, oDepositCounter.Nmax);
    
    return *this;
  }

  int count() const
  {
    int c = 0;

    for(typename std::map<DepositInfo, int>::const_iterator it = counter.begin(); it != counter.end(); it++)
      {
	c += it->second;
      }
    
    return c;
  }

  void accumulate(DepositGrid<GridT> &grid)
  {
    Nmax = std::max(Nmax, grid.N);

    for(int l = 0; grid.size(l) > 0; l++)
      {
	for(int n = 0; n < grid.N; n++)
	  {
	    for(int m = 0; m < grid.N; m++)
	      {
		Index q(n, m, l);

		if(grid.check(q))
		  {
		    DepositInfo depositInfo(grid.checkType(q), q, grid.checkAssociation(q));
		    typename std::map<DepositInfo, int>::iterator it = counter.find(depositInfo);

		    if(it == counter.end())
		      {
			counter[depositInfo] = 1;
		      }
		    else
		      {
			(*it).second++;
		      }
		  }
	      }
	  }
      }
  }

  void accumulate(const DepositCounter &other)
  {
    Nmax = std::max(Nmax, other.Nmax);

    for(typename std::map<DepositInfo, int>::const_iterator it2 = other.counter.begin(); it2 != other.counter.end(); it2++)
      {
	typename std::map<DepositInfo, int>::iterator it1 = counter.find(it2->first);
	
	if(it1 == counter.end())
	  {
	    counter[it2->first] = it2->second;
	  }
	else
	  {
	    it1->second += it2->second;
	  }
      }
  }

  double distance(const DepositCounter &other)
  {
    double sum = 0.0;

    int tcount = count(),
      ocount = other.count();

    for(typename std::map<DepositInfo, int>::iterator it1 = counter.begin(); it1 != counter.end(); it1++)
      {
	double v1 = 0.0, v2 = 0.0;

	v1 = double((*it1).second) / double(tcount);
	
	typename std::map<DepositInfo, int>::const_iterator it2 = other.counter.find((*it1).first);
	
	if(it2 != other.counter.end())
	  v2 = (*it2).second / double(ocount);

	sum += (v1 - v2) * (v1 - v2);
      }

    for(typename std::map<DepositInfo, int>::const_iterator it2 = other.counter.begin(); it2 != other.counter.end(); it2++)
      {
	double v2 = 0.0;

	v2 = double((*it2).second) / double(ocount);
	
	typename std::map<DepositInfo, int>::const_iterator it1 = counter.find((*it2).first);
	
	if(it1 == counter.end())
	  sum += v2 * v2;
      }

    return sqrt(sum);
  }

  void clear()
  {
    Nmax = 0;
    counter.clear();
  }

  template<GridType GridT1>
  friend inline std::ostream &operator<<(std::ostream &out, const DepositCounter<GridT1> &depositCounter);

  template<GridType GridT1>
  friend inline std::istream &operator>>(std::istream &in, DepositCounter<GridT1> &depositCounter);
};

template<GridType GridT>
inline std::ostream &operator<<(std::ostream &out, DepositCounter<GridT> &depositCounter)
{
  //out << depositCounter.Nmax << " " << GridT << std::endl;

  for(typename std::map<typename DepositCounter<GridT>::DepositInfo, int>::iterator it = depositCounter.counter.begin(); it != depositCounter.counter.end(); it++)
    {
      typename DepositCounter<GridT>::DepositInfo q = it->first;
      int count = it->second;
      
      Type type = q.getType();
      Association association = q.getAssociation();
      
      out << q.getIndex().getn() << " " << q.getIndex().getm() << " " << q.getIndex().getl() << " " << association << " " << type << " " << count << std::endl;
    }

  return out;
}

template<GridType GridT>
inline std::istream &operator>>(std::istream &in, DepositCounter<GridT> &depositCounter)
{
  if(!in.good())
    return in;

  int _Nmax;
  GridType type;

  in >> _Nmax;

  ASSERT(in.good(), "Error");

  depositCounter.Nmax = _Nmax;

  in >> type;

  ASSERT(type == GridT, "Trying to import a file of different type that the initilized grid");
    
  while(1)
    {
      Association association;
      Type type;
      int n, m, l, count;

      if(!in.good())
	break;
      in >> n;

      if(!in.good())
	break;
      in >> m;

      if(!in.good())
	break;
      in >> l;

      if(!in.good())
	break;
      in >> association;

      if(!in.good())
	break;
      in >> type;

      if(!in.good())
	break;
      in >> count;

      ASSERT(depositCounter.counter.insert(
					   typename std::map<typename DepositCounter<GridT>::DepositInfo, int>
					   ::value_type(typename DepositCounter<GridT>::DepositInfo(type, Index(n, m, l), association), count)
					   ).second, "Trying to load the same index/count pair from a file two times!");
    }
  
  return in;
}

#endif
