#ifndef KMC_HPP__
#define KMC_HPP__

#include <stdlib.h>
#include <stdio.h>
//#include <malloc.h>
#include <math.h>
#include <map>
#include <unordered_set>
#include <list>
#include <cfloat>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>

#include "util.hpp"
#include "defines.hpp"

#include "Association.hpp"
#include "RNG.hpp"
#include "CDF.hpp"
#include "Event.hpp"
#include "Walker.hpp"
#include "WalkerGrid.hpp"
#include "Deposit.hpp"
#include "DepositGrid.hpp"
#include "GridType.hpp"
#include "DepositCounter.hpp"
#include "EdgeCDF.hpp"

template<GridType GridT>
class KMC
{
private:
  std::map< double, CDF<GridT> > cdfs;
  RNG rng;
  std::map< double, EdgeCDF<GridT> > edgeCDFs;

  enum class EdgeEventType
    {
      EDGE_UP,
      EDGE_BREAK,
      EDGE_DIFFUSE
    };

  //Grid dimensions -- I don't know why I leave M in there... N is the only one that matters
  int N, M;

  //List of events! This is map of Events sorted on time values (doubles)
  EventMap events;

  //Various reaction constants
  struct RatePack
  {
    double FonSi, FonM, Dedge, Dbreak, Dup, Ddown, Dcorner, DonSi, DonM;
  };

  std::map<Type, RatePack> ratePacks;
  double a, c, C;
  int s, r, RMAX;

  //We need this Event to keep track of the reaction (every time a deposit happens, the reaction rate changes so the reaction time must be resampled and a new event made)
  EventMap::iterator reactionEvent;

  //These keep track of all our stuff
  WalkerGrid<GridT> walkerGrid;

public:
  DepositCounter<GridT> depositCounter;
  DepositGrid<GridT> depositGrid;

private:
  //Used for reaction mechanism
  double lastRxnTime;
  double lastDiffTime;

  bool visualize;

  int hops, propagates, earlyPropagates, edgePropagates, earlyEdgePropagates;

  int reactionCount;

  double edgeTime;

  std::fstream visuFile;

  std::string checkPtFile;

public:
  double distance(KMC<GridT> &other)
  {
    return depositCounter.distance(other.depositCounter);
  }
  
  //Resample the events around index named around for walker (so do not resample walker itself)
  void resample(const Walker &walker, Index around, Index index)
  {
    std::pair<Walker &, bool> out = walkerGrid.get(around + index);

    if(DEBUG)
      std::cout << "resample: walker " << around + index << std::endl;
    
    if(out.second)
      {
	// Do not try to resample the walker we're working with
	// Do not try to resample a deposit event
	if(out.first != walker && !out.first.deposited)
	  {
	    if(out.first.event != events.end())
	      {
		events.erase(out.first.event);
		out.first.event = events.end();
	      }
	    else
	      if(DEBUG) std::cout << "resample: Finding event for no-event walker " << out.first << " which is not equal to " << walker << std::endl;
	    
	    findR(out.first, false);
	  }
      }
  }

  double computeNextRxnTime();

  double maxReactionRate()
  {
    return std::max({ ratePacks[Type::Au].FonM, ratePacks[Type::Au].FonSi, ratePacks[Type::Ag].FonM, ratePacks[Type::Ag].FonSi });
  }

  //reaction
  // Find a spot to insert a new particle and do it
  // Execute findR on this particle (if the particle interferes with any other particle's zone, findR will trigger the necessary updates)
  void reaction(double nextRxnTime)
  {
    Index q;
    Type floorType;

    //do
    //{
    Index i = Index(rng.getInt(), rng.getInt());
    
    q = adjust(i, N);
    
    //if(DEBUG) std::cout << "reaction: Selecting " << q << std::endl;
    
    q = adjust(Index(q.getn(), q.getm(), depositGrid.getHeight(q)), N);
	
    floorType = depositGrid.checkFloorType(q);
    //}
    //while(!(depositGrid.isAvailable(q) && walkerGrid.isAvailable(q)));
    
    //std::cout << "depositGrid.isAvailable(q) " << q << " " << depositGrid.isAvailable(q) << std::endl;
    lastRxnTime = nextRxnTime;

    reactionEvent = events.end();

    nextRxnTime = computeNextRxnTime();

    reactionEvent = events.insert(std::pair<double, std::function<void ()> >(nextRxnTime, std::function<void ()>(std::bind(&KMC::reaction, std::ref(*this), nextRxnTime))));

    //double Ftotal = ratePacks[Type::Ag].FonSi + ratePacks[Type::Ag].FonAg + ;

    typename DepositGrid<GridT>::Distance distance = depositGrid.getDistanceToDeposit(q);

    if((floorType == Type::Si && rng.getDouble() < std::max(ratePacks[Type::Ag].FonSi, ratePacks[Type::Au].FonSi) / maxReactionRate()) ||
       (floorType == Type::Ag && rng.getDouble() < std::max(ratePacks[Type::Ag].FonM, ratePacks[Type::Au].FonM) / maxReactionRate()))
      {
	Type walkerType;
	if(floorType == Type::Si)
	  {
	    if(rng.getDouble() * (ratePacks[Type::Ag].FonSi + ratePacks[Type::Au].FonSi) < ratePacks[Type::Ag].FonSi)
	      walkerType = Type::Ag;
	    else
	      walkerType = Type::Au;
	  }

	if(floorType == Type::Ag || floorType == Type::Au)
	  {
	    if(rng.getDouble() * (ratePacks[Type::Ag].FonM + ratePacks[Type::Au].FonM) < ratePacks[Type::Ag].FonM)
	      walkerType = Type::Ag;
	    else
	      walkerType = Type::Au;
	  }

	if(depositGrid.isAvailable(q) && walkerGrid.isAvailable(q))
	  {
	    Walker &newWalker = (*walkerGrid.insert(Walker(q, lastRxnTime, walkerType, 0, 0))).second;

	    newWalker.event = events.end();

	    if(visualize)
	      {
		visuFile << newWalker.t << " REACTION " << newWalker.index.getn() << " " << newWalker.index.getm() << " " << newWalker.index.getl() << std::endl;
	      }
	
	    findR(newWalker);

	    //All the new neighbors must be re-sampled now
	    const std::set<Index> &iset = walkerGrid.getStencil(newWalker.index);
	    std::for_each(iset.begin(), iset.end(), std::bind(&KMC::resample, std::ref(*this), std::ref(newWalker), std::ref(newWalker.index), std::placeholders::_1));

	    if(DEBUG)
	      std::cout << "reaction: inserting new walker " << newWalker << "on floorType: " << floorType << std::endl;

	    reactionCount++;
	  }
	else
	  {
	    if(DEBUG) std::cout << "reaction: spot not available" << std::endl;
	  }
      }
    else
      {
	if(DEBUG) std::cout << "reaction: not taken" << std::endl;
      }
    
    if(DEBUG) std::cout << "reaction: t = " << lastRxnTime << ", nextRxnTime = " << nextRxnTime << ", depositCount = " << depositGrid.size() << ", walkers.size = " << walkerGrid.size() << std::endl;
  }

  int getRadius(int dn, int dm);

  void selectEvent(Walker &walker, bool resample = false);

  //findR
  // Given a freshly moved walker, look around that walker and assess the situation in Grid independent fashion
  // Call select event (a function templated on gridType) to decide exactly what to do
  void findR(Walker &walker, bool resample = true)
  {
    if(DEBUG) std::cout << "findR (entry): walker " << walker << std::endl;

    walker.reset(RMAX);
    walker.event = events.end();

    int dn, dm;

    // Compute the potential radius of the walker
    if(!walker.deposited)
      {
	typename DepositGrid<GridT>::Distance distance = depositGrid.getDistanceToEdge(walker.index - Index(0, 0, 1));
	
	walker.r = std::min(walker.r, std::max(std::max(distance.dn, distance.dm) - 2, 0));
	walker.edgeR = std::min(walker.edgeR, std::max(std::max(distance.dn, distance.dm) - 2, 0));
	
	distance = depositGrid.getDistanceToDeposit(walker.index);
	
	dn = distance.dn;
	dm = distance.dm;

	ASSERT(dn != 0 || dm != 0, "collideDeposit: Landed on existing deposit\n");
	
	if(DEBUG) std::cout << "findR: Walker " << walker << " checking against @ rel " << Index(dn, dm, 0) << std::endl;

	walker.r = std::min(walker.r, std::max(std::max(dn, dm) - 2, 0));

	std::vector<typename WalkerGrid<GridT>::MapT *> blockVector = std::move(walkerGrid.findNeighbors(walker.index, std::max(RMAX + walker.r + 1, 2)));

	for(int i = 0; i < int(blockVector.size()); i++)
	  {
	    typename WalkerGrid<GridT>::MapT &grid = *blockVector[i];

	    for(typename WalkerGrid<GridT>::MapT::iterator it = grid.begin(); it != grid.end(); it++)
	      {
		Walker &owalker = it->second;
		
		if(walker == owalker)
		  {
		    continue;
		  }

		if(DEBUG) std::cout << "findR: Checking walker " << walker << " against walker " << owalker << std::endl;
	    
		dn = cyclicDistance(owalker.index.getn(), walker.index.getn(), N);
		dm = cyclicDistance(owalker.index.getm(), walker.index.getm(), M);
		int dl = owalker.index.getl() - walker.index.getl();

		//If we're touching another walker we may need to do something!
		bool isWithinKMCRange = false;

		//2d codes only need this first step
		if(getRadius(dn, dm) <= 1)
		  {
		    isWithinKMCRange = true;
		  }
		else if(dl != 0)
		//we have to be painfully careful with 3d codes
		  {
		    const std::set<Index> &set = walkerGrid.getStencil(walker.index);

		    for(std::set<Index>::const_iterator it = set.begin(); it != set.end(); it++)
		      {
			if(it->getl() != 0)
			  {
			    if(adjust(walker.index + *it, N) == owalker.index)
			      {
				isWithinKMCRange = true;
				break;
			      }
			  }
		      }
		  }
		
		if(isWithinKMCRange) //We need to be careful!
		  {
		    if(DEBUG)
		      std::cout << "findR: Walker " << walker << " is too close to " << owalker << std::endl;
		    bool doDeposit = false;

		    if(GridT == Square2D || GridT == Square)
		      {
			if((std::abs(dn) + std::abs(dm)) <= 1 && dl == 0)
			  {
			    doDeposit = true;
			  }
		      }
		    else if(GridT == Triangle2D || GridT == Triangle)
		      {
			if(dl == 0)
			  {
			    doDeposit = true;
			  }
		      }

		    if(doDeposit == true)
		      {
			walker.association.adopt(owalker.association);

			if(!walker.deposited)
			  {
			    deposit(walker);
			
			    if(walker.event != events.end())
			      events.erase(walker.event);
			
			    walker.event = events.insert(events.begin(), std::pair<double, std::function<void ()> >(walker.t, std::function<void ()>(std::bind(&KMC::eraseWalker, std::ref(*this), std::ref(walker)))));
			  }
		    
			walker.deposited = true;
		    
			if(!owalker.deposited)
			  {
			    deposit(owalker);
			
			    if(owalker.event != events.end())
			      events.erase(owalker.event);
			    else
			      if(DEBUG) std::cout << "findR: Assigning deposit to no-event walker " << owalker << std::endl;
			
			    owalker.event = events.insert(events.begin(), std::pair<double, std::function<void ()> >(walker.t, std::function<void ()>(std::bind(&KMC::eraseWalker, std::ref(*this), std::ref(owalker)))));
			  }
			
			owalker.deposited = true;
		      }
		    else
		      {
			if(resample && !owalker.deposited)
			  {
			    if(owalker.event != events.end())
			      events.erase(owalker.event);
			    
			    findR(owalker, false);
			  }
		      }
		  }

		dn = std::abs(dn);
		dm = std::abs(dm);
	    
		int rn = std::max(dn, dm) - owalker.r - 2;
		int edgeRn = std::max(dn, dm) - owalker.edgeR - 2;
		
		if(DEBUG && rn < 0)
		  {
		    std::cout << "findR: Walker " << walker << " landed too close to " << owalker << ", rn = " << rn << ", " << walker.t << " " << owalker.exitTime << " " << owalker.t << " " << ((owalker.deposited) ? "true " : "false ") << ((owalker.attached) ? "true " : "false ") << std::endl;
		  }
		
		if(DEBUG && edgeRn < 0)
		  {
		    std::cout << "findR: Walker " << walker << " landed too close to " << owalker << ", edgeRn = " << edgeRn << std::endl;
		  }
		
		walker.r = std::min(walker.r, std::max(std::min(rn, edgeRn), 0));
		walker.edgeR = std::min(walker.edgeR, std::max(std::min(rn, edgeRn), 0));

		//Check if we have landed in the zone of another walker -- there are time constraints too
		if(rn < 0 && owalker.r > 0 && walker.t < owalker.exitTime && walker.t > owalker.t && owalker.deposited == false && owalker.attached == false)
		  {
		    if(DEBUG) std::cout << "findR: Walkers have gotten too close: dn = " << dn << ", dm = " << dm << std::endl;
		    //ASSERT(owalker.index.getl() == walker.index.getl(), "This cannot come from different levels");
		    
		    owalker.forceTime = std::min(owalker.forceTime, walker.t);

		    if(owalker.event != events.end())
		      events.erase(owalker.event);
		    else
		      {
			if(DEBUG) std::cout << "findR: Assigning deposit to no-event walker " << owalker << std::endl;
		  
			ASSERT(0, "This should probably be impossible\n");
		      }

		    owalker.event = events.insert(events.begin(), std::pair<double, std::function<void ()> >(owalker.forceTime, std::function<void ()>(std::bind(&KMC::earlyPropagate, std::ref(*this), std::ref(owalker)))));

		    if(DEBUG) std::cout << "findR: Applying forceTime = " << owalker.forceTime << " to " << owalker << " courtesy " << walker << std::endl;
		  }

		//Check if we have landed in the edge propagate zone of another walker -- there are time constraints too
		if(edgeRn < 0 && owalker.edgeR > 0 && walker.t < owalker.exitTime && walker.t > owalker.t && owalker.deposited == false && owalker.attached == true)
		  {
		    if(DEBUG) std::cout << "findR: Walkers have gotten too close (edge): dn = " << dn << ", dm = " << dm << std::endl;
		    //ASSERT(owalker.index.getl() == walker.index.getl(), "This cannot come from different levels");
		    
		    owalker.forceTime = std::min(owalker.forceTime, walker.t);

		    Event event = *owalker.event;

		    ASSERT(owalker.forceTime < owalker.exitTime, "This should be true");

		    if(owalker.event != events.end())
		      events.erase(owalker.event);
		    else
		      {
			if(DEBUG) std::cout << "findR: Found a no-event edge walker " << owalker << std::endl;
		  
			ASSERT(0, "This should probably be impossible\n");
		      }

		    owalker.event = events.insert(events.begin(), Event(owalker.forceTime, event.second));

		    if(DEBUG) std::cout << "findR: Applying edge diffusion forceTime = " << owalker.forceTime << " to " << owalker << " courtesy " << walker << std::endl;
		  }
	      }
	  }
      }

    // Generate a new event
    if(!walker.deposited)
      {
	selectEvent(walker, resample);
      }

    /*if(resample)
      {
	const std::set<Index> &iset = walkerGrid.getStencil(walker.index);
	std::for_each(iset.begin(), iset.end(), std::bind(&KMC::resample, std::ref(*this), std::ref(walker), walker.index, std::placeholders::_1));
	}*/
  }

  // Hop! These should always be of dn <= 1, dm <= 1, dl <= 1
  void hop(Walker &walker, Index q)
  {
    Index oindex = walker.index;

    Walker _walker = walker;
    walkerGrid.erase(_walker.index);

    if(q.getl() < 0)
      if(DEBUG) std::cout << "Jumping down!" << std::endl;

    _walker.index = adjust(_walker.index + q, N);

    hops++;
    
    if(DEBUG) std::cout << "hop: Hopping from " << oindex << " to " << _walker.index << " with exitTime = " << _walker.exitTime << "\n";

    ASSERT(depositGrid.isAvailable(_walker.index), "Trying to hop onto another deposit!\n");
    ASSERT(walkerGrid.isAvailable(_walker.index), "Trying to hop onto another walker!\n");
    //ASSERT(depositGrid.checkFloor(_walker.index), "There is nothing below me!");

    if(_walker.attached)
      edgeTime += _walker.exitTime - _walker.t;

    _walker.t = _walker.exitTime;

    lastDiffTime = std::max(lastDiffTime, _walker.t);

    if(visualize)
      {
	visuFile << _walker.t << " HOP " << oindex.getn() << " " << oindex.getm() << " " << oindex.getl() << " " << _walker.index.getn() << " " << _walker.index.getm() << " " << _walker.index.getl() << std::endl;
      }

    findR((*walkerGrid.insert(_walker)).second);
    
    //All the old neighbors must be re-sampled now
    
    const std::set<Index> &iset = walkerGrid.getStencil(oindex);
    std::for_each(iset.begin(), iset.end(), std::bind(&KMC::resample, std::ref(*this), std::ref(_walker), oindex, std::placeholders::_1));
  }

//If the edgePropagater lands on another particle in its propagate, resample spot. Ben arbitrarily decided wiggling was too annoying to implement
  void edgePropagate(Walker &walker, EdgeEventType edgeEventType, std::vector<Index> &offsets, int cornerPosition)
  {
    Index q;
    double dt;
    int k = 0;

    Walker _walker = walker;
    walkerGrid.erase(_walker.index);

    typename std::map<Type, RatePack>::iterator itrp = ratePacks.find(_walker.type);

    ASSERT(itrp != ratePacks.end(), "Trying to select a ratePack that does not exist");
  
    RatePack &ratePack = itrp->second;

    double Dedge = ratePack.Dedge;

    typename std::map< double, EdgeCDF< GridT > >::iterator it = edgeCDFs.find(Dedge);

    ASSERT(it != edgeCDFs.end(), "Trying to propagate a walker (edge) which does not have an EdgeCDF");

    EdgeCDF< GridT > &edgeCDF = it->second;

    if(DEBUG)
      for(int i = 0; i < walker.edgeR * 2 + 3; i++)
	{
	  std::cout << "selectEvent possiblity: " << offsets[i] << std::endl;
	}

    do
      {
	q = Index(0, 0, 0);
	int i = 0;

	// Scratch whatever we planned to do, just edgePropagate!
	if(_walker.forceTime < _walker.exitTime)
	  {
	    earlyEdgePropagates++;
	    if(DEBUG) std::cout << "earlyEdgePropagate: forceTime = " << _walker.forceTime << std::endl;
	    dt = _walker.forceTime - _walker.t;
	    i = edgeCDF.computeExitPosition(dt, _walker.edgeR, cornerPosition, rng, true);

	    edgeTime += dt;
	  }
	else
	  {
	    dt = _walker.exitTime - _walker.t;
	    if(edgeEventType == EdgeEventType::EDGE_UP || edgeEventType == EdgeEventType::EDGE_BREAK)
	      {
		i = edgeCDF.computeExitPosition(dt, _walker.edgeR, cornerPosition, rng, true);
	      }
	    else
	      {
		edgePropagates++;

		edgeTime += _walker.exitTime - _walker.t;
		i = edgeCDF.computeExitPosition(dt, _walker.edgeR, cornerPosition, rng, false);
	      }
	  }

	q = offsets[i];

	//We did the early propagate... Now handle any break/down events that need to happen
	if(_walker.exitTime <= _walker.forceTime && edgeEventType == EdgeEventType::EDGE_BREAK)
	  {
	    //ASSERT(0, "You'll want to re-verify this code");
	    
	    if(DEBUG)
	      std::cout << "edgePropagate: Handling break event" << std::endl;

	    const std::set<Index> &set = walkerGrid.getStencil(_walker.index + q);

	    std::multimap<double, Index> possibilities;

	    for(std::set<Index>::const_iterator it = set.begin(); it != set.end(); it++)
	      {
		int count = 0;

		if(GridT == Square2D || GridT == Square)
		  {
		    if(std::abs((*it).getn()) + std::abs((*it).getm()) > 1)
		      continue;
		  }

		Index probe = _walker.index + q + *it;

		for(std::set<Index>::const_iterator it2 = set.begin(); it2 != set.end() && count == 0; it2++)
		  {
		    if(depositGrid.check(probe + *it2))
		      count++;
		  }

		if(DEBUG)
		  std::cout << "Checking " << *it << " at early propagate " << q;

		if(depositGrid.isAvailable(probe) && count == 0)
		  {
		    if(DEBUG)
		      std::cout << ", It's possible!";

		    possibilities.insert(std::map<double, Index>::value_type(1.0, *it));
		  }

		if(DEBUG)
		  std::cout << std::endl;
	      }

	    std::map<double, Index>::iterator it = select(rng.getDouble(), possibilities);

	    if(it != possibilities.end())
	      {
		if(DEBUG)
		  std::cout << "edgePropagate: Chose break event " << it->second << " of " << possibilities.size() << " possibilities" << std::endl;

		q += it->second;
	      }
	    else
	      {
		if(DEBUG)
		  std::cout << "edgePropagate: Found no possible break" << std::endl;
	      }
	  }
	else if(_walker.exitTime <= _walker.forceTime && edgeEventType == EdgeEventType::EDGE_UP)
	  {
	    if(DEBUG)
	      std::cout << "edgePropagate: Handling up event" << std::endl;

	    const std::set<Index> &set = walkerGrid.getStencil(_walker.index + q);

	    std::multimap<double, Index> possibilities;

	    for(std::set<Index>::const_iterator it = set.begin(); it != set.end(); it++)
	      {
		int count = 0;

		if((it->getl() - _walker.index.getl()) == 1)
		  {
		    Index probe = _walker.index + q + *it;

		    if(DEBUG)
		      std::cout << "Checking " << *it << " at early propagate " << q;

		    if(depositGrid.isAvailable(probe) && count == 0)
		      {
			if(DEBUG)
			  std::cout << ", It's possible!";

			possibilities.insert(std::map<double, Index>::value_type(1.0, *it));
		      }

		    if(DEBUG)
		      std::cout << std::endl;
		  }
	      }

	    std::map<double, Index>::iterator it = select(rng.getDouble(), possibilities);

	    if(it != possibilities.end())
	      {
		if(DEBUG)
		  std::cout << "edgePropagate: Chose up event " << it->second << " of " << possibilities.size() << " possibilities" << std::endl;

		q += it->second;
	      }
	    else
	      {
		if(DEBUG)
		  std::cout << "edgePropagate: Found no possible up" << std::endl;
	      }
	  }

	if(DEBUG)
	  std::cout << "edgePropagate: event i = " << i << " selected" << std::endl;

	q += _walker.index;

	if(DEBUG) std::cout << "edgePropagate: Attempting jump from " << _walker << " to " << q << " at t = " << _walker.t + dt << " with dt = " << dt << "\n";

	if(DEBUG)
	  if(k > 0)
	    std::cout << "Wiggling..." << std::endl;

	if(k >= 1)
	  {
	    if(k >= 10)
	    {
	      q = _walker.index;
	    }

	    if(k >= 11)
	      {
		ASSERT(0, "Error");
	      }
	  }

	k++;

	//Check if the walker coordinates have gone out of bounds. If so, wrap them around
	q = adjust(q, N);
      } while(!depositGrid.isAvailable(q) || !walkerGrid.isAvailable(q));

    _walker.t += dt;
    lastDiffTime = std::max(lastDiffTime, _walker.t);
    Index oindex = _walker.index;
    _walker.index = q;
    _walker.exitTime = _walker.t;

    //ASSERT(depositGrid.isAvailable(_walker.index), "edgePropagate landed where there was nothing!");
    //ASSERT(walkerGrid.isAvailable(_walker.index), "Edge walker landed on another walker!\n");

    if(visualize)
      {
	visuFile << _walker.t << " EDGE_PROPAGATE " << oindex.getn() << " " << oindex.getm() << " " << oindex.getl() << " " << _walker.index.getn() << " " << _walker.index.getm() << " " << _walker.index.getl() << std::endl;
      }

    findR(walkerGrid.insert(_walker)->second);
  }

  //earlyPropagate
  //  A walker has been told to propagate within its zone
  //  If the propagate lands on another walker (possible) or deposit (if bug free, impossible), then it is wiggled a few times to find a free position. Eventually, it is resampled
  //  findR is called to schedule next event
  void earlyPropagate(Walker &walker)
  {
    earlyPropagates++;

    Index oindex = walker.index;

    if(DEBUG) std::cout << "earlyPropagate: Checking to propagate Walker " << walker << " with forceTime = " << walker.forceTime << " and exitTime = " << walker.exitTime << std::endl;

    Walker _walker = walker;

    walkerGrid.erase(_walker.index);

    bool keepTrying = true;

    if(DEBUG) std::cout << "earlyPropagate: Early jumping from " << _walker;

    typename std::map<Type, RatePack>::iterator itrp = ratePacks.find(_walker.type);

    ASSERT(itrp != ratePacks.end(), "Trying to select a ratePack that does not exist");

    double D;

    RatePack &ratePack = itrp->second;

    if(depositGrid.checkFloorType(_walker.index) == Type::Si)
      D = ratePack.DonSi;
    else
      D = ratePack.DonM;

    typename std::map< double, CDF<GridT> >::iterator it = cdfs.find(D);

    ASSERT(it != cdfs.end(), "Trying to propagate a walker which does not have a CDF");

    CDF<GridT> &cdf = it->second;

    Index q;
    while(keepTrying)
      {
	keepTrying = false;
		
	q = cdf.computeExitPosition(_walker.forceTime - _walker.t, _walker.r, rng, true);

	int adjusting = 2;
	while(adjusting)
	  {
	    Index i = _walker.index + q;
	    
	    //If we try to land where there is already a deposit, try to stick on the side instead
	    //  If that is impossible, just generate a new landing spot
	    if(!depositGrid.isAvailable(i) || !walkerGrid.isAvailable(i))
	      {
		const std::set<Index> &set = walkerGrid.getStencil(q);

		int next = rng.getInt() % set.size();

		for(std::set<Index>::const_iterator it = set.begin(); it != set.end(); it++, next--)
		  {
		    if(next == 0)
		      {
			q += *it;
			break;
		      }
		  }
		    
		adjusting--;

		if(std::abs(q.getn()) > _walker.r || std::abs(q.getm()) > _walker.r)
		  {
		    adjusting = 0;
		  }

		keepTrying = true;
	      }
	    else
	      {
		adjusting = 0;
		
		keepTrying = false;
	      }
	  }
      }

    _walker.index = adjust(_walker.index + q, N);

    _walker.t = _walker.exitTime = _walker.forceTime;
    lastDiffTime = std::max(lastDiffTime, _walker.t);

    if(DEBUG) std::cout << " to " << _walker.index << " with t = " << _walker.t;

    /*ASSERT(depositGrid.checkFloor(_walker.index) &&
	   !walkerGrid.check(_walker.index) &&
	   !depositGrid.check(_walker.index), "Propagate landed where there was nothing!");*/

    if(visualize)
      {
	visuFile << _walker.t << " EARLY_PROPAGATE " << oindex.getn() << " " << oindex.getm() << " " << oindex.getl() << " " << _walker.index.getn() << " " << _walker.index.getm() << " " << _walker.index.getl() << std::endl;
      }

    findR((*walkerGrid.insert(_walker)).second);
  }

  //deposit
  //  This event simply removes the walker from the walker data structure, inserts a new deposit in the right place on the depositGrid, recomputes a new reaction rate, and resamples a new reaction time from that rate
  void deposit(Walker &walker)
  {
    //std::cout << "Depositing: " << walker << std::endl;

    //SUPERIOR CODE
    //Search neighboring deposits to find an association.
    //  Choose the most common one
    Association association;

    Index index = walker.index;

    std::map<Association, int> below_counts;
    std::map<Association, int> counts;

    {
      const std::set<Index> &iset = depositGrid.getFloorStencil(index);

      //Search supporting deposits!
      for(std::set<Index>::const_iterator it = iset.begin(); it != iset.end(); it++)
	{
          //Do not look at the substrate for association
          Index q = index + *it + Index(0, 0, -1);

          if(q.getl() >= 0)
            {
              Association association = depositGrid.getAssociation(q);

              if(below_counts.find(association) == below_counts.end())
                below_counts[association] = 1;
              else
                below_counts[association] = below_counts[association] + 1;
            }
        }
    }

    {
      //Search neighbors on the same level
      const std::set<Index> &iset = walkerGrid.getStencil(index);
      
      for(std::set<Index>::const_iterator it = iset.begin(); it != iset.end(); it++)
        {
          Index q = index + *it;
            
          if(it->getl() == 0)
            {
              Association association = depositGrid.getAssociation(q);
                
              if(association != Association::None)
                {
                  if(counts.find(association) == counts.end())
                    {
                      counts[association] = 1;
                      //std::cout << "starting " << q << " assoc " << association << std::endl;
                    }
                  else
                    {
                      counts[association] = counts[association] + 1;
                    }
                }
            }
        }
    }

    if(below_counts.size() > 0)
      {
        auto max_below = std::max_element(below_counts.begin(), below_counts.end());

        if(counts.size() > 0)
          {
            auto max_above = std::max_element(counts.begin(), counts.end());

            if(below_counts.find(max_above->first) != below_counts.end())
              walker.association = max_above->first;
            else
              walker.association = max_below->first;
          }
        else
          walker.association = max_below->first;
      }
    else
      {
        //If we found touching deposits, choose the one that occurs most often, otherwise just make up an association number!
        if(counts.size() > 0)
          {
            association = std::max_element(counts.begin(), counts.end())->first;
            //std::cout << "Chose " << association << " for " << index << std::endl;
          }
        else
          {
            //std::cout << "Tha fuq " << index << std::endl;
          }
        
        walker.association.adopt(association);
      }
    
    depositGrid.insert(walker);

    if(visualize)
      {
	visuFile << walker.t << " DEPOSIT " << walker.index.getn() << " " << walker.index.getm() << " " << walker.index.getl() << std::endl;
      }

    if(DEBUG) std::cout << "Deposit: Walker " << walker << std::endl;

    //This needs rethought
    //if(DEBUG)
    //std::cout << depositGrid;
      
    double nextRxnTime = computeNextRxnTime();

    if(DEBUG) std::cout << "deposit: nextRxnTime set to " << nextRxnTime << std::endl;

    if(reactionEvent != events.end())
      events.erase(reactionEvent);

    reactionEvent = events.insert(std::pair<double, std::function<void ()> >(nextRxnTime, std::function<void ()>(std::bind(&KMC::reaction, std::ref(*this), nextRxnTime))));
  }
  
  void eraseWalker(Walker &walker)
  {
    Walker _walker = walker;
    walkerGrid.erase(_walker.index);
  }

  //propagate
  //  There are no surprises here. A particle is propagated and then fed through findR to sample new events
  //  Note, the walker should not have radius zero. Those movements should go through hop instead
  void propagate(Walker &walker)
  {
    propagates++;

    Walker _walker = walker;

    walkerGrid.erase(_walker.index);

    typename std::map<Type, RatePack>::iterator itrp = ratePacks.find(_walker.type);

    ASSERT(itrp != ratePacks.end(), "Trying to select a ratePack that does not exist");

    RatePack &ratePack = itrp->second;

    double D;

    if(depositGrid.checkFloorType(_walker.index) == Type::Si)
      D = ratePack.DonSi;
    else
      D = ratePack.DonM;

    typename std::map< double, CDF<GridT> >::iterator it = cdfs.find(D);

    ASSERT(it != cdfs.end(), "Trying to propagate a walker which does not have a CDF");

    CDF<GridT> &cdf = it->second;

    double dt;
    Index q = Index(0, 0, 0);

    dt = _walker.exitTime - _walker.t;

    ASSERT(_walker.r > 0, "We should never get to this point (r = 0 propagates handled as hops)\n");

    //Get the location where it exits
    q = cdf.computeExitPosition(dt, _walker.r, rng);

    if(DEBUG)
      std::cout << "Propagate: Selecting propagate: " << q << std::endl;

    q += _walker.index;

    //Check if the walker coordinates have gone out of bounds. If so, wrap them around
    q = adjust(q, N);

    if(DEBUG) std::cout << "Propagate: Attempting jump from " << _walker << " to " << q << " at t = " << _walker.t << " with dt = " << dt << "\n";

    Index oindex = _walker.index;

    _walker.t += dt;
    lastDiffTime = std::max(lastDiffTime, _walker.t);
    _walker.index = q;

    /*ASSERT(depositGrid.checkFloor(_walker.index), "Propagate landed where there was nothing!");
    ASSERT(!depositGrid.check(_walker.index), "Walker landed on another deposit!\n");
    ASSERT(!walkerGrid.check(_walker.index), "Walker landed on another walker!\n");*/

    if(visualize)
      {
	visuFile << _walker.t << " PROPAGATE " << oindex.getn() << " " << oindex.getm() << " " << oindex.getl() << " " << _walker.index.getn() << " " << _walker.index.getm() << " " << _walker.index.getl() << std::endl;
      }

    findR((*walkerGrid.insert(_walker)).second);
  }

public:

  void run(double eC)
  {
    C = depositGrid.size() / double(N * M);
    
    while(eC + C >= depositGrid.size() / double(N * M))
      {
	EventMap::iterator it = events.begin();
	
	if(it == events.end())
	  ASSERT(0, "We've run out of events... Exiting (this should be an error)\n");
	
	if((*it).first < 0.0)
	  ASSERT(0, "An event was scheduled with a negative time");
	
	(*it).second();
	
	/*if((nC - depositGrid.size()) / std::max(1, int(0.01 * C * double(N * M))) < tracker)
	  {
		std::cout << (*it).first << " " << reactionCount << std::endl;
		reactionCount = 0;
		tracker--;
		}*/
	
	events.erase(it);
	
	if(DEBUG) printf("--\n");
      }
    
    r++;
  }

  double getTime()
  {
    return std::max(lastRxnTime, lastDiffTime);
  }

  void restart(int _r)
  {
    depositCounter.clear();
    
    r = _r;

    reset();
  }

  void reset()
  {
    depositCounter.accumulate(depositGrid);

    events.clear();

    walkerGrid.clear();
    depositGrid.clear();

    lastRxnTime = 0.0;
    lastDiffTime = 0.0;

    reactionCount = 0;

    rng.reseed(r + s);
    
    if (checkPtFile.compare("NONE")){
	std::ifstream infile;
	infile.open(checkPtFile, std::ifstream::in);
	// check to see if file exists
	
	if (infile.good()){
	   // std::string line;	
	    
	    // check lattice parameters to make sure  they conform with arguments given at program call
	    int NXcheckPt;
	    std::string LatticeCheckPt;
	    infile >> NXcheckPt >> LatticeCheckPt;

	    //std::cout << "check for grid sizes: "<< NXcheckPt << ", " << N <<std::endl;
	    ASSERT(NXcheckPt == N,"Dimension of checkFile doesn't match given dimension\n");
	    // need to check if LatticeCheckPt conforms with runtime parameters

	    //std::cout << NXcheckPt <<" " <<LatticeCheckPt <<std::endl;
	    
	    int l,m,n, association, tmp;
	    std::string typeStr;
	    // std::cout << "Reading CheckPoint File: " << checkPtFile<<std::endl;
	    while (infile >> l >> m >> n >> association >> typeStr >>tmp){
		// std::cout << l<<" "<<m<<" "<<n<<" "<<association<<" "<<typeStr<<" "<<tmp<<std::endl;
		Type theType;
		std::stringstream typeStream;
		typeStream << typeStr;
		typeStream >> theType;

		Walker walker(Index(l,m,n),0.0, theType);

		walker.association.force(association);

		depositGrid.insert(walker);
	    }

	   // std::cout <<" done reading "<< checkPtFile<<std::endl;
	}
	else{
	    std::cout << "trying to open checkpointFile: " << checkPtFile <<std::endl;
	    ASSERT(0,"Problem opening checkpointFile");
	}
	infile.close();
    }
    else
    {
    //int bN = N / 4,
    //bM = M / 4;

    //Association association = Walker(Index(), 0.0, Type::Si).association;

    //Add a square to propagate on
    /*for(int i = N / 2 - bN / 2; i < N / 2 + bN / 2; i++)
      {
	for(int j = M / 2 - bM / 2; j < M / 2 + bM / 2; j++)
	  {
	    Index newDeposit(i, j);
	    
	    if(i != (N - 1) / 2 || j != (M - 1) / 2)
	      depositGrid.insert(Walker(newDeposit, 0.0, Type::Ag));
	  }
	  }*/

    //Add a line to propagate on
    /*int i = N / 2;
    for(int j = M / 2 - bM / 2; j < M / 2 + bM / 2; j++)
      {
	Index newDeposit(i, j);
      
	if(i != (N - 1) / 2 || j != (M - 1) / 2)
	  depositGrid.insert(Walker(newDeposit, 0.0, Type::Ag));
	  }*/

    //Add one deposit in the center
    //Index newDeposit((N - 1) / 2, (M - 1) / 2, 0);
    Walker walker(Index((N - 1) / 2, (M - 1) / 2, 0), 0.0, Type::Ag);
    walker.association.takeSeed();
    depositGrid.insert(walker);
    /*depositGrid.insert(Deposit((N - 1) / 2 - 1, (M - 1) / 2));
    depositGrid.insert(Deposit((N - 1) / 2, (M - 1) / 2 - 1));*/

    //Add a hexagon to propagate on
    /*for(int i = N / 2 - bN / 2; i < N / 2 + bN / 2; i++)
      {
	for(int j = M / 2 - bM / 2; j < M / 2 + bM / 2; j++)
	  {
      //for(int i = 0; i < N; i++)
      //{
      //for(int j = 0; j < M; j++)
      //{
    	    Index newDeposit(i, j);
	    
	    if(getRadius((i - N / 2), (j - M / 2)) <= 4)
	      {
		Walker walker(newDeposit, 0.0, Type::Ag);

		walker.association = association;

		depositGrid.insert(walker);
		
		if(visualize)
		  {
		    visuFile << 0.0000001 << " REACTION " << newDeposit.getn() << " " << newDeposit.getm() << " " << newDeposit.getl() << std::endl;
		    
		    visuFile << 0.000001 << " DEPOSIT " << newDeposit.getn() << " " << newDeposit.getm() << " " << newDeposit.getl() << std::endl;
		  }
	      }
	  }
	  }*/
    } // end else

    double nextRxnTime = computeNextRxnTime();

    reactionEvent = events.insert(std::pair<double, std::function<void ()> >(nextRxnTime, std::function<void ()>(std::bind(&KMC::reaction, std::ref(*this), nextRxnTime))));
  }
  
  //check no lose particle
  //check ww wd collisions
  KMC(int N, double a, double FonM_Ag, double FonSi_Ag, double Dedge_Ag, double Dbreak_Ag, double Dup_Ag, double Ddown_Ag, double Dcorner_Ag, double D_Ag,
      double FonM_Au, double FonSi_Au, double Dedge_Au, double Dbreak_Au, double Dup_Au, double Ddown_Au, double Dcorner_Au, double D_Au,
      int RMAX, int s, bool visualize = false, std::string CkPtFile = "NONE") :
    rng(s),
    N(N), M(N),
    c(0.0), C(0), s(s), r(0), RMAX(RMAX),
    walkerGrid(N, RMAX),
    depositGrid(N, RMAX),
    lastRxnTime(0.0),
    lastDiffTime(0.0),
    visualize(visualize),
    hops(0),
    propagates(0),
    earlyPropagates(0),
    edgePropagates(0),
    earlyEdgePropagates(0),
    reactionCount(0),
    checkPtFile(CkPtFile),
    edgeTime(0.0)
  {
    RatePack ratePack;

    ratePack.Dedge = Dedge_Ag;
    ratePack.Dbreak = Dbreak_Ag;
    ratePack.Dup = Dup_Ag;
    ratePack.Ddown = Ddown_Ag;
    ratePack.Dcorner = Dcorner_Ag;
    ratePack.DonM = D_Ag;
    ratePack.DonSi = D_Ag;
    ratePack.FonSi = FonSi_Ag;
    ratePack.FonM = FonM_Ag;

    ratePacks.insert(typename std::map< Type, RatePack >::value_type(Type::Ag, ratePack));

    ratePack.Dedge = Dedge_Au;
    ratePack.Dbreak = Dbreak_Au;
    ratePack.Dup = Dup_Au;
    ratePack.Ddown = Ddown_Au;
    ratePack.Dcorner = Dcorner_Au;
    ratePack.DonM = D_Au;
    ratePack.DonSi = D_Au;
    ratePack.FonSi = FonSi_Au;
    ratePack.FonM = FonM_Au;

    ratePacks.insert(typename std::map< Type, RatePack >::value_type(Type::Au, ratePack));

    cdfs.insert(typename std::map<double, CDF<GridT>>::value_type(ratePacks[Type::Ag].DonM, CDF<GridT>(ratePacks[Type::Ag].DonM, RMAX)));
    cdfs.insert(typename std::map<double, CDF<GridT>>::value_type(ratePacks[Type::Au].DonM, CDF<GridT>(ratePacks[Type::Au].DonM, RMAX)));
    cdfs.insert(typename std::map<double, CDF<GridT>>::value_type(ratePacks[Type::Ag].DonSi, CDF<GridT>(ratePacks[Type::Ag].DonSi, RMAX)));
    cdfs.insert(typename std::map<double, CDF<GridT>>::value_type(ratePacks[Type::Au].DonSi, CDF<GridT>(ratePacks[Type::Au].DonSi, RMAX)));
    edgeCDFs.insert(typename std::map<double, EdgeCDF<GridT>>::value_type(ratePacks[Type::Ag].Dedge, EdgeCDF<GridT>(ratePacks[Type::Ag].Dedge, ratePacks[Type::Ag].Dcorner, RMAX)));
    edgeCDFs.insert(typename std::map<double, EdgeCDF<GridT>>::value_type(ratePacks[Type::Au].Dedge, EdgeCDF<GridT>(ratePacks[Type::Au].Dedge, ratePacks[Type::Au].Dcorner, RMAX)));

    std::ostringstream filename;
    
    filename << "vis." << s << "." << r << ".fpkmc";

    if(visualize)
      {
	visuFile.open(filename.str(), std::fstream::out);
	
	visuFile << N << std::endl;
      }

    reset();
    //r = 50;
  }

  ~KMC()
  {
    if(visualize)
      visuFile.close();
  }
};

#endif
