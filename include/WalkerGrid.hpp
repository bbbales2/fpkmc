#ifndef OVERLAPPEDGRID_HPP__
#define OVERLAPPEDGRID_HPP__

#include <stdlib.h>
//#include <malloc.h>
#include <map>
#include <set>
#include <list>
#include <iostream>
#include <vector>

#include "util.hpp"
#include "GridType.hpp"
#include "Walker.hpp"
#include "Index.hpp"

//This container holds all the walkers
//  The large grid (NxN) is chopped up into smaller subgrids (blockSize x blockSize)
//  Each of the blocks track walkers in a Map (where walkers are sorted by a 1d projection of their 2d position)
//  It's not exactly C++ kosher. Lotta public variables accessed from the outside
//

/*namespace std
{
  template<>
  class hash<Walker>
  {
  public:
    size_t operator()(Walker walker) const
    {
      return walker.m * walker.N + walker.n;
    }
  };
}*/

template<GridType GridT>
class WalkerGrid
{
public:
  typedef std::map<Index, Walker> MapT;
  typedef std::vector<MapT> BlockGridT;

private:
  int N, blockSize, NB, RMAX, _size;
  
  BlockGridT Q;
public:

  WalkerGrid(int N, int RMAX) : N(N), RMAX(RMAX), _size(0)
  {
    int next_up = 2;

    while(next_up < (RMAX + 1) * 2)
      {
	next_up *= 2;
      }

    blockSize = next_up;

    ASSERT(N % blockSize == 0, "blockSize is invalid");
    
    NB = N / blockSize;

    Q.resize(NB * NB, MapT());

    clear();
  }

  ~WalkerGrid()
  {
    Q.clear();

    _size = 0;
  }

  //Erase everything
  void clear()
  {
    Q.clear();

    Q.resize(NB * NB);

    _size = 0;
  }

  //This returns the number of active walkers (walkers that deposit are removed)
  int size() const
  {
    return _size;
  }

  //This returns a reference to a set containing the relative points surrounding a walker/deposit at the given index
  const std::set<Index> &getStencil(Index index);

  //Check to see if a walker is at a position. Return true if occupied
  bool isAvailable(Index index)
  {
    std::pair<Walker &, bool> out = get(index);

    if(DEBUG) std::cout << "WalkerGrid::isAvailable " << out.second << std::endl;

    return !out.second;
  }
  
  std::pair<Walker &, bool> get(Index index)
  {
    int BN = N / blockSize;

    index = adjust(index, N);

    {
      int bn = adjust(index.getn() / blockSize, BN),
	bm = adjust(index.getm() / blockSize, BN);

      MapT::iterator it = Q[bn + BN * bm].find(index);
      
      if(it != Q[bn + BN * bm].end())
	{
	  return std::pair<Walker &, bool>((*it).second, true);
	}
      else
	{
	  return std::pair<Walker &, bool>(*((Walker *)NULL), false);
	}
    }
  }

  //This returns a collection of iterators that lets us see all walkers in the block of a gridpoint
  //  as well as all the walkers in neighbor blocks
  std::vector<typename WalkerGrid<GridT>::MapT *> findNeighbors(Index index, int r)
  {
    std::vector<typename WalkerGrid<GridT>::MapT *> vtVec;

    int BN = N / blockSize;

    if(BN * BN >= 9)
      {
	int bn = index.getn() / blockSize,
	  bm = index.getm() / blockSize;
	
	int ln = bn * blockSize,
	  lm = bm * blockSize,
	  un = (bn + 1) * blockSize,
	  um = (bm + 1) * blockSize;
	
	bool mm = false, mo = false, mp = false,
	  om = false, oo = false, op = false,
	  pm = false, po = false, pp = false;

	if(index.getn() - r < ln)
	  {
	    if(index.getm() - r < lm)
	      {
		mm = true;
	      }

	    mo = true;
	    
	    if(index.getm() + r >= um)
	      {
		mp = true;
	      }
	  }

	if(index.getm() - r < lm)
	  {
	    om = true;
	  }
	
	oo = true;
	
	if(index.getm() + r >= um)
	  {
	    op = true;
	  }

	if(index.getn() + r >= un)
	  {
	    if(index.getm() - r < lm)
	      {
		pm = true;
	      }

	    po = true;
	    
	    if(index.getm() + r >= um)
	      {
		pp = true;
	      }
	  }

	int bmm1 = adjust(bm - 1, BN),
	  bmp1 = adjust(bm + 1, BN),
	  bnm1 = adjust(bn - 1, BN),
	  bnp1 = adjust(bn + 1, BN);

	int count = 0;

	if(DEBUG)
	  std::cout << "getNeighbors: blocksize = " << blockSize << " bm = " << bm << " bn = " << bn << std::endl;

	if(mm)
	  {
	    if(DEBUG)
	      std::cout << "getNeighbors: checking mm" << std::endl;
	    count++;
	  }

	if(mo)
	  {
	    if(DEBUG)
	      std::cout << "getNeighbors: checking mo" << std::endl;
	  count++;
	  }

	if(mp)
	  {
	    if(DEBUG)
	      std::cout << "getNeighbors: checking mp" << std::endl;
	  count++;
	  }

	if(om)
	  {
	    if(DEBUG)
	      std::cout << "getNeighbors: checking om" << std::endl;
	  count++;
	  }

	if(oo)
	  {
	    if(DEBUG)
	      std::cout << "getNeighbors: checking oo" << std::endl;
	  count++;
	  }

	if(op)
	  {
	    if(DEBUG)
	      std::cout << "getNeighbors: checking op" << std::endl;
	  count++;
	  }

	if(pm)
	  {
	    if(DEBUG)
	      std::cout << "getNeighbors: checking pm" << std::endl;
	  count++;
	  }
	
	if(po)
	  {
	    if(DEBUG)
	      std::cout << "getNeighbors: checking po" << std::endl;
	  count++;
	  }
	
	if(pp)
	  {
	    if(DEBUG)
	      std::cout << "getNeighbors: checking pp" << std::endl;
	  count++;
	  }
 
	vtVec.resize(count);

	if(mm)
	  {
	    vtVec[--count] = &Q[bnm1 + BN * bmm1];
	  }

	if(mo)
	  {
	    vtVec[--count] = &Q[bnm1 + BN * bm];
	  }	

	if(mp)
	  {
	    vtVec[--count] = &Q[bnm1 + BN * bmp1];
	  }
	
	if(om)
	  {
	    vtVec[--count] = &Q[bn + BN * bmm1];
	  }	

	if(oo)
	  {
	    vtVec[--count] = &Q[bn + BN * bm];
	  }	

	if(op)
	  {
	    vtVec[--count] = &Q[bn + BN * bmp1];
	  }
	
	if(pm)
	  {
	    vtVec[--count] = &Q[bnp1 + BN * bmm1];
	  }

	if(po)
	  {
	    vtVec[--count] = &Q[bnp1 + BN * bm];
	  }

	if(pp)
	  {
	    vtVec[--count] = &Q[bnp1 + BN * bmp1];
	  }
      }
    else
      {
	for(int i = 0; i < BN * BN; i++)
	  {
	    vtVec.push_back(&Q[i]);
	  }
      }

    return vtVec;
  }

  //Insert a walker into the map
  MapT::iterator insert(Walker walker)
  {
    int BN = N / blockSize;

    int bn = adjust(walker.index.getn() / blockSize, BN),
      bm = adjust(walker.index.getm() / blockSize, BN);

    if(DEBUG)
      std::cout << "WalkerGrid::insert: Inserting walker " << walker << " with exitTime = " << walker.exitTime << std::endl;

    std::pair<MapT::iterator, bool> out = Q[bn + BN * bm].insert(MapT::value_type(walker.index, walker));

    ASSERT(out.second == true, "WalkerGrid::insert: Trying to insert the same walker twice...\n");

    MapT::iterator it = out.first;

    _size++;

    return it;
  }

  //Erase a walker from the map... Careful, program will end if walker not found
  void erase(Index index)
  {
    int BN = N / blockSize;

    int bn = adjust(index.getn() / blockSize, BN),
      bm = adjust(index.getm() / blockSize, BN);

    MapT::iterator it = Q[bn + BN * bm].find(index);

    ASSERT(it != Q[bn + BN * bm].end(), "Failed to find Walker to erase\n");

    if(DEBUG)
      std::cout << "erase: Erasing walker " << it->second << " with exitTime = " << it->second.exitTime << std::endl;

    Q[bn + BN * bm].erase(index);

    _size--;
  }
};

#endif
