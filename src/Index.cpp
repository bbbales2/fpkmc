#include "Index.hpp"

std::ostream &operator<<(std::ostream &stream, const Index &index)
{
  return stream << "(" << index.n << ", " << index.m << ", " << index.l << ")";
}
