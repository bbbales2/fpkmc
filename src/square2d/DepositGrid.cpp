#include "DepositGrid.hpp"

template<>
typename DepositGrid<Square2D>::Distance DepositGrid<Square2D>::rebuildEdge(Layer<Distance> &dLayer, Index index)
{
  return Distance(RMAX, RMAX);
}

template<>
const std::set<Index> &DepositGrid<Square2D>::getFloorStencil(Index index)
{
  static std::set<Index> iset = {};

  return iset;
}

template<>
bool DepositGrid<Square2D>::isAvailable(Index index)
{
  Distance distance = getDistanceToEdge(index - Index(0, 0, 1));
    
  if(index.getl() != 0)
    {
      return false;
    }
  else
    {
      distance = getDistanceToDeposit(index);
	
      if(distance.dn == 0 && distance.dm == 0)
	{
	  return false;
	}
      else
	{
	  return true;
	}
    }
}
