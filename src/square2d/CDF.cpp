#include <stdio.h>

#include <gsl/gsl_cblas.h>
#include <amdlibm.h>

#include "RNG.hpp"
#include "CDF.hpp"

void CDF<Square2D>::initialize(int r)
{
  int n = 2 * r + 1;

  //Compute all the temporaries
  double *evals, *Eix0, *evecs, *dens, *t, *Fp, *Fpp, *PEix0, *EPEix0;
  evals = (double *)calloc(n, sizeof(double));
  Eix0 = (double *)calloc(n, sizeof(double));
  ASSERT(posix_memalign((void **)&evecs, 16, n * NMAX * sizeof(double)) == 0, "Invalid memalign");
  ASSERT(posix_memalign((void **)&PEix0, 16, n * sizeof(double)) == 0, "Invalid memalign");
  ASSERT(posix_memalign((void **)&EPEix0, 16, n * sizeof(double)) == 0, "Invalid memalign");
  dens = (double *)calloc(r + 1, sizeof(double));
  t = (double *)calloc(T, sizeof(double));
  Fp = (double *)calloc(T, sizeof(double));
  Fpp = (double *)calloc(T, sizeof(double));

  memset(evecs, 0, n * NMAX * sizeof(double));
  memset(PEix0, 0, n * sizeof(double));
  memset(EPEix0, 0, n * sizeof(double));

  for(int i = 0; i < n; i++)
    {
      double tmp = sin(PI * double(i + 1) / (2.0 * double(n + 1)));
      evals[i] = (-4.0 / (h * h)) * tmp * tmp;
    }

  for(int i = 0; i < n; i++)
    {
      for(int j = 0; j < n; j++)
	{
	  evecs[j * NMAX + i] = sqrt(2.0 / double(n + 1)) * sin(PI * double((i + 1) * (j + 1)) / double(n + 1));
	}
    }

  for(int i = 0; i < n; i++)
    {
      Eix0[i] = evecs[i * NMAX + (n / 2)];
    }

  for(int i = 0; i <= r; i++)
    {
      dens[i] = evals[2 * i];

      for(int j = 0; j <= r; j++)
	{
	  if(j == i)
	    continue;

	  dens[i] *= evals[2 * i] - evals[2 * j];
	}

      dens[i] = 1.0 / dens[i];
    }

  Workspace workspace;

  workspace.evals = evals;
  workspace.evecs = evecs;
  workspace.Eix0 = Eix0;
  workspace.dens = dens;
  workspace.t = t;
  workspace.Fp = Fp;
  workspace.Fpp = Fpp;
  workspace.PEix0 = PEix0;
  workspace.EPEix0 = EPEix0;

  workspaces[r] = workspace;

  t[0] = 0.0; //The value here should actually be infinite, but t[0] should never be accessed anyway
  for(int i = 0; i < T; i++)
    {
      double rn = i * (1.0 / double(T));
      t[i] = computeFirstExit(r, 1 - (1 - rn) * (1 - rn), 1e-12, false);
      Fp[i] = evaluateFp(t[i], r, workspaces[r]);
      Fpp[i] = evaluateFpp(t[i], r, workspaces[r]);
    }

  EPEix0TableT &table = workspaces[r].EPEix0Table;

  double t0 = log(0.01) / (1.0e9 * evals[0]),
    t1 = log(0.01) / evals[0];

  for(int i = 0; i < T; i++)
    {
      double tval = exp(t0 + i * (t1 - t0) / double(T));

      std::vector<double> vec(n);
      evaluate(tval, n, workspaces[r]);
      memcpy(&vec[0], workspaces[r].EPEix0, n * sizeof(double));

      table.insert(EPEix0TableT::value_type(tval, vec));
    }
}

void CDF<Square2D>::evaluate(double t, int _n, Workspace &workspace)
{
  unsigned int n = (unsigned int)_n;

  //double space[_n];

  for(unsigned int i = 0; i < n; i++)
    {
      workspace.PEix0[i] = exp(t * workspace.evals[i]) * workspace.Eix0[i];
    }

  for(unsigned int i = 0; i < n; i++)
    {
      workspace.EPEix0[i] = 0;
    }

  unsigned int i = 0;

  unsigned int nn = n;

  for(; i < n - nn % 16; i += 16)
    {
      __m128d c1 = _mm_setzero_pd(),
	c2 = _mm_setzero_pd(),
	c3 = _mm_setzero_pd(),
	c4 = _mm_setzero_pd(),
	c5 = _mm_setzero_pd(),
	c6 = _mm_setzero_pd(),
	c7 = _mm_setzero_pd(),
	c8 = _mm_setzero_pd();

      for(unsigned int j = 0; j < n; j++)
	{
	  __m128d a = _mm_load1_pd(workspace.PEix0 + j);

	  c1 = _mm_add_pd(c1, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 0)]), a));
	  c2 = _mm_add_pd(c2, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 2)]), a));
	  c3 = _mm_add_pd(c3, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 4)]), a));
	  c4 = _mm_add_pd(c4, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 6)]), a));
	  c5 = _mm_add_pd(c5, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 8)]), a));
	  c6 = _mm_add_pd(c6, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 10)]), a));
	  c7 = _mm_add_pd(c7, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 12)]), a));
	  c8 = _mm_add_pd(c8, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 14)]), a));
	}

      _mm_store_pd(&workspace.EPEix0[i + 0], c1);
      _mm_store_pd(&workspace.EPEix0[i + 2], c2);
      _mm_store_pd(&workspace.EPEix0[i + 4], c3);
      _mm_store_pd(&workspace.EPEix0[i + 6], c4);
      _mm_store_pd(&workspace.EPEix0[i + 8], c5);
      _mm_store_pd(&workspace.EPEix0[i + 10], c6);
      _mm_store_pd(&workspace.EPEix0[i + 12], c7);
      _mm_store_pd(&workspace.EPEix0[i + 14], c8);
    }

  nn = n - i;

  for(; i < n - nn % 14; i += 14)
    {
      __m128d c1 = _mm_setzero_pd(),
	c2 = _mm_setzero_pd(),
	c3 = _mm_setzero_pd(),
	c4 = _mm_setzero_pd(),
	c5 = _mm_setzero_pd(),
	c6 = _mm_setzero_pd(),
	c7 = _mm_setzero_pd();

      for(unsigned int j = 0; j < n; j++)
	{
	  __m128d a = _mm_load1_pd(workspace.PEix0 + j);

	  c1 = _mm_add_pd(c1, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 0)]), a));
	  c2 = _mm_add_pd(c2, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 2)]), a));
	  c3 = _mm_add_pd(c3, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 4)]), a));
	  c4 = _mm_add_pd(c4, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 6)]), a));
	  c5 = _mm_add_pd(c5, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 8)]), a));
	  c6 = _mm_add_pd(c6, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 10)]), a));
	  c7 = _mm_add_pd(c7, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 12)]), a));
	}

      _mm_store_pd(&workspace.EPEix0[i + 0], c1);
      _mm_store_pd(&workspace.EPEix0[i + 2], c2);
      _mm_store_pd(&workspace.EPEix0[i + 4], c3);
      _mm_store_pd(&workspace.EPEix0[i + 6], c4);
      _mm_store_pd(&workspace.EPEix0[i + 8], c5);
      _mm_store_pd(&workspace.EPEix0[i + 10], c6);
      _mm_store_pd(&workspace.EPEix0[i + 12], c7);
    }

  nn = n - i;

  for(; i < n - nn % 12; i += 12)
    {
      __m128d c1 = _mm_setzero_pd(),
	c2 = _mm_setzero_pd(),
	c3 = _mm_setzero_pd(),
	c4 = _mm_setzero_pd(),
	c5 = _mm_setzero_pd(),
	c6 = _mm_setzero_pd();

      for(unsigned int j = 0; j < n; j++)
	{
	  __m128d a = _mm_load1_pd(workspace.PEix0 + j);

	  c1 = _mm_add_pd(c1, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 0)]), a));
	  c2 = _mm_add_pd(c2, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 2)]), a));
	  c3 = _mm_add_pd(c3, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 4)]), a));
	  c4 = _mm_add_pd(c4, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 6)]), a));
	  c5 = _mm_add_pd(c5, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 8)]), a));
	  c6 = _mm_add_pd(c6, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 10)]), a));
	}

      _mm_store_pd(&workspace.EPEix0[i + 0], c1);
      _mm_store_pd(&workspace.EPEix0[i + 2], c2);
      _mm_store_pd(&workspace.EPEix0[i + 4], c3);
      _mm_store_pd(&workspace.EPEix0[i + 6], c4);
      _mm_store_pd(&workspace.EPEix0[i + 8], c5);
      _mm_store_pd(&workspace.EPEix0[i + 10], c6);
    }

  nn = n - i;

  for(; i < n - nn % 8; i += 8)
    {
      __m128d c1 = _mm_setzero_pd(),
	c2 = _mm_setzero_pd(),
	c3 = _mm_setzero_pd(),
	c4 = _mm_setzero_pd();

      unsigned int j = 0;
      for(; j < n; j++)
	{
	  __m128d a = _mm_load1_pd(workspace.PEix0 + j);

	  c1 = _mm_add_pd(c1, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 0)]), a));
	  c2 = _mm_add_pd(c2, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 2)]), a));
	  c3 = _mm_add_pd(c3, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 4)]), a));
	  c4 = _mm_add_pd(c4, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 6)]), a));
	}

      _mm_store_pd(&workspace.EPEix0[i + 0], c1);
      _mm_store_pd(&workspace.EPEix0[i + 2], c2);
      _mm_store_pd(&workspace.EPEix0[i + 4], c3);
      _mm_store_pd(&workspace.EPEix0[i + 6], c4);
    }

  nn = n - i;

  for(; i < n - nn % 4; i += 4)
    {
      __m128d c1 = _mm_setzero_pd(),
	c2 = _mm_setzero_pd();

      for(unsigned int j = 0; j < n; j++)
	{
	  __m128d a = _mm_load1_pd(workspace.PEix0 + j);

	  c1 = _mm_add_pd(c1, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 0)]), a));
	  c2 = _mm_add_pd(c2, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 2)]), a));
	}

      _mm_store_pd(&workspace.EPEix0[i + 0], c1);
      _mm_store_pd(&workspace.EPEix0[i + 2], c2);
    }

  nn = n - i;

  for(; i < n - nn % 2; i += 2)
    {
      __m128d c1 = _mm_setzero_pd();

      for(unsigned int j = 0; j < n; j++)
	{
	  __m128d a = _mm_load1_pd(workspace.PEix0 + j);

	  c1 = _mm_add_pd(c1, _mm_mul_pd(_mm_load_pd(&workspace.evecs[j * NMAX + (i + 0)]), a));
	}

      _mm_store_pd(&workspace.EPEix0[i + 0], c1);
    }

  for(; i < n; i++)
    {
      __m128d c1 = _mm_setzero_pd();

      for(unsigned int j = 0; j < n; j++)
	{
	  __m128d a = _mm_load_sd(workspace.PEix0 + j);

	  c1 = _mm_add_sd(c1, _mm_mul_sd(_mm_load_sd(&workspace.evecs[j * NMAX + (i + 0)]), a));
	}

      _mm_store_sd(&workspace.EPEix0[i + 0], c1);
    }

  /*for(int j = 0; j < n; j++)
    {
    space[i] = 0.0;
    for(int i = 0; i < n; i++)
    {
    space[i] += workspace.evecs[j * NMAX + i] * workspace.PEix0[j];
    }
    }*/

  //ASSERT(0, "yearaeyrararyayregggg");

  //cblas_dgemv(CblasColMajor, CblasNoTrans, n, n, 1.0, workspace.evecs, NMAX, workspace.PEix0, 1, 0.0, workspace.EPEix0, 1);
  //const enum CBLAS_ORDER order, const enum CBLAS_TRANSPOSE TransA, const int M, const int N, const double alpha, const double * A, const int lda, const double * x, const int incx, const double beta, double * y, const int incy)
}

double CDF<Square2D>::evaluateF(double t, int r, Workspace &workspace)
{
  double sum = 2.0;

  for(int i = 0; i <= r; i++)
    {
      sum += 2.0 * (amd_exp(workspace.evals[2 * i] * t) + 1.0) * workspace.dens[i];
    }

  return sum;
}

double CDF<Square2D>::evaluateFp(double t, int r, Workspace &workspace)
{
  double sum = 0.0;

  for(int i = 0; i <= r; i++)
    {
      sum += 2.0 * (workspace.evals[2 * i] * exp(workspace.evals[2 * i] * t)) * workspace.dens[i];
    }

  return sum;
}

double CDF<Square2D>::evaluateFpp(double t, int r, Workspace &workspace)
{
  double sum = 0.0;

  for(int i = 0; i <= r; i++)
    {
      sum += 2.0 * (workspace.evals[2 * i] * workspace.evals[2 * i] * exp(workspace.evals[2 * i] * t)) * workspace.dens[i];
    }

  return sum;
}

CDF<Square2D>::CDF(double w, int RMAX) : w(w), RMAX(RMAX), T(1000), NMAX(2 * RMAX + 2)
{
  h = sqrt(1.0 / w);

  workspaces = new Workspace[RMAX + 1];

  for(int i = 0; i <= RMAX; i++)
    {
      initialize(i);
    }
}

CDF<Square2D>::~CDF()
{
  for(int i = 0; i <= RMAX; i++)
    {
      free(workspaces[i].evals);
      free(workspaces[i].evecs);
      free(workspaces[i].Eix0);
      free(workspaces[i].dens);
      free(workspaces[i].t);
      free(workspaces[i].Fp);
      free(workspaces[i].Fpp);
      free(workspaces[i].PEix0);
      free(workspaces[i].EPEix0);
    };

  delete [] workspaces;
}

CDF<Square2D>::CDF(const CDF<Square2D> &other) : CDF(other.w, other.RMAX)
{
}

double CDF<Square2D>::computeFirstExit(int r, RNG &rng, double tol, bool cache)
{
  return computeFirstExit(r, rng.getDouble(), tol, cache);
}

double CDF<Square2D>::computeFirstExit(int r, double rn, double tol, bool cache)
{
  int n = r * 2 + 1;
    
  rn = 1.0 - sqrt(1.0 - rn);
  
  Workspace &workspace = workspaces[r];

  //Done computing temporaries... Find walker's time of exit
  double tl = 0.0,
    tr = 1.0,
    vr = 0.0,
    vl = 0.0;

  double tm = 0.0, vm;

  double error = 1.0;

  if(n == 1)
    {
      tm = -log(1 - rn) / (2 * w);
    }
  else
    {
      int i = int(rn / (1.0 / double(T)) + 1.0);

      int k = 0;

      if(cache && i < T)
	{
	  vr = i * (1.0 / double(T));
	  vl = (i - 1) * (1.0 / double(T));

	  tr = workspace.t[i];
	  tl = workspace.t[i - 1];
	    
	  double a, b, c, det;

	  //The result of this interpolation failing is usually walkers landing on each other outside of this
	  //  code. I have no idea how it happens, but be warned!
	  if((rn - vl) < (vr - rn) && (i - 1) != 0)
	    {
	      a = 0.5 * workspace.Fpp[i - 1],
		b = workspace.Fp[i - 1],
		c = vl - rn;

	      det = b * b - 4 * a * c;

	      if(a != 0.0 && det > 0.0)
		{
		  double dt = (-b + sqrt(b * b - 4 * a * c)) / (2.0 * a);
		    
		  tm = tl + dt;
		}
	      else if(b != 0.0)
		{
		  tm = tl - c / b;
		}
	      else
		{
		  ASSERT(0, "We should never get here, but it'll probably be okay\n");
		}

	      //printf("* %e\n", rn - evaluateF(tm, r, workspace));
	    }
	  else
	    {
	      a = 0.5 * workspace.Fpp[i],
		b = -workspace.Fp[i],
		c = vr - rn;

	      det = b * b - 4 * a * c;

	      if(a != 0.0 && det > 0.0)
		{
		  double dt = (-b - sqrt(det)) / (2.0 * a);
		    
		  tm = tr - dt;
		}
	      else if(b != 0.0)
		{
		  tm = tr + c / b;
		}
	      else
		{
		  ASSERT(0, "We should never get here, but it'll probably be okay\n");
		}

	      //printf("** %e\n", rn - evaluateF(tm, r, workspace));
	    }

	  /*else
	    {
	    tm = tr;
	    vm = vr;
		
	    while(1)
	    {
	    if(rn > 0.0)
	    error = fabs((vm - rn) / vm);
	    else
	    error = fabs(vm - rn);
		
	    if(error < tol)
	    break;
		
	    double vmp = evaluateFp(tm, r, workspace);
		
	    tm = tm - (vm - rn) / vmp;
		
	    vm = evaluateF(tm, r, workspace);
	    }
	    }*/
	}
      else
	{
	  //Find upper bound on t
	  while(1)
	    {
	      ASSERT(k++ < 1000, "Exit time not converging");

	      vr = evaluateF(tr, r, workspace);
		
	      if(vr > rn)
		{
		  break;
		}
	      else
		{
		  tl = tr;
		  tr *= 2.0;
		}
	    }

	  tm = tl;
	  vm = vl;

	  //With bounds on t, iterate to satisfactory error
	  while(1)
	    {
	      ASSERT(k++ < 1000, "Exit time not converging");

	      if(rn > 0.0)
		error = fabs((vm - rn) / vm);
	      else
		error = fabs(vm - rn);
	  
	      if(error < tol)
		break;
	      else
		{
		  if(vm > rn)
		    {
		      tr = tm;
		      vr = vm;
		    }
		  else
		    {
		      tl = tm;
		    }
		}

	      tm = (tl + tr) / 2.0;
	    
	      vm = evaluateF(tm, r, workspace);
	    }
	}
    }

  return tm;
}

Index CDF<Square2D>::computeExitPosition(double t, int r, RNG &rng, bool early)
{
  int n = r * 2 + 1;

  Workspace &workspace = workspaces[r];

  EPEix0TableT::iterator itr = workspace.EPEix0Table.lower_bound(t), itl;
    
  if(itr != workspace.EPEix0Table.end() && itr != workspace.EPEix0Table.begin())
    {
      itl = itr;
      itl--;

      double r = (t - (*itl).first) / ((*itr).first - (*itl).first);

      for(int i = 0; i < n; i++)
	{
	  double vl = itl->second[i],
	    vr = itr->second[i];

	  workspace.EPEix0[i] = vl + r * (vr - vl);
	}
    }
  else
    {
      evaluate(t, n, workspace);
    }

  int val = select(rng.getDouble(), workspace.EPEix0, n);

  val -= r;

  Index idx;

  if(!early)
    {
      if(rng.getDouble() > 0.5)
	{
	  if(rng.getDouble() > 0.5)
	    {
	      idx = Index(r + 1, val);
	    }
	  else
	    {
	      idx = Index(-(r + 1), val);
	    }
	}
      else
	{
	  if(rng.getDouble() > 0.5)
	    {
	      idx = Index(val, r + 1);
	    }
	  else
	    {
	      idx = Index(val, -(r + 1));
	    }
	}
    }
  else
    {
      int val2 = select(rng.getDouble(), workspace.EPEix0, n);

      val2 -= r;

      idx = Index(val, val2);
    }

  return idx;
}

template class CDF<Square2D>;

/*Index tmpfunc()
  {
  CDF<Square2D> cdf(0.5);
  RNG rng;
  
  return cdf.computeExitPosition(cdf.computeFirstExit(3, 0.5), 3, rng);
  }*/

/*template<>
  double CDF<Square2D>::computeFirstExit(int, double, double, bool);

  template<>
  Index CDF<Square2D>::computeExitPosition(double, int, RNG&, bool);*/
