#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <thread>
#include <omp.h>
#include <tclap/CmdLine.h>

#include "util.hpp"
#include "defines.hpp"

#include "KMC.hpp"

const GridType GridT = Square2D;
typedef KMC<GridT> KMCT;

int main(int argc, char **argv)
{
  int R = 1, P = 1, N = 16, s = 0;

  double DoverF = 1.0e6;    // Diffusion rate vs flux constant (actually: (D/a^2)/(F*a^2) )
  double DedgeoverF = 1e3;    // Diffusion rate vs flux constant (actually: (D/a^2)/(F*a^2) )

  try
    {
      TCLAP::CmdLine cmd("KMC code", ' ', "0.0");

      TCLAP::ValueArg<int> RArg("r", "runs", "The number of runs in the simulation", false, 1, "unsigned int");
      cmd.add(RArg);

      TCLAP::ValueArg<int> PArg("p", "print", "The number of runs between printouts (not strictly followed -- depends on threading)", false, 1, "unsigned int");
      cmd.add(PArg);

      TCLAP::ValueArg<int> NArg("n", "grid", "The dimension of the grid (this must be a multiple of BLOCKSIZE (defined in WalkerGrid.hpp))", false, 16, "unsigned int");
      cmd.add(NArg);

      TCLAP::ValueArg<double> DoFArg("d", "dof", "D over F (D == 1)", false, 1.0e6, "double");
      cmd.add(DoFArg);

      TCLAP::ValueArg<double> DedgeoverFArg("e", "dedge", "Dedge over F", false, 1.0e3, "double");
      cmd.add(DedgeoverFArg);

      TCLAP::ValueArg<int> seedArg("s", "seed", "Seed offset. Runs are seeded as seed offset + run number", false, 0, "int");
      cmd.add(seedArg);

      // Parse the argv array.
      cmd.parse( argc, argv );

      R = RArg.getValue();
      P = PArg.getValue();
      N = NArg.getValue();
      DoverF = DoFArg.getValue();
      DedgeoverF = DedgeoverFArg.getValue();
      s = seedArg.getValue();

      std::cout << "Executing with R = " << R << std::endl << " P = "  << P << std::endl << " N = "  << N << std::endl << " DoverF = "  << DoverF << std::endl << " DedgeoverF = "  << DedgeoverF << std::endl << " s = "  << s << std::endl;
    }
  catch (TCLAP::ArgException &e)  // catch any exceptions
    {
      std::cerr << "error with input arguments: " << e.error() << " for arg " << e.argId() << std::endl;
      return -1;
    }

  ASSERT(DoverF != 0.0, "DoverF cannot be equal to zero");

  const double a = 1.0;           // lattice constant
  const double D = 1.0;           // Surface Diffusion rate
  const double F = (D / DoverF);        // rate of influx of walkers onto surface (per lattice square per second)
  double Dedge = F * DedgeoverF;
  double Dbreak = 0.0 * D;
  double Dup = 0.0;
  double Ddown = 0.0;
  double Dcorner = Dedge / 100.0;

  double C = 0.10;

  double initTime = omp_get_wtime();

  KMCT *kmcr = new KMCT(N, a, F, Dedge, Dbreak, Dup, Ddown, Dcorner, D,
			C, 0, s + rand());

  KMCT *kmcrr = new KMCT(N, a, F, Dedge, Dbreak, Dup, Ddown, Dcorner, D,
			 C, 0, s + rand());

  KMCT *kmc = new KMCT(N, a, F, Dedge, Dbreak, Dup, Ddown, Dcorner, D,
		       C, 7, s + rand());

  initTime = omp_get_wtime() - initTime;

  double time = omp_get_wtime();

  DepositCounter<GridT> depositCounter,
    refDepositCounter,
    ref2DepositCounter;

  for(int r = 0; r < R; r += P)
    {
      std::thread t2, t3;

      char filename[500],
	refFilename[500],
	ref2Filename[500];
      
      std::fstream stream,
	refStream,
	ref2Stream;

      sprintf(filename, "output.%d.%d.%d.%d.exp.txt", N, s, r, P);
      kmc->restart(r);
      stream.open(filename, std::fstream::out | std::fstream::trunc);
      std::thread t1(std::bind(&KMCT::run, std::ref(*kmc), P));

      sprintf(refFilename, "output.%d.%d.%d.%d.ref.txt", N, s, r, P);      
      refStream.open(refFilename, std::fstream::in);
      refStream.peek();
      if(!refStream.good())
	{
	  kmcr->restart(r);
	  t2 = std::thread(std::bind(&KMCT::run, std::ref(*kmcr), P));
	}

      sprintf(ref2Filename, "output.%d.%d.%d.%d.ref2.txt", N, s, r, P);
      ref2Stream.open(ref2Filename, std::fstream::in);
      ref2Stream.peek();
      if(!ref2Stream.good())
	{
	  kmcrr->restart(r);
	  t3 = std::thread(std::bind(&KMCT::run, std::ref(*kmcrr), P));
	}

      t1.join();

      if(t2.joinable())
	t2.join();

      if(t3.joinable())
	t3.join();

      depositCounter.accumulate(kmc->depositCounter);

      stream << kmc->depositCounter;

      if(!refStream.good())
	{
	  refDepositCounter.accumulate(kmcr->depositCounter);

	  refStream.close();
	  refStream.open(refFilename, std::fstream::out | std::fstream::trunc);
	  refStream << kmcr->depositCounter;
	}
      else
	{
	  DepositCounter<GridT> tmp;

	  refStream >> tmp;

	  refDepositCounter.accumulate(tmp);
	}

      if(!ref2Stream.good())
	{
	  ref2DepositCounter.accumulate(kmcrr->depositCounter);

	  ref2Stream.close();
	  ref2Stream.open(ref2Filename, std::fstream::out | std::fstream::trunc);
	  ref2Stream << kmcrr->depositCounter;
	}
      else
	{
	  DepositCounter<GridT> tmp;

	  ref2Stream >> tmp;

	  ref2DepositCounter.accumulate(tmp);
	}

      stream.close();
      refStream.close();
      ref2Stream.close();

      sprintf(filename, "output.%d.%d.%d.%d.sum.exp.txt", N, s, r, P);
      stream.open(filename, std::fstream::out | std::fstream::trunc);
      stream << depositCounter;
      stream.close();

      sprintf(refFilename, "output.%d.%d.%d.%d.sum.ref.txt", N, s, r, P);
      refStream.open(refFilename, std::fstream::out | std::fstream::trunc);
      refStream << refDepositCounter;
      refStream.close();

      sprintf(ref2Filename, "output.%d.%d.%d.%d.sum.ref2.txt", N, s, r, P);
      ref2Stream.open(ref2Filename, std::fstream::out | std::fstream::trunc);
      ref2Stream << ref2DepositCounter;
      ref2Stream.close();

      std::cout << r + P << "/" << R << ", ";
      
      std::cout << "Reference: " << refDepositCounter.distance(ref2DepositCounter) << ", Experimental: " << refDepositCounter.distance(depositCounter) << std::endl;
    }
  time = omp_get_wtime() - time;

  printf("Init took %fs\nRun took %fs\n", initTime, time);

  delete kmc;
  delete kmcr;
  delete kmcrr;
}
