#include "WalkerGrid.hpp"

template<>
const std::set<Index> &WalkerGrid<Triangle>::getStencil(Index index)
{
  if(index.getl() % 2 == 0)
    {
      static std::set<Index> zeroSet =
	{
	  Index(-1, 0, 0),
	  Index(-1, 1, 0),
	  Index(0, 1, 0),
	  
	  Index(1, 0, 0),
	  Index(1, -1, 0),
	  Index(0, -1, 0),
	  
	  Index(-1, 1, -1),
	  Index(1, -1, -1),
	  Index(1, 1, -1),
	  
	  Index(-1, 1, 1),
	  Index(1, -1, 1),
	  Index(1, 1, 1)
	};

      return zeroSet;
    }
  else
    {
      static std::set<Index> oneSet =
	{
	  Index(-1, 0, 0),
	  Index(-1, 1, 0),
	  Index(0, 1, 0),

	  Index(1, 0, 0),
	  Index(1, -1, 0),
	  Index(0, -1, 0),

	  Index(1, -1, -1),
	  Index(-1, 1, -1),
	  Index(-1, -1, -1),

	  Index(1, -1, 1),
	  Index(-1, 1, 1),
	  Index(-1, -1, 1)
	};

      return oneSet;
    }
}
