#include <stdio.h>

#include <gsl/gsl_cblas.h>
//#include <amdlibm.h>

#include "RNG.hpp"
#include "EdgeCDF.hpp"

static void handler (const char * reason,
		     const char * file,
		     int line,
		     int gsl_errno)
{
  std::cout << "GSL found error: " << reason << " " << file << ":" << line << std::endl;
}

static int func(double t, const double *y, double *f, void *params)
{
  int n = (*((EdgeCDF<Triangle>::ode_params *)params)).n;
  double *A = (*((EdgeCDF<Triangle>::ode_params *)params)).A;

  cblas_dgemv(CblasRowMajor, CblasNoTrans, n, n, 1.0, A, n, y, 1, 0.0, f, 1);  

  return GSL_SUCCESS;
}

static int jac(double t, const double *y, double *dfdy, double *dfdt, void *params)
{
  int n = (*((EdgeCDF<Triangle>::ode_params *)params)).n;
  double *A = (*((EdgeCDF<Triangle>::ode_params *)params)).A;

  for(int i = 0; i < n * n; i++)
    dfdy[i] = A[i];

  for(int i = 0; i < n; i++)
    dfdt[i] = 0.0;

  return GSL_SUCCESS;
}

typename EdgeCDF<Triangle>::Workspace EdgeCDF<Triangle>::initialize(int r, int corner)
{
  int n = 2 * r + 3;

  //Compute all the temporaries
  double *x0, *A, t = 0.0, h = 1e-6;
  ode_params *params = (ode_params *)calloc(1, sizeof(ode_params));
  x0 = (double *)calloc(n, sizeof(double));
  A = (double *)calloc(n * n, sizeof(double));
  
  params->n = n;
  params->A = A;

  const gsl_odeiv2_step_type *T = gsl_odeiv2_step_rk2; //gsl_odeiv2_step_bsimp
  gsl_odeiv2_step *s = gsl_odeiv2_step_alloc(T, n);
  gsl_odeiv2_control *c = gsl_odeiv2_control_y_new(1e-8, 1e-6);
  gsl_odeiv2_evolve *e = gsl_odeiv2_evolve_alloc(n);
  gsl_odeiv2_system sys = { func, jac, (size_t)n, params };

  for(int i = 0; i < n; i++)
    {
      double flux = 0.0;

      if(((i - 1) == corner || i == corner) && corner != -1)
	{
	  if(i > 1)
	    A[i * n + (i - 1)] = wcorner;
	  flux += wcorner;
	}
      else
	{
	  if(i > 1)
	    A[i * n + (i - 1)] = w;
	  flux += w;
	}

      if((i == corner || (i + 1) == corner) && corner != -1)
	{
	  if(i < n - 2)
	    A[i * n + (i + 1)] = wcorner;
	  flux += wcorner;
	}
      else
	{
	  if(i < n - 2)
	    A[i * n + (i + 1)] = w;
	  flux += w;
	}

      if(i != 0 && i != n - 1)
	A[i * n + i] = -flux;
    }

  Workspace workspace;

  for(int i = 0; i < n; i++)
    x0[i] = 0;
  x0[(n - 1) / 2] = 1;

  workspace.x.insert(std::map<double, double *>::value_type(0.0, x0));
  workspace.F.insert(std::map<double, double>::value_type(0.0, 0.0));
  workspace.A = A;
  workspace.t = t;
  workspace.h = h;
  workspace.params = params;
  workspace.s = s;
  workspace.c = c;
  workspace.e = e;
  workspace.sys = sys;

  return workspace;
}

void EdgeCDF<Triangle>::evaluate(double xF, double xt, int r, Workspace &workspace)
{
  double F = workspace.F.rbegin()->first, t = workspace.t;//F.rbegin()->second

  if(workspace.t != workspace.F.rbegin()->second)
    {
      printf("xt = %f, workspace.t = %f, workspace.F.rbegin()->second = %f\n", xt, workspace.t, workspace.F.rbegin()->second);
      ASSERT(0, " ");
    }

  int n = 2 * r + 3;

  double *xc = (*workspace.x.rbegin()).second;

  while(!(F >= xF || t >= xt))
    {
      double *x = (double *)malloc(n * sizeof(double));

      memcpy(x, xc, n * sizeof(double));

      double ot = workspace.t;
      double oxt = xt;

      if(DEBUG && (workspace.t >= xt))
	{
	  std::cout << "xt = " << xt << ", t = " << t << ", workspace.t = " << workspace.t << ", ot = " << ot << ", oxt = " << oxt << " h = " << workspace.h << std::endl;

	  fflush(stdout);
	}

      //workspace.h = std::min(workspace.h, 0.001);
      ASSERT(gsl_odeiv2_evolve_apply(workspace.e, workspace.c, workspace.s, &workspace.sys, &workspace.t, xt, &workspace.h, x) == GSL_SUCCESS, "ODE Solver faile!!!\nt = %f, xt = %f, ot = %f, oxt = %f, workspace.t = %f, %f\n", t, xt, ot, oxt, workspace.t, workspace.h);

      /*F = 1.0;

      for(int i = 1; i < n - 1; i++)
      F -= x[i];*/

      F = x[0] + x[n - 1];

      //printf("Evaluated: t = %f, F = %f, r = %d, ", t, F, r);

      /*if(xF < 1e10)
	printf("xF = %f\n", xF);

      if(xt < 1e10)
      printf("xt = %f\n", xt);*/

      t = workspace.t;

      //workspace.h = 0.01;
      ASSERT(workspace.x.insert(std::map<double, double *>::value_type(workspace.t, x)).second, "Trying to save the same state twice");
      ASSERT(workspace.F.insert(std::map<double, double>::value_type(F, workspace.t)).second || F == 0.0, "Trying to save the same value of F twice");

      xc = x;
    }
}

EdgeCDF<Triangle>::EdgeCDF(double w, double wcorner, int RMAX) : w(w), wcorner(wcorner), RMAX(RMAX)
{
  gsl_set_error_handler(handler);

  workspaces.resize(RMAX + 1);

  for(int i = 0; i <= RMAX; i++)
    {
      workspaces[i].resize(i * 2 + 4, Workspace());
    }

  for(int i = 0; i <= RMAX; i++)
    {
      for(int j = 0; j < i * 2 + 3; j++)
	{
	  workspaces[i][j] = initialize(i, j);
	}

      workspaces[i][i * 2 + 3] = initialize(i, -1);
    }
}

EdgeCDF<Triangle>::EdgeCDF(const EdgeCDF<Triangle> &other) : EdgeCDF(other.w, other.wcorner, other.RMAX)
{
}

EdgeCDF<Triangle>::~EdgeCDF()
{
  for(int i = 0; i <= RMAX; i++)
    {
      for(int j = 0; j < i * 2 + 4; j++)
	{
	  Workspace &workspace = workspaces[i][j];

	  std::map<double, double *> &x = workspace.x;

	  for(std::map<double, double *>::iterator it = x.begin(); it != x.end(); it++)
	    {
	      free((*it).second);
	    }
	  
	  free(workspace.A);
	  
	  free(workspace.params);
	  
	  gsl_odeiv2_evolve_free(workspace.e);
	  gsl_odeiv2_control_free(workspace.c);
	  gsl_odeiv2_step_free(workspace.s);
	}
    }
}

double EdgeCDF<Triangle>::computeFirstExit(int r, int corner, RNG &rng)
{
  return computeFirstExit(r, corner, rng.getDouble());
}

double EdgeCDF<Triangle>::computeFirstExit(int r, int corner, double rn)
{
  ASSERT(r <= RMAX, "r > RMAX");
  ASSERT(corner < r * 2 + 3, "Corner index out of range");

  corner = (corner == -1) ? r * 2 + 3 : corner;

  Workspace &workspace = workspaces[r][corner];

  //Run until we have information about F
  evaluate(rn, DBL_MAX, r, workspace);

  std::map<double, double>::iterator itr, itl = workspace.F.lower_bound(rn);
  itr = itl--;

  //Done computing temporaries... Find walker's time of exit
  double tl = itl->second,
    Fl = itl->first,
    tr = itr->second,
    Fr = itr->first;

  if((Fr - Fl) != 0.0)
    {
      return tl + (tr - tl) * (rn - Fl) / (Fr - Fl);
    }
  else
    {
      printf("yeargh\n");
      return tl;
    }
}

int EdgeCDF<Triangle>::computeExitPosition(double t, int r, int corner, RNG &rng, bool early)
{
  int n = 2 * r + 3;

  ASSERT(r <= RMAX, "r > RMAX");
  ASSERT(corner < r * 2 + 3, "Corner index out of range");

  corner = ((corner == -1) ? r * 2 + 3 : corner);

  Workspace &workspace = workspaces[r][corner];

  evaluate(DBL_MAX, t, r, workspace);

  double *array = (double *)malloc(n * sizeof(double)),
    *dydt = (double *)malloc(n * sizeof(double));

  std::map<double, double *>::iterator itl = workspace.x.lower_bound(t), itr;
  itr = itl--;

    double tl = itl->first;
//  double tr = itr->first;

  double ts = tl;

  //double v = (t - tl) / (tr - tl);

  for(int i = 0; i < n; i++)
    {
      array[i] = itl->second[i]/* + (itr->second[i] - itl->second[i]) * v*/;
    }

  ASSERT(gsl_odeiv2_evolve_apply(workspace.e, workspace.c, workspace.s, &workspace.sys, &ts, t, &workspace.h, array) == GSL_SUCCESS, "Ode solver faile!!!");

  /*printf("r = %d, corner = %d, t = %f, v = %f, tl = %f, tr = %f\n", r, corner, t, v, tl, tr);

  printf("L: ");
  for(int i = 0; i < n; i++)
    {
      printf("%f ", itl->second[i]);
    }

  printf("\nR: ");
  for(int i = 0; i < n; i++)
    {
      printf("%f ", itr->second[i]);
    }

  printf("\nLinear: ");
  for(int i = 0; i < n; i++)
    {
      printf("%f ", (itl->second[i] + v * (itr->second[i] - itl->second[i])));
    }
  printf("\nODE: ");

  for(int i = 0; i < n; i++)
    {
      printf("%f ", array[i]);
    }
    printf("\n");*/

  int i = 0;

  if(DEBUG)
    {
      for(int tmp = 0; tmp < n; tmp++)
	{
	  std::cout << array[tmp] << ", ";
	}
      
      std::cout << std::endl;
    }
  
  if(DEBUG)
    for(int i = 0; i < n; i++)
      {
	for(int j = 0; j < n; j++)
	  {
	    std::cout << workspace.A[i * n + j] << " ";
	  }
	std::cout << std::endl;
      }
  
  if(early)
    {
      i = select(rng.getDouble(), array + 1,  n - 2) + 1;
    }
  else
    {
      func(0.0, array, dydt, workspace.params);

      double dFl = dydt[0];
      double dFr = dydt[n - 1];

      double sum = dFl + dFr;
      double rn = rng.getDouble();
      double v = rn * sum;
      
      if(DEBUG)
	std::cout << "rn = " << rn << " v = " << v << " sum = " << sum << " dFl = " << dFl << " dFr = " << dFr << std::endl;

      if(v < dFl)
	{
	  i = 0;
	}
      else
	{
	  i = n - 1;
	}
    }

  free(array);
  free(dydt);

  return i;
}
