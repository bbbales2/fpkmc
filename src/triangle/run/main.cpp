#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <unistd.h>
#include <thread>
#include <fstream>
#include <omp.h>
#include <tclap/CmdLine.h>
#include <sys/stat.h>
#include <type_traits>

#include "util.hpp"
#include "defines.hpp"

#include "KMC.hpp"

#define GridT Triangle

typedef KMC<GridT> KMCT;

template <typename T>
void writeValueToFile(T value, const char *filename)
{
  std::fstream file(filename, std::fstream::out | std::fstream::trunc);
  
  file << std::scientific << value;
  
  file.close();
};

/*void writeValueToFile(double value, const char *filename)
{
  std::fstream file(filename, std::fstream::out | std::fstream::trunc);
  
  file << std::scientific << value;
  
  file.close();
};

void writeValueToFile(std::string value, const char *filename)
{
    std::fstream file(filename, std::fstream::out | std::fstream::trunc);
    file << value;
    file.close();
    }*/


int main(int argc, char **argv)
{
  int R = 1, P = 1, N = 16, s = 0, RMAX = 7;

  int frames = 1;
  double C = 0.1;

  std::string outDir;
  std::string checkPtFile;

  double D_Ag = 1.0;           // Surface Diffusion rate
  double D_Au = 1.0;           // Surface Diffusion rate
  double FonM_Ag = 1.0;        // rate of influx of walkers onto surface (per lattice square per second)
  double FonM_Au = 1.0;        // rate of influx of walkers onto surface (per lattice square per second)
  double FonSi_Ag = 1.0;        // rate of influx of walkers onto surface (per lattice square per second)
  double Dedge_Ag = 1.0;
  double Ddown_Ag = 1.0;
  double FonSi_Au = 1.0;        // rate of influx of walkers onto surface (per lattice square per second)
  double Dedge_Au = 1.0;
  double Ddown_Au = 1.0;
  
  try
    {
      TCLAP::CmdLine cmd("KMC code", ' ', "0.0");

      TCLAP::ValueArg<int> RArg("r", "runs", "The number of runs in the simulation", false, 1, "unsigned int");
      cmd.add(RArg);

      TCLAP::ValueArg<int> PArg("p", "print", "The number of runs between printouts (not strictly followed -- depends on threading)", false, 1, "unsigned int");
      cmd.add(PArg);

      TCLAP::ValueArg<int> NArg("n", "grid", "The dimension of the grid (this must be a multiple of BLOCKSIZE (defined in WalkerGrid.hpp))", false, 16, "unsigned int");
      cmd.add(NArg);

      TCLAP::ValueArg<double> D_AgArg("", "D_Ag", "D", false, 1, "double");
      cmd.add(D_AgArg);

      TCLAP::ValueArg<double> FonM_AgArg("", "FonM_Ag", "F", false, 1e-4, "double");
      cmd.add(FonM_AgArg);

      TCLAP::ValueArg<double> FonSi_AgArg("", "FonSi_Ag", "F", false, 1e-7, "double");
      cmd.add(FonSi_AgArg);

      TCLAP::ValueArg<double> Dedge_AgArg("", "Dedge_Ag", "Dedge", false, 1e-2, "double");
      cmd.add(Dedge_AgArg);

      TCLAP::ValueArg<double> Ddown_AgArg("", "Ddown_Ag", "Ddown", false, 1e0, "double");
      cmd.add(Ddown_AgArg);

      TCLAP::ValueArg<double> D_AuArg("", "D_Au", "D", false, 1, "double");
      cmd.add(D_AuArg);

      TCLAP::ValueArg<double> FonM_AuArg("", "FonM_Au", "F", false, 0, "double");
      cmd.add(FonM_AuArg);

      TCLAP::ValueArg<double> FonSi_AuArg("", "FonSi_Au", "F", false, 0, "double");
      cmd.add(FonSi_AuArg);

      TCLAP::ValueArg<double> Dedge_AuArg("", "Dedge_Au", "Dedge", false, 0, "double");
      cmd.add(Dedge_AuArg);

      TCLAP::ValueArg<double> Ddown_AuArg("", "Ddown_Au", "Ddown", false, 1e0, "double");
      cmd.add(Ddown_AuArg);

      TCLAP::ValueArg<int> seedArg("s", "seed", "Seed offset. Runs are seeded as seed offset + run number", false, 0, "int");
      cmd.add(seedArg);

      TCLAP::ValueArg<double> CArg("", "C", "Coverage", false, 0.1, "double");
      cmd.add(CArg);

      TCLAP::ValueArg<int> framesArg("f", "frames", "Number of distinct outputs to generate", false, 1, "int");
      cmd.add(framesArg);

      TCLAP::ValueArg<int> RMAXArg("m", "RMAX", "The maximum radius of the walkers", false, 7, "int");
      cmd.add(RMAXArg);

      TCLAP::ValueArg<std::string> CheckPtFile("","CheckPt", "Start simulation from a checkpoint file (MUST be relative to executable!!)", false, "NONE", "String");
      cmd.add(CheckPtFile);

      TCLAP::UnlabeledValueArg<std::string> outDirArg("outDir", "The output directory in which data is stored,", true, "", "String");
      cmd.add( outDirArg );

      // Parse the argv array.
      cmd.parse( argc, argv );

      R = RArg.getValue();
      P = PArg.getValue();
      N = NArg.getValue();

      D_Ag = D_AgArg.getValue();
      FonM_Ag = FonM_AgArg.getValue();
      FonSi_Ag = FonSi_AgArg.getValue();
      Dedge_Ag = Dedge_AgArg.getValue();
      Ddown_Ag = Ddown_AgArg.getValue();

      D_Au = D_AuArg.getValue();
      FonM_Au = FonM_AuArg.getValue();
      FonSi_Au = FonSi_AuArg.getValue();
      Dedge_Au = Dedge_AuArg.getValue();
      Ddown_Au = Ddown_AuArg.getValue();

      s = seedArg.getValue();
      C = CArg.getValue();
      outDir = outDirArg.getValue();
      frames = framesArg.getValue();
      RMAX = RMAXArg.getValue();
    
      // do some magic and find the path where program is executed (only needed if a checkpointfile is read)
      checkPtFile = CheckPtFile.getValue();
      if (checkPtFile.compare("NONE")){
	std::string thePath(getcwd(NULL,0));
	checkPtFile = (thePath + "/" + checkPtFile);
      }


      ASSERT(frames > 0, "--frames must be positive");
      ASSERT(s >= 0, "--seed must be positive or zero");
      ASSERT(C > 0.0, "--C must be positive");

      std::cout << "Executing with R = " << R << std::endl << " P = "  << P << std::endl << " N = "  << N << std::endl << " D = "  << D_Ag << " " << D_Au << std::endl << "FonM = " << FonM_Ag << " " << FonM_Au << "- FonSi = " << FonSi_Ag << " " << FonSi_Au << std::endl << " Dedge = " << Dedge_Ag << " " << Dedge_Au << std::endl << " s = "  << s << std::endl << " RMAX = " << RMAX << std::endl << " CheckPointFile = " << checkPtFile <<std::endl;
    }
  catch (TCLAP::ArgException &e)  // catch any exceptions
    {
      std::cerr << "error with input arguments: " << e.error() << " for arg " << e.argId() << std::endl;
      return -1;
    }


  const double a = 1.0;           // lattice constant
  double Dbreak = 0.0 * D_Ag;
  double Dup = 0.0 * D_Ag;
  double Dcorner_Ag = Dedge_Ag;
  double Dcorner_Au = Dedge_Au;

  std::cout << "C = " << C << std::endl;

  double initTime = omp_get_wtime();

  KMCT kmc(N, a, FonM_Ag, FonSi_Ag, Dedge_Ag, Dbreak, Dup, Ddown_Ag, Dcorner_Ag, D_Ag,
	   FonM_Au, FonSi_Au, Dedge_Au, Dbreak, Dup, Ddown_Au, Dcorner_Au, D_Au, RMAX, s, false, checkPtFile);

  initTime = omp_get_wtime() - initTime;

  double outTime = 0.0;
  double time = omp_get_wtime();

  ASSERT(0 == mkdir(outDir.c_str(), S_IREAD | S_IWRITE | S_IEXEC | S_IRGRP), "Error making directory");
  ASSERT(0 == chdir(outDir.c_str()), "Failed to chdir into directory %s", outDir.c_str());

  //Write each parameter to an aptly named file in the output directory
  {
    writeValueToFile(N, "N" );
    writeValueToFile(GridT, "grid" );
    writeValueToFile(R, "R");
    writeValueToFile(frames, "frames");
    writeValueToFile(C, "C");
    writeValueToFile(D_Ag, "DAg");
    writeValueToFile(s, "seed");
    writeValueToFile(FonM_Ag, "FonMAg");
    writeValueToFile(FonSi_Ag, "FonSiAg");
    writeValueToFile(Dedge_Ag, "Dedge_Ag");
    writeValueToFile(Ddown_Ag, "Ddown_Ag");
    writeValueToFile(D_Au, "DAu");
    writeValueToFile(FonM_Au, "FonMAu");
    writeValueToFile(FonSi_Au, "FonSiAu");
    writeValueToFile(Dedge_Au, "DedgeAu");
    writeValueToFile(Ddown_Au, "DdownAu");
    writeValueToFile(checkPtFile, "checkPointFile");
  }

  for( int r = 0 ; r < R ; )
    {
      std::string runDir = std::string("run.") + std::to_string(r);

      ASSERT(0 == mkdir(runDir.c_str(), S_IREAD | S_IWRITE | S_IEXEC | S_IRGRP), "Error making directory");
      ASSERT(0 == chdir(runDir.c_str()), "Failed to chdir into directory %s", runDir.c_str());

      std::fstream times("times", std::fstream::out | std::fstream::trunc);

      printf("frames %d\n", frames);
      for( int f = 0; f < frames; f++ )
	{
	  printf("f %d\n", f);

	  kmc.run(C / double(frames));

          times << kmc.getTime() << std::endl;

	  double tmp = omp_get_wtime();
	  
	  DepositCounter<GridT> depositCounter;
	  
	  std::string filename = std::string("frame.") + std::to_string(f) + std::string(".dat");
	  std::fstream stream(filename.c_str(), std::fstream::out | std::fstream::trunc);
	  
	  depositCounter.accumulate(kmc.depositGrid);
	  
	  stream << depositCounter;
	  
	  stream.close();
	  
	  outTime += omp_get_wtime() - tmp;
	}

      times.close();

      ASSERT(0 == chdir("../"), "Failed to chdir back up a directory");
      
      kmc.depositCounter.clear();
      kmc.reset();

      r++;

      printf("%d/%d\n", r, R);
    }

  ASSERT(0 == chdir("../"), "Failed to chdir up a directory");

  time = omp_get_wtime() - time;

  printf("Init took %fs\nRun took %fs\nPrint out took %fs\n", initTime, time, outTime);
}
