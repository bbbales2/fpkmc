#include "DepositGrid.hpp"

static inline int elements_on_level(int r)
{
  return r / 2 + 1;
}

template<>
const std::set<Index> &DepositGrid<Triangle>::getFloorStencil(Index index)
{
  if(index.getl() % 2 == 0)
    {
      static std::set<Index> iset =
	{
	  Index(0, 0, 0), 
	  Index(1, 0, 0),
	  Index(0, 1, 0)
	};

      return iset;
    }
  else
    {
      static std::set<Index> iset =
	{
	  Index(0, 0, 0), 
	  Index(-1, 0, 0),
	  Index(0, -1, 0)
	};

      return iset;
   }
}

template<>
typename DepositGrid<Triangle>::Distance DepositGrid<Triangle>::rebuildEdge(Layer<Distance> &dLayer, Index index)
{
  Distance distance(0, 0);

  bool supported = true;

  //int oo;

  const std::set<Index> &stencil = getFloorStencil(index + Index(0, 0, 1));

  if(DEBUG) std::cout << "Rebuilding edge at " << index << std::endl;

  int r = 0;
  
  Type sourceType = checkFloorType(index);

  for(r = 0; r <= RMAX && supported; )
    {
      if(DEBUG) std::cout << "For r = " << r << std::endl;

      Index base[6] = { Index(-r, 0),
			Index(-r, r),
			Index(0, r),
			Index(r, 0), 
			Index(r, -r),
			Index(0, -r) };
      
      Index theta_offsets[6] = { Index(0, 1),
				 Index(1, 0),
				 Index(1, -1),
				 Index(0, -1),
				 Index(-1, 0),
				 Index(-1, 1) };

      for(int i = 0; i < 6 || (r == 0 && i < 1); i++)
	{
	  int e = elements_on_level(r);
	  
	  int ee = 2 * e;
	  
	  if(r % 2 == 0)
	    ee--;
	  
	  for(int t = 0; t < ee; t++)
	    {
	      Index offset = base[i] + theta_offsets[i] * t;
	      
	      for(std::set<Index>::iterator it = stencil.begin(); it != stencil.end(); it++)
		{
		  Index stencilIdx = adjust(index + offset + *it, N);
		  
		  Type destType = checkFloorType(stencilIdx);

		  Distance distance = dLayer(stencilIdx.getn(), stencilIdx.getm());
		  
		  //if(DEBUG) std::cout << "Distance (" << (int)distance.dn << ", " << (int)distance.dm << ") for " << stencilIdx << std::endl;
		  
		  if(distance.dn != 0 || distance.dm != 0 || destType != sourceType)
		    {
		      supported = false;
		      break;
		    }
		}
	    }
	}
      
      if(DEBUG) std::cout << std::endl;

      if(supported)
	r++;
    }

  return Distance(r, 0);
}

template<>
bool DepositGrid<Triangle>::isAvailable(Index index)
{
  if(index.getl() < 0)
    return false;

  Distance distance = getDistanceToEdge(index - Index(0, 0, 1));

  if(distance.dn == 0 && distance.dm == 0)
    {
      return false;
    }
  else
    {
      distance = getDistanceToDeposit(index);
	
      if(distance.dn == 0 && distance.dm == 0)
	{
	  return false;
	}
      else
	{
	  return true;
	}
    }
}
