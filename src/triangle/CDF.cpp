
#include <stdio.h>

#include <gsl/gsl_cblas.h>

#include "RNG.hpp"
#include "CDF.hpp"

static int elements_on_level(int r)
{
  return r / 2 + 1;
}

static int elements(int r)
{
  int n = 0;

  for(int i = 0; i <= r + 1; i++)
    {
      n += i / 2 + 1;
    }

  return n;  
}

static int level_offset(int r)
{
  int sum = 0;
  
  for(int i = 0; i < r; i++)
    {
      sum += elements_on_level(i);
    };
  
  return sum;
}

static int index(int r, int t)
{
  int e = elements_on_level(r);

  if(t >= e)
    {
      if(r % 2 == 1)
	t = e - 1;
      else
	t = e - 2;
    }

  if(t == -1)
    {
      if(r == 1)
	t = 0;
      else
	t = 1;
    }

  return level_offset(r) + t;
}

static int func(double t, const double *y, double *f, void *params)
{
  int n = (*((CDF<Triangle>::ode_params *)params)).n;
  double *A = (*((CDF<Triangle>::ode_params *)params)).A;

  cblas_dgemv(CblasRowMajor, CblasNoTrans, n, n, 1.0, A, n, y, 1, 0.0, f, 1);  

  return GSL_SUCCESS;
}

static int jac(double t, const double *y, double *dfdy, double *dfdt, void *params)
{
  int n = (*((CDF<Triangle>::ode_params *)params)).n;
  double *A = (*((CDF<Triangle>::ode_params *)params)).A;

  for(int i = 0; i < n * n; i++)
    dfdy[i] = A[i];

  for(int i = 0; i < n; i++)
    dfdt[i] = 0.0;

  return GSL_SUCCESS;
}

void CDF<Triangle>::initialize(int r)
{
  int n = elements(r);

  //Compute all the temporaries
  double *x0, *A, *factors, t = 0.0, h = 1e-6;
  ode_params *params = (ode_params *)calloc(1, sizeof(ode_params));
  x0 = (double *)calloc(n, sizeof(double));
  A = (double *)calloc(n * n, sizeof(double));
  factors = (double *)calloc(n, sizeof(double));
  
  params->n = n;
  params->A = A;

  const gsl_odeiv2_step_type *T = gsl_odeiv2_step_rk2;
  gsl_odeiv2_step *s = gsl_odeiv2_step_alloc(T, n);
  gsl_odeiv2_control *c = gsl_odeiv2_control_y_new(1e-6, 0.0);
  gsl_odeiv2_evolve *e = gsl_odeiv2_evolve_alloc(n);
  gsl_odeiv2_system sys = {func, jac, (size_t)n, params};

  /*ASSERT(0, "");*/
  for(int i = 0; i <= r + 1; i++)
    {
      int e = elements_on_level(i);
      for(int t = 0; t < e; t++)
	{
	  if(i > 0)
	    {
	      if(t == 0 || (t == e - 1 && i % 2 == 0))
		factors[index(i, t)] = 6;
	      else		
		factors[index(i, t)] = 12;
	    }
	  else
	    factors[index(i, t)] = 1;

	  if(i <= r)
	    {
	      A[index(i, t) * n + index(i, t)] = -6;
	    }

	  if(i == 0)
	    {
	      if(i + 1 <= r)
		A[index(i, t) * n + index(i + 1, t)] = 6;
	    }
	  else if(t == 0)
	    {
	      /*A[index(i, t) * n + index(i - 1, t)] += 6;
	      
	      if(i <= r)
		A[index(i, t) * n + index(i, t - 1)] += 6;

	      if(i <= r)
		A[index(i, t) * n + index(i, t + 1)] += 6;
	  
	      if(i + 1 <= r)
		A[index(i, t) * n + index(i + 1, t - 1)] += 6;

	      if(i + 1 <= r)
		A[index(i, t) * n + index(i + 1, t)] += 6;

	      if(i + 1 <= r)
	      A[index(i, t) * n + index(i + 1, t + 1)] += 6;*/
	      A[index(i, t) * n + index(i - 1, t)] += 1;
	      
	      if(i <= r)
		A[index(i, t) * n + index(i, t - 1)] += 1;

	      if(i <= r)
		A[index(i, t) * n + index(i, t + 1)] += 1;
	  
	      if(i + 1 <= r)
		A[index(i, t) * n + index(i + 1, t - 1)] += 1;

	      if(i + 1 <= r)
		A[index(i, t) * n + index(i + 1, t)] += 1;

	      if(i + 1 <= r)
		A[index(i, t) * n + index(i + 1, t + 1)] += 1;
	    }
	  else
	    {
	      A[index(i, t) * n + index(i - 1, t - 1)] += 1;
	      A[index(i, t) * n + index(i - 1, t)] += 1;

	      if(i <= r)
		A[index(i, t) * n + index(i, t - 1)] += 1;

	      if(i <= r)
		A[index(i, t) * n + index(i, t + 1)] += 1;
	  
	      if(i + 1 <= r)
		A[index(i, t) * n + index(i + 1, t)] += 1;

	      if(i + 1 <= r)
		A[index(i, t) * n + index(i + 1, t + 1)] += 1;
	    }
	  /*else if(t == e - 1 && i % 2 == 0)
	    {
	      A[index(i, t) * n + index(i - 1, t - 1)] += 6;
	      A[index(i, t) * n + index(i - 1, t)] += 6;

	      if(i <= r)
		A[index(i, t) * n + index(i, t - 1)] += 6;

	      if(i <= r)
		A[index(i, t) * n + index(i, t + 1)] += 6;
	  
	      if(i + 1 <= r)
		A[index(i, t) * n + index(i + 1, t)] += 6;

	      if(i + 1 <= r)
		A[index(i, t) * n + index(i + 1, t + 1)] += 6;
	    }
	  else
	    {
	      A[index(i, t) * n + index(i - 1, t - 1)] += 12;
	      A[index(i, t) * n + index(i - 1, t)] += 12;

	      if(i <= r)
		A[index(i, t) * n + index(i, t - 1)] += 12;

	      if(i <= r)
		A[index(i, t) * n + index(i, t + 1)] += 12;
	  
	      if(i + 1 <= r)
		A[index(i, t) * n + index(i + 1, t)] += 12;

	      if(i + 1 <= r)
		A[index(i, t) * n + index(i + 1, t + 1)] += 12;
		}*/
	}
    }

  Workspace workspace;

  for(int i = 0; i < n * n; i++)
    A[i] *= w/* * 0.5 / (3.0 * sqrt(3.0) / 8.0)*/;
  
  for(int i = 0; i < n; i++)
    x0[i] = 0;
  x0[0] = 1;

  workspace.x.insert(std::map<double, double *>::value_type(0.0, x0));
  workspace.F.insert(std::map<double, double>::value_type(0.0, 0.0));
  workspace.A = A;
  workspace.factors = factors;
  workspace.t = t;
  workspace.h = h;
  workspace.params = params;
  workspace.s = s;
  workspace.c = c;
  workspace.e = e;
  workspace.sys = sys;

  workspaces[r] = workspace;
}

void CDF<Triangle>::evaluate(double xF, double xt, int r, Workspace &workspace)
{
  double F = workspace.F.rbegin()->first, t = workspace.F.rbegin()->second;

  int n = elements(r),
    nr = elements(r - 1);

  double *xc = (*workspace.x.rbegin()).second;

  while(!(F > xF || t > xt))
    {
      double *x = (double *)malloc(n * sizeof(double));

      memcpy(x, xc, n * sizeof(double));
	     
      ASSERT(gsl_odeiv2_evolve_apply(workspace.e, workspace.c, workspace.s, &workspace.sys, &workspace.t, xt, &workspace.h, x) == GSL_SUCCESS, "ODE Solver faile!!!\n");

      F = 0.0;
      t = workspace.t;

      for(int i = nr; i < n; i++)
	F += x[i] * workspace.factors[i];

      workspace.x.insert(std::map<double, double *>::value_type(workspace.t, x));
      workspace.F.insert(std::map<double, double>::value_type(F, workspace.t));

      xc = x;
    }
}

CDF<Triangle>::CDF(double w, int RMAX) : w(w), RMAX(RMAX)
{
  workspaces = new Workspace[RMAX + 1];

  for(int i = 0; i <= RMAX; i++)
    {
      initialize(i);
    }
}

CDF<Triangle>::CDF(const CDF<Triangle> &other) : CDF(other.w, other.RMAX)
{
}

CDF<Triangle>::~CDF()
{
  for(int i = 0; i <= RMAX; i++)
    {
      Workspace &workspace = workspaces[i];

      std::map<double, double *> &x = workspace.x;

      for(std::map<double, double *>::iterator it = x.begin(); it != x.end(); it++)
	{
	  free((*it).second);
	}

      free(workspace.A);

      free(workspace.params);
  
      gsl_odeiv2_evolve_free(workspace.e);
      gsl_odeiv2_control_free(workspace.c);
      gsl_odeiv2_step_free(workspace.s);
    }

  delete [] workspaces;
}

double CDF<Triangle>::computeFirstExit(int r, RNG &rng)
{
  return computeFirstExit(r, rng.getDouble());
}

double CDF<Triangle>::computeFirstExit(int r, double rn)
{
  //ASSERT(0, "");

  //int n = elements(r);
  
  Workspace &workspace = workspaces[r];

  //Run until we have information about F
  evaluate(rn, DBL_MAX, r, workspace);

  std::map<double, double>::iterator itr, itl = workspace.F.lower_bound(rn);
  itr = itl--;

  //Done computing temporaries... Find walker's time of exit
  double tl = itl->second,
    Fl = itl->first,
    tr = itr->second,
    Fr = itr->first;

  return tl + (tr - tl) * (rn - Fl) / (Fr - Fl);
}

Index CDF<Triangle>::computeExitPosition(double t, int r, RNG &rng, bool early)
{
  //ASSERT(0, "");

  int n = elements(r),
    nr = elements(r - 1);

  Workspace &workspace = workspaces[r];

  evaluate(DBL_MAX, t, r, workspace);

  double *array = (double *)malloc(n * sizeof(double));

  std::map<double, double *>::iterator itl = workspace.x.lower_bound(t), itr;
  itr = itl--;

  double tl = itl->first,
    tr = itr->first;

  double v = (t - tl) / (tr - tl);

  for(int i = 0; i < n; i++)
    array[i] = workspace.factors[i] * (itl->second[i] + v * (itr->second[i] - itl->second[i]));

  auto index_to_polar = [](int i, int &r, int &t)
    {
      r = 0;
      int e = elements_on_level(r);

      while(i - e >= 0)
	{
	  r++;
	  i -= e;
	  e = elements_on_level(r);
	}

      t = i;
    };

  int radius, theta;
  if(early)
    {
      int i = select(rng.getDouble(), array,  nr);

      index_to_polar(i, radius, theta);
    }
  else
    {
      int i = select(rng.getDouble(), &array[nr], n - nr);

      index_to_polar(i + nr, radius, theta);
    }

  int e = elements_on_level(radius);

  int ee = e * 2;

  if(radius % 2 == 0)
    ee -= 1;

  //Flip on vertical axis if not on edge or center
  if(!(radius % 2 == 0 && theta == e - 1) && theta != 0 && radius != 0)
    {
      if(rng.getDouble() > 0.5)
	{
	  //std::cout << "theta = " << theta << std::endl;
	  //std::cout << "flipping!" << std::endl;
	  theta = ee - 1 - theta;
	}
    }

  Index base[6] = { Index(-radius, 0),
		    Index(-radius, radius),
		    Index(0, radius),
		    Index(radius, 0), 
		    Index(radius, -radius),
		    Index(0, -radius) };

  Index theta_offsets[6] = { Index(0, 1),
			     Index(1, 0),
			     Index(1, -1),
			     Index(0, -1),
			     Index(-1, 0),
			     Index(-1, 1) };

  int rotate = rng.getInt() % 6;

  //std::cout << "radius: " << radius << ", theta : " << theta << ", rotate: " << rotate << std::endl;
  
  Index idx = base[rotate] + theta_offsets[rotate] * theta;
    
  return idx;
}

template class CDF<Triangle>;
