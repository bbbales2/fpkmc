#include <KMC.hpp>

template<>
int KMC<Triangle>::getRadius(int dn, int dm)
{
  return std::max(std::max(std::abs(dn), std::abs(dm)), std::abs(dn + dm));
}

template<>
double KMC<Triangle>::computeNextRxnTime()
{
  double rn = rng.getDouble();

  //std::cout << rn << std::endl
  //<< ratePacks[Type::Ag].FonAg << std::endl;
  return std::max(lastDiffTime, lastRxnTime) - log(rn) / double(maxReactionRate() * double(N * M));
}

template<>
void KMC<Triangle>::selectEvent(Walker &walker, bool resample)
{
  std::function< void () > edgeFunction;

  bool collidedDeposit = false;

  std::map<Type, RatePack>::iterator it = ratePacks.find(walker.type);

  ASSERT(it != ratePacks.end(), "Trying to select a ratePack that does not exist");

  RatePack &ratePack = it->second;

  if(ratePack.Dedge == 0.0)
    {
      walker.edgeR = 0;
    }

  // Tried this as a list and it was slow
  typedef std::multimap<double, Index > EventChoiceMap;

  EventChoiceMap evmap;
	
  if(walker.r == 0)
    {
      // If r has been limited to zero, there are five options
      // 1. We generate a climb event
      // 2. We generate a jump down event
      // 3. We generate a regular propagate event
      // 4. We generate an edge diffusion event
      // 5. We deposit
      
      int depositCount = 0;
      
      std::set<Index> neighbors;
      
      //Assess the deposit situation
      //if(collidedDeposit)
      Index direction[6] = { Index(-1, 0),
			     Index(-1, 1),
			     Index(0, 1),
			     Index(1, 0), 
			     Index(1, -1),
			     Index(0, -1) };
      
      int branches[6][2] =
	{
	  { 5, 1 },
	  { 0, 2 },
	  { 1, 3 },
	  { 2, 4 },
	  { 3, 5 },
	  { 4, 0 }
	};
      
      //Assess the deposit situation
      //if(collidedDeposit)

      std::list<Index> neighborOffsets;

      for(int i = 0; i < 6; i++)
	{
	  if(depositGrid.check(walker.index + direction[i]))
	    {
	      depositCount++;

	      if(collidedDeposit == false)
		{
		  if(DEBUG)
		    std::cout << "selectEvent: Attachment detected @ ";
		  collidedDeposit = true;
		}

	      neighborOffsets.push_back(direction[i]);
	      if(DEBUG) std::cout << walker.index + direction[i] << ", ";
	    }
	}

      if(collidedDeposit == true)
	{
	  walker.attached = true;

	  int i = rng.getInt() % neighborOffsets.size();

	  std::list<Index>::iterator it = neighborOffsets.begin();
	  walker.association = depositGrid.checkAssociation(walker.index + *(std::next(it, i)));

	  if(DEBUG)
	    {
	      std::cout << std::endl;
	    }
	}
      else
	{
	  walker.attached = false;
	}

      auto checkSupport = [](Index index, int _direction, bool _right, Index *direction, DepositGrid<Triangle> &depositGrid)
	{
	  Index position = index + direction[_direction];

	  int support1, support2;

	  if(!_right)
	    {
	      support1 = adjust(_direction - 1, 6);
	      support2 = adjust(_direction - 2, 6);
	    }
	  else
	    {
	      support1 = adjust(_direction + 1, 6);
	      support2 = adjust(_direction + 2, 6);
	    }

	  /*if(DEBUG)
	    {
	      std::cout << "selectEvent: checkSupport: " << ((_right) ? "on right " : "on left ") << position + direction[support1] << " " << depositGrid.check(position + direction[support1]) << " and " << position + direction[support2] << " " << depositGrid.check(position + direction[support2]) << std::endl;
	      }*/
	  
	  return depositGrid.check(position + direction[support1]) && depositGrid.check(position + direction[support2]);
	};

      int leftD = -1, rightD = -1;
      if(collidedDeposit && depositCount >= 1)
	{
	  walker.attached = true;

	  int leftCorner = -1, rightCorner = -1;

	  enum class State { NORMAL, CORNER };
	  
	  State leftState = State::NORMAL, rightState = State::NORMAL;
	  if(depositCount == 2)
	    {
	      std::list<Index>::iterator it2 = neighborOffsets.begin(), it1;
	      it1 = it2++;

	      if((*it1 == direction[4] && *it2 == direction[5]) || (*it2 == direction[4] && *it1 == direction[5]))
		{
		  leftD = 0;
		  rightD = 3;
		}
	      else if((*it1 == direction[1] && *it2 == direction[2]) || (*it2 == direction[1] && *it1 == direction[2]))
		{
		  leftD = 3;
		  rightD = 0;
		}
	      else if((*it1 == direction[0] && *it2 == direction[5]) || (*it2 == direction[0] && *it1 == direction[5]))
		{
		  leftD = 1;
		  rightD = 4;
		}
	      else if((*it1 == direction[2] && *it2 == direction[3]) || (*it2 == direction[2] && *it1 == direction[3]))
		{
		  leftD = 4;
		  rightD = 1;
		}
	      else if((*it1 == direction[0] && *it2 == direction[1]) || (*it2 == direction[0] && *it1 == direction[1]))
		{
		  leftD = 2;
		  rightD = 5;
		}
	      else if((*it1 == direction[3] && *it2 == direction[4]) || (*it2 == direction[3] && *it1 == direction[4]))
		{
		  leftD = 5;
		  rightD = 2;
		}
	      else
		{
		  //brokenConnection = true;
		  walker.edgeR = 0;
		  //Better go KMC on this case...
		}
	    }
	  else if(depositCount == 1)
	    {
	      std::list<Index>::iterator it1 = neighborOffsets.begin();

	      if(DEBUG) std::cout << "selectEvent: Attached at corner" << std::endl;

	      if(*it1 == direction[0])
		{
		  leftD = 1;
		  rightD = 5;
		}
	      else if(*it1 == direction[1])
		{
		  leftD = 2;
		  rightD = 0;
		}
	      else if(*it1 == direction[2])
		{
		  leftD = 3;
		  rightD = 1;
		}
	      else if(*it1 == direction[3])
		{
		  leftD = 4;
		  rightD = 2;
		}
	      else if(*it1 == direction[4])
		{
		  leftD = 5;
		  rightD = 3;
		}
	      else if(*it1 == direction[5])
		{
		  leftD = 0;
		  rightD = 4;
		}
	      else
		{
		  ASSERT(0, "This shouldn't be possible");
		}

	      leftState = State::CORNER;
	      rightState = State::CORNER;
	    }
	  else
	    {
	      //This calls for a deposit!

	      walker.edgeR = 0;
	    }
	  
	  //We're edge propagating baby!
	  if(leftD != -1 && rightD != -1 && walker.edgeR > 0)
	    {
	      int edgeRp1 = 0;
	      std::vector<Index> offsets(walker.edgeR * 2 + 3);

	      Index left = Index(0, 0, 0), right = Index(0, 0, 0);

	      offsets[walker.edgeR + 1] = Index(0, 0, 0);

	      if(DEBUG)
		std::cout << "selectEvent: Checking for edgeR = " << walker.edgeR << std::endl;
	      
	      if(DEBUG)
		std::cout << "selectEvent: rightCorner = " << rightCorner << " and leftCorner = " << leftCorner << std::endl;

	      for(; edgeRp1 <= walker.edgeR; edgeRp1++)
		{
		  bool leftGood = false,
		    rightGood = false;

		  int proposedLeftCorner = leftCorner,
		    proposedRightCorner = rightCorner;

		  State proposedLeftState = leftState,
		    proposedRightState = rightState;

		  int proposedLeftD = leftD,
		    proposedRightD = rightD;

		  if(checkSupport(walker.index + left, leftD, false, direction, depositGrid))
		    {
		      if(depositGrid.isAvailable(walker.index + left + direction[leftD]))
			{
			  leftGood = true;
			}
		      else
			{
			  if(DEBUG) std::cout << "selectEvent: (left) Support there, deposit in the way @ " << walker.index + left + direction[leftD] << std::endl;

			  leftGood = false;
			}
		    }
		  else
		    {
		      if(leftState == State::NORMAL && rightState == State::NORMAL)
			{
			  //If we are currently propagating along our original ednge, check for a corner

			  if(depositGrid.isAvailable(walker.index + left + direction[leftD]))
			    {
			      proposedLeftCorner = edgeRp1;
			      leftGood = true;
			      proposedLeftState = State::CORNER;
			      proposedLeftD = branches[leftD][0];

			      if(DEBUG) std::cout << "selectEvent: proposing switch from leftD = " << leftD << " to " << proposedLeftD << std::endl;
			    }
			  else
			    {
			      //We'd need to go to KMC here
			      // Could be a kink site.
			      // Could also support a surface puncture
			      if(DEBUG) std::cout << "selectEvent: (left) No support there, but no availablity end location either : " << walker.index + left + direction[leftD] << std::endl;

			      //ASSERT(0, "This is the error you were looking for!");

			      leftGood = false;
			    }
			}
		      else
			{
			  //We cannot make two turns

			  if(DEBUG) std::cout << "selectEvent: (left) Trying to make two turns" << std::endl;

			  leftGood = false;
			}
		    }

		  if(checkSupport(walker.index + right, rightD, true, direction, depositGrid))
		    {
		      if(depositGrid.isAvailable(walker.index + right + direction[rightD]))
			{
			  rightGood = true;
			}
		      else
			{
			  if(DEBUG) std::cout << "selectEvent: (right) Support there, deposit in the way @ " << walker.index + right + direction[rightD] << std::endl;

			  rightGood = false;
			}
		    }
		  else
		    {
		      if(rightState == State::NORMAL && leftState == State::NORMAL && proposedLeftState == State::NORMAL)
			{
			  //If we are currently propagating along our original ednge, check for a corner

			  if(depositGrid.isAvailable(walker.index + right + direction[rightD]))
			    {
			      proposedRightCorner = edgeRp1;
			      rightGood = true;
			      proposedRightState = State::CORNER;
			      proposedRightD = branches[rightD][1];

			      if(DEBUG) std::cout << "selectEvent: proposing switch from rightD = " << rightD << " to " << proposedRightD << std::endl;
			    }
			  else
			    {
			      //We'd need to go to KMC here
			      // Could be a kink site.
			      // Could also support a surface puncture
			      if(DEBUG) std::cout << "selectEvent: (right) No support there, but no availablity end location either : " << walker.index + right + direction[rightD] << std::endl;

			      rightGood = false;
			    }
			}
		      else
			{
			  //We cannot make two turns

			  if(DEBUG) std::cout << "selectEvent: (right) Trying to make two turns" << std::endl;

			  rightGood = false;
			}
		    }

		  if(leftGood && rightGood)
		    {
		      left += direction[leftD];
		      right += direction[rightD];

		      int newLeftDepositCount = 0,
			newRightDepositCount = 0;

		      bool slip = false;

		      for(int i = 0; i < 6; i++)
			{
			  if(depositGrid.check(walker.index + left + direction[i]))
			    {
			      newLeftDepositCount++;
			    }

			  if(depositGrid.check(walker.index + right + direction[i]))
			    {
			      newRightDepositCount++;
			    }
			}

		      if(newLeftDepositCount > 2 || newRightDepositCount > 2)
			{
			  if(DEBUG)
			    std::cout << "Trying to slip through an opening" << std::endl;

			  break;
			}
		      else if(((leftState == State::NORMAL && proposedLeftState == State::CORNER) && newLeftDepositCount != 1) || ((rightState == State::NORMAL && proposedRightState == State::CORNER) && newRightDepositCount != 1))
			{
			  if(DEBUG)
			    std::cout << "Now this is a weird one, eh?" << std::endl;

			  break;
			}
		      else if(newLeftDepositCount == 0 || newRightDepositCount == 0)
			{
			  ASSERT(0, "BIG ERROR");
			}
		      else if((newLeftDepositCount == 1 && proposedLeftCorner == -1) || (newRightDepositCount == 1 && proposedRightCorner == -1))
			{
			  ASSERT(0, "DID NOT EXPECT");
			}

		      offsets[walker.edgeR + 1 - (edgeRp1 + 1)] = left; //Note: edgeRp1 isn't incremented yet, so we must add one
		      offsets[walker.edgeR + 1 + (edgeRp1 + 1)] = right;

		      leftCorner = proposedLeftCorner;
		      rightCorner = proposedRightCorner;

		      leftState = proposedLeftState;
		      rightState = proposedRightState;

		      leftD = proposedLeftD;
		      rightD = proposedRightD;

		      if(DEBUG)
			std::cout << "selectEvent: Accepting (left) " << left << ", And (right) " << right << ", leftCorner = " << leftCorner << ", and rightCorner = " << rightCorner <<std::endl;
		      
		      if(slip)
			{
			  edgeRp1++;

			  break;
			}
		    }
		  else
		    {
		      break;
		    }
		}

	      walker.edgeR = std::min(walker.edgeR, std::max(edgeRp1 - 1, 0));

	      int cornerPosition = -1;

	      if(leftState == State::NORMAL && rightState == State::CORNER)
		{
		  cornerPosition = walker.edgeR + 1 + (rightCorner + 1);
		}
	      else if(leftState == State::CORNER && rightState == State::NORMAL)
		{
		  cornerPosition = walker.edgeR + 1 - (leftCorner + 1);
		}
	      else if(leftState == State::CORNER && rightState == State::CORNER)
		{
		  cornerPosition = walker.edgeR + 1;
		}

	      if(DEBUG)
		std::cout << "selectEvent: edgeR = " << walker.edgeR << ", edgeRp1 = " << edgeRp1 << ", cornerPosition = " << cornerPosition << ", leftD = " << leftD << ", rightD = " << rightD << std::endl;

	      if(walker.edgeR > 0)
		{
		  double rn = rng.getDouble();

		  std::map<double, EdgeCDF<Triangle>>::iterator it = edgeCDFs.find(ratePacks[walker.type].Dedge);

		  ASSERT(it != edgeCDFs.end(), "Trying to propagate a walker (edge) which does not have an EdgeCDF");
		  
		  EdgeCDF<Triangle> &edgeCDF = it->second;

		  double dtEdge = edgeCDF.computeFirstExit(walker.edgeR, cornerPosition, rn);
		  double dtBreak = -log(rng.getDouble()) / (2 * ratePack.Dbreak);
		  double dtUp = -log(rng.getDouble()) / (ratePack.Dup);

		  EdgeEventType edgeEventType;

		  ASSERT(cornerPosition < walker.edgeR * 2 + 3, "Corner index out of range");

		  for(int i = 0; i < walker.edgeR * 2 + 3; i++)
		    {
		      offsets[i] = offsets[offsets.size() / 2 - (walker.edgeR + 1) + i];
		    }

		  offsets.resize(walker.edgeR * 2 + 3);

		  if(DEBUG)
		    std::cout << "selectEvent: Computing dt with rn = " << rn << std::endl;

		  if(dtBreak < dtEdge || dtUp < dtEdge)
		    {
		      if(dtBreak < dtUp)
			{
			  if(DEBUG)
			    std::cout << "selectEvent: Down event selected dt = " << dtBreak << std::endl;
			  walker.exitTime = walker.t + dtBreak;
		      
			  edgeEventType = EdgeEventType::EDGE_BREAK;
			  edgeFunction = std::function<void ()>(std::bind(&KMC::edgePropagate, std::ref(*this), std::ref(walker), edgeEventType, offsets, cornerPosition));
			}
		      else if(dtUp <= dtBreak)
			{
			  if(DEBUG)
			    std::cout << "selectEvent: Down event selected dt = " << dtBreak << std::endl;
			  walker.exitTime = walker.t + dtUp;
		      
			  edgeEventType = EdgeEventType::EDGE_UP;
			  edgeFunction = std::function<void ()>(std::bind(&KMC::edgePropagate, std::ref(*this), std::ref(walker), edgeEventType, offsets, cornerPosition));
			}
		    }
		  else
		    {
		      if(DEBUG)
			std::cout << "selectEvent: Diffuse selected dt = " << dtEdge << std::endl;
		      walker.exitTime = walker.t + dtEdge;
		      
		      edgeEventType = EdgeEventType::EDGE_DIFFUSE;
		      edgeFunction = std::function<void ()>(std::bind(&KMC::edgePropagate, std::ref(*this), std::ref(walker), edgeEventType, offsets, cornerPosition));
		    }
		}
	    }
	}
      else
	{
	  walker.edgeR = 0;
	}

      if(walker.edgeR == 0)
	{
	  const std::set<Index> &stencil = walkerGrid.getStencil(walker.index);

	  for(std::set<Index>::const_iterator it = stencil.begin(); it != stencil.end(); it++)
	    {
	      Index offset = *it;
	      Index q = walker.index + offset;
	  
	      if(depositGrid.isAvailable(q))
		{
		  std::pair<Walker &, bool> probe = walkerGrid.get(q);

		  if(probe.second == false)
		    {
		      double Dtot;
		      if(depositGrid.checkFloorType(walker.index) == Type::Si)
			{
			  Dtot = ratePack.DonSi;
			}
		      else
			{
			  Dtot = ratePack.DonM;
			}

		      if(collidedDeposit)
			{
			  if(q.getl() > walker.index.getl())
			    {
			      Dtot = ratePack.Dup;
			      if(DEBUG) std::cout << "selectEvent: Found edge up event" << std::endl;
			    }
			  else if(q.getl() < walker.index.getl())
			    {
			      Dtot = ratePack.Ddown * ratePack.Dbreak;
			      if(DEBUG) std::cout << "selectEvent: Found edge down event (break)" << std::endl;
			    }
			  else
			    {
			      int newDepositCount = 0;

			      for(int i = 0; i < 6; i++)
				{
				  if(depositGrid.check(q + direction[i]))
				    {
				      newDepositCount++;
				    }
				}

			      if(newDepositCount == 0)
				{
				  Dtot = ratePack.Dbreak;
				  if(DEBUG) std::cout << "selectEvent: Found break event to " << offset << " (horizontal diffusion) with Dtot = " << Dtot << std::endl;
				}			  
			      else if(newDepositCount == 1 || depositCount == 1)
				{
				  Dtot = ratePack.Dcorner;
				  if(DEBUG) std::cout << "selectEvent: Found corner event to " << offset << " with Dtot = " << Dtot << std::endl;
				}
			      else if(newDepositCount >= 2)
				{
				  Dtot = ratePack.Dedge;
				  if(DEBUG) std::cout << "selectEvent: Found edge event to " << offset << " with Dtot = " << Dtot << std::endl;
				}
			    }
			}
		      else
			{
			  if(q.getl() > walker.index.getl())
			    {
			      Dtot = ratePack.Dup;
			      if(DEBUG) std::cout << "selectEvent: Found edge up event" << std::endl;
			    }
			  else if(q.getl() < walker.index.getl())
			    {
			      Dtot = ratePack.Ddown;
			      if(DEBUG) std::cout << "selectEvent: Found edge down event" << std::endl;
			    }
			  else
			    {
			      if(DEBUG) std::cout << "selectEvent: Found diffusion event" << std::endl;
			    }
			}

		      evmap.insert(EventChoiceMap::value_type(Dtot, offset));

		      if(DEBUG) std::cout << std::endl;

		      ASSERT(q.getl() <= walker.index.getl() || (q.getl() > walker.index.getl() && collidedDeposit), "This shouldn't be possible\n");
		    }
		  else
		    {
		      if(resample)
			{
			  if(probe.first.event != events.end())
			    events.erase(probe.first.event);
				  
			  findR(probe.first, false);
			}
		    }
		}
	    }
	}

      // We deposited
      if(depositCount > 2/* || (depositCount == 1 && Dedge == 0.0)*/)
	{
	  if(DEBUG) std::cout << "findR: Deposit detected for Walker " << walker << "\n";

	  walker.r = 0;
	  walker.edgeR = 0;

	  deposit(walker);
	  if(walker.event != events.end())
	    events.erase(walker.event);

	  walker.event = events.insert(events.begin(), std::pair<double, std::function<void ()> >(0.0, std::function<void ()>(std::bind(&KMC::eraseWalker, std::ref(*this), std::ref(walker)))));

	  walker.deposited = true;
	}
      else if(walker.edgeR > 0)
	{
	  walker.r = 0;
      
	  walker.event = events.insert(Event(walker.exitTime, edgeFunction));

	  if(DEBUG)
	    std::cout << "edgePropagate selected" << std::endl;
	}
      // It is time to select an event from the generated list
      else
	{
	  walker.r = 0;
	  walker.edgeR = 0;

	  if(walker.event != events.end())
	    events.erase(walker.event);

	  double sum = 0.0;
	  for(EventChoiceMap::iterator it = evmap.begin(); it != evmap.end(); it++)
	    {
	      sum += (*it).first;
	    }

	  double rn = rng.getDouble();
	  double dt = -log(rn) / sum;

	  if(sum == 0.0)
	    {
	      walker.event = events.end();
	      if(DEBUG)
		std::cout << "findR: Creating null-event walker " << walker << std::endl;
	    }
	  else
	    {
	      walker.exitTime = std::max(lastDiffTime, lastRxnTime) + dt;

	      EventChoiceMap::iterator it = select(rng.getDouble(), evmap);
	  
	      walker.event = events.insert(Event(walker.exitTime, std::function<void ()>(std::bind(&KMC::hop, std::ref(*this), std::ref(walker), it->second))));

	      if(DEBUG)
		std::cout << "findR: Selected event with exitTime = " << std::max(lastDiffTime, lastRxnTime) + dt << std::endl;
	    }
	}
    }
  //If we are not touching another walker or deposit, and we successfully chose an r >= 1, we must be free to propagate
  else
    {
      walker.edgeR = 0;

      double D;

      if(depositGrid.checkFloorType(walker.index) == Type::Si)
	{
	  D = ratePack.DonSi;
	}
      else
	{
	  D = ratePack.DonM;
	}

      std::map< double, CDF<Triangle> >::iterator it = cdfs.find(D);

      ASSERT(it != cdfs.end(), "Trying to propagate a walker which does not have a CDF");

      CDF<Triangle> &cdf = it->second;

      double r = rng.getDouble();
      double dt = cdf.computeFirstExit(walker.r, r);

      ASSERT(walker.r != 0, "walker.r should not be zero");
	    
      if(DEBUG) std::cout << "findFPTs: Walker " << walker;
      
      walker.exitTime = walker.t + dt;

      //std::max(lastDiffTime, lastRxnTime)
	    
      if(walker.event != events.end())
	events.erase(walker.event);       
	    
      walker.event = events.insert(Event(walker.exitTime, std::function<void ()>(std::bind(&KMC::propagate, std::ref(*this), std::ref(walker)))));
    }
}
