#include <KMC.hpp>

template<>
int KMC<Square>::getRadius(int dn, int dm)
{
  
  return std::max(std::abs(dn), std::abs(dm));
}

template<>
double KMC<Square>::computeNextRxnTime()
{
  double rn = rng.getDouble();
  return std::max(lastDiffTime, lastRxnTime) - log(rn) / double(ratePacks[Type::A].F * double(N * M));
}

template<>
void KMC<Square>::selectEvent(Walker &walker, bool resample)
{
  //These directions are used for edgeDiffuse 'MM' -> minus 1 in M direction, 'MP' -> plus 1 etc.
  enum Direction
    {
      MM,
      MP,
      NM,
      NP
    };

  std::map<Type, RatePack>::iterator it = ratePacks.find(walker.type);

  ASSERT(it != ratePacks.end(), "Trying to select a ratePack that does not exist");

  RatePack &ratePack = it->second;

  if(ratePack.Dedge == 0.0)
    {
      walker.edgeR = 0;
    }

  bool collidedDeposit = false;
  bool edgePropagate = false;

  int cornerPosition = 0;

  // Tried this as a list and it was slow
  typedef std::multimap<double, Index > EventChoiceMap;
  std::function<void () > edgeFunction;

  EventChoiceMap evmap;
	
  if(walker.r == 0)
    {
      // If r has been limited to zero, there are five options
      // 1. We generate a climb event
      // 2. We generate a jump down event
      // 3. We generate a regular propagate event
      // 4. We generate an edge diffusion event
      // 5. We deposit
      
      int depositCount = 0;

      //Assess the deposit situation
      Direction direction;
	  
      if(depositGrid.check(walker.index + Index(0, -1, 0)))
	{
	  depositCount++;
	  direction = MM;
	}
	  
      if(depositGrid.check(walker.index + Index(0, 1, 0)))
	{
	  depositCount++;
	  direction = MP;
	}
	    
      if(depositGrid.check(walker.index + Index(-1, 0, 0)))
	{
	  depositCount++;
	  direction = NM;
	}
	      
      if(depositGrid.check(walker.index + Index(1, 0, 0)))
	{
	  depositCount++;
	  direction = NP;
	}

      if(depositCount >= 1)
	{
	  collidedDeposit = true;
	  walker.attached = true;
	}
      else
	{
	  walker.attached = false;
	}

      if(depositCount >= 2)
	{
	  walker.deposited = true;
	}

      if(collidedDeposit)
	{
	  //If we are only touching a deposit on one side, edge diffusion, climbing, down jumping, and regular propagating are all possible
	  if(depositCount == 1)
	    {
	      if(DEBUG) std::cout << "findR: Attachment detected for Walker " << walker << std::endl;
	      walker.attached = true;

	      //Can try to do an edgePropagate
	      if(walker.edgeR > 0)
		{
		  int leftR = 0, rightR = 0, leftCornerR = RMAX, rightCornerR = RMAX;
		  std::vector<Index> offsets;

		  if(direction == MM || direction == MP)
		    {
		      if(DEBUG) std::cout << "Victory!" << std::endl;
		      int dm;

		      if(direction == MM)
			dm = -1;
		      else
			dm = 1;

		      int dn = -1;

		      if(DEBUG) std::cout << "Checking : " << Index(dn, 0) << leftR << std::endl;
		      while(depositGrid.check(walker.index + Index(dn, dm)) &&
			    depositGrid.isAvailable(walker.index + Index(dn, 0)))
			{
			  if(DEBUG) std::cout << "Accepting : " << Index(dn, 0) << leftR << std::endl;

			  if(leftR >= walker.edgeR)
			    {
			      break;
			    }

			  leftR++;
			  dn--;
			  
			  if(DEBUG) std::cout << "Checking : " << Index(dn, 0) << leftR << std::endl;
			}

		      if(leftR < walker.edgeR)
			{
			  leftCornerR = leftR;

			  int ddm;

			  if(direction == MM)
			    {
			      ddm = -1;
			    }
			  else
			    {
			      ddm = 1;
			    }

			  if(DEBUG) std::cout << "Checking : " << Index(dn, dm) << leftR << std::endl;
			  while(depositGrid.check(walker.index + Index(dn + 1, dm)) &&
				depositGrid.isAvailable(walker.index + Index(dn, dm)))
			    {
			      if(DEBUG) std::cout << "Accepting : " << Index(dn, dm) << leftR << std::endl;

			      if(leftR >= walker.edgeR)
				break;

			      leftR++;
			      dm += ddm;

			      if(DEBUG) std::cout << "Checking : " << Index(dn, dm) << leftR << std::endl;
			    }
			}

		      dn = 1;

		      if(direction == MM)
			dm = -1;
		      else
			dm = 1;

		      if(DEBUG) std::cout << "Checking : " << Index(dn, 0) << rightR << std::endl;
		      while(depositGrid.check(walker.index + Index(dn, dm)) &&
			    depositGrid.isAvailable(walker.index + Index(dn, 0)))
			{
			  if(DEBUG) std::cout << "Accepting : " << Index(dn, 0) << rightR << std::endl;

			  if(rightR >= walker.edgeR)
			    {
			      break;
			    }

			  if(rightR >= leftR)
			    {
			      break;
			    }
			  
			  rightR++;
			  dn++;

			  if(DEBUG) std::cout << "Checking : " << Index(dn, 0) << rightR << std::endl;
			}

		      if(rightR < walker.edgeR && rightR < leftR)
			{
			  rightCornerR = rightR;
			  
			  int ddm;
			  
			  if(direction == MM)
			    {
			      ddm = -1;
			    }
			  else
			    {
			      ddm = 1;
			    }

			  if(DEBUG) std::cout << "Checking : " << Index(dn, dm) << rightR << std::endl;
			  while(depositGrid.check(walker.index + Index(dn - 1, dm)) &&
				depositGrid.isAvailable(walker.index + Index(dn, dm)))
			    {
			      if(DEBUG) std::cout << "Accepting : " << Index(dn, dm) << rightR << std::endl;

			      if(rightR >= walker.edgeR && DEBUG)
				{
				  if(direction == MM)
				    {
				      std::cout << "Broke MM @ edge" << std::endl;
				    }
				  break;
				}
			      
			      if(rightR >= leftR)
				{
				  if(direction == MM && DEBUG)
				    {
				      std::cout << "Broke MM @ left" << std::endl;
				    }
				  break;
				}
			      
			      rightR++;
			      dm += ddm;

			      if(DEBUG) std::cout << "Checking : " << Index(dn, dm) << rightR << std::endl;
			    }
			}

		      walker.edgeR = std::max(std::min(walker.edgeR, std::min(leftR - 1, rightR - 1)), 0);

		      walker.edgeR = std::max(std::min(walker.edgeR, std::max(leftCornerR, rightCornerR) - 1), 0);

		      if(walker.edgeR >= leftCornerR)
			{
			  cornerPosition = walker.edgeR - leftCornerR;
			}
		      else if(walker.edgeR >= rightCornerR)
			{
			  cornerPosition = walker.edgeR + 1 + rightCornerR;
			}
		      else
			{
			  cornerPosition = 2 * walker.edgeR + 2; // This tells the CDF to take no corner at all
			}

		      ASSERT(cornerPosition < walker.edgeR * 2 + 3, "Corner index out of range");

		      if(DEBUG) std::cout << "walker.edgeR: " << walker.edgeR << "; leftR: " << leftR << "; rightR: " << rightR << "; leftCornerR: " << leftCornerR << "; rightCornerR: " << rightCornerR << "; cornerPosition: " << cornerPosition << std::endl;

		      if(leftCornerR == rightCornerR)
			{
			  ASSERT(walker.edgeR < leftCornerR || walker.edgeR == 0, "Cannot happen");
			  //walker.edgeR = std::max(std::min(walker.edgeR, leftCornerR - 1), 0);
			}

		      offsets.resize(walker.edgeR * 2 + 3);

		      int i1 = 0, i2 = 0, di2;

		      if(direction == MM)
			{
			  di2 = -1;
			}
		      else
			{
			  di2 = 1;
			}

		      offsets[walker.edgeR + 1] = Index(0, 0, 0);

		      if(DEBUG) std::cout << "Offset built: " << offsets[walker.edgeR + 1] << std::endl;

		      bool turn = false;

		      if(walker.edgeR > 0)
			{
			  for(int i = 0; i >= -walker.edgeR; i--)
			    {
			      if(std::abs(i) < leftCornerR)
				{
				  i1--;
				}
			      else if(std::abs(i) == leftCornerR)
				{
				  turn = true;

				  i1--;

				  if(direction == MM)
				    {
				      i2 = -1;
				    }
				  else
				    {
				      i2 = 1;
				    }
				}
			      else
				{
				  i2 += di2;
				}

			      offsets[i + walker.edgeR + 1 - 1] = Index(i1, i2, 0);

			      if(DEBUG) std::cout << "Offset built: " << offsets[i + walker.edgeR + 1 - 1] << std::endl;
			    }

			  i1 = 0;
			  i2 = 0;

			  for(int i = 0; i <= walker.edgeR; i++)
			    {
			      if(i < rightCornerR)
				{
				  i1++;
				}
			      else if(i == rightCornerR)
				{
				  ASSERT(turn == false, "Noooo!");

				  i1++;

				  if(direction == MM)
				    {
				      i2 = -1;
				    }
				  else
				    {
				      i2 = 1;
				    }
				}
			      else
				{
				  i2 += di2;
				}

			      offsets[i + walker.edgeR + 2] = Index(i1, i2, 0);

			      if(DEBUG) std::cout << "Offset built: " << offsets[i + walker.edgeR + 2] << std::endl;
			    }
			}
		    }

		  if(direction == NM || direction == NP)
		    {
		      int dn;

		      if(direction == NM)
			dn = -1;
		      else
			dn = 1;

		      int dm = -1;

		      if(DEBUG) std::cout << "Checking : " << Index(0, dm) << leftR << std::endl;
		      while(depositGrid.check(walker.index + Index(dn, dm)) &&
			    depositGrid.isAvailable(walker.index + Index(0, dm)))
			{
			  if(DEBUG) std::cout << "Accepting : " << Index(0, dm) << leftR << std::endl;

			  if(leftR >= walker.edgeR)
			    break;
			  
			  leftR++;
			  dm--;

			  if(DEBUG) std::cout << "Checking : " << Index(0, dm) << leftR << std::endl;
			}

		      if(leftR < walker.edgeR)
			{
			  leftCornerR = leftR;

			  int ddn;

			  if(direction == NM)
			    {
			      ddn = -1;
			    }
			  else
			    {
			      ddn = 1;
			    }

			  if(DEBUG) std::cout << "Checking : " << Index(dn, dm) << leftR << std::endl;
			  while(depositGrid.check(walker.index + Index(dn, dm + 1)) &&
				depositGrid.isAvailable(walker.index + Index(dn, dm)))
			    {
			      if(DEBUG) std::cout << "Accepting : " << Index(dn, dm) << leftR << std::endl;

			      if(leftR >= walker.edgeR)
				break;

			      leftR++;
			      dn += ddn;

			      if(DEBUG) std::cout << "Checking : " << Index(dn, dm) << leftR << std::endl;
			    }
			}

		      if(direction == NM)
			dn = -1;
		      else
			dn = 1;

		      dm = 1;

		      if(DEBUG) std::cout << "Checking : " << Index(0, dm) << rightR << std::endl;
		      while(depositGrid.check(walker.index + Index(dn, dm)) &&
			    depositGrid.isAvailable(walker.index + Index(0, dm)))
			{
			  if(DEBUG) std::cout << "Accepting : " << Index(0, dm) << rightR << std::endl;

			  if(rightR >= walker.edgeR)
			    break;

			  if(rightR >= leftR)
			    break;

			  rightR++;
			  dm++;

			  if(DEBUG) std::cout << "Checking : " << Index(0, dm) << rightR << std::endl;
			}

		      if(rightR < walker.edgeR && rightR < leftR)
			{
			  rightCornerR = rightR;
			  
			  int ddn;
			  
			  if(direction == NM)
			    {
			      ddn = -1;
			    }
			  else
			    {
			      ddn = 1;
			    }

			  if(DEBUG) std::cout << "Checking : " << Index(dn, dm) << rightR << std::endl;
			  while(depositGrid.check(walker.index + Index(dn, dm - 1)) &&
				depositGrid.isAvailable(walker.index + Index(dn, dm)))
			    {
			      if(DEBUG) std::cout << "Accepting : " << Index(dn, dm) << rightR << std::endl;

			      if(rightR >= walker.edgeR)
				break;

			      if(rightR >= leftR)
				break;
			      
			      rightR++;
			      dn += ddn;

			      if(DEBUG) std::cout << "Checking : " << Index(dn, dm) << rightR << std::endl;
			    }
			}

		      walker.edgeR = std::max(std::min(walker.edgeR, std::min(leftR - 1, rightR - 1)), 0);

		      walker.edgeR = std::max(std::min(walker.edgeR, std::max(leftCornerR, rightCornerR) - 1), 0);
		      if(walker.edgeR >= leftCornerR)
			{
			  cornerPosition = walker.edgeR - leftCornerR;
			}
		      else if(walker.edgeR >= rightCornerR)
			{
			  cornerPosition = walker.edgeR + 1 + rightCornerR;
			}
		      else
			{
			  cornerPosition = 2 * walker.edgeR + 2; // This tells the CDF to take no corner at all
			}

		      ASSERT(cornerPosition < walker.edgeR * 2 + 3, "Corner index out of range");
			
		      if(DEBUG) std::cout << "walker.edgeR: " << walker.edgeR << "; leftR: " << leftR << "; rightR: " << rightR << "; leftCornerR: " << leftCornerR << "; rightCornerR: " << rightCornerR << "; cornerPosition: " << cornerPosition << std::endl;

		      if(leftCornerR == rightCornerR)
			{
			  ASSERT(walker.edgeR < leftCornerR || walker.edgeR == 0, "Cannot happen");
			  //walker.edgeR = std::max(std::min(walker.edgeR, leftCornerR - 1), 0);
			}


		      offsets.resize(walker.edgeR * 2 + 3);

		      int i1 = 0, i2 = 0, di1;

		      if(direction == NM)
			{
			  di1 = -1;
			}
		      else
			{
			  di1 = 1;
			}

		      offsets[walker.edgeR + 1] = Index(0, 0, 0);

		      if(DEBUG) std::cout << "Offset built: " << offsets[walker.edgeR + 1] << std::endl;

		      bool turn = false;

		      if(walker.edgeR > 0)
			{
			  for(int i = 0; i >= -walker.edgeR; i--)
			    {
			      if(std::abs(i) < leftCornerR)
				{
				  i2--;
				}
			      else if(std::abs(i) == leftCornerR)
				{
				  turn = true;

				  i2--;

				  if(direction == NM)
				    {
				      i1 = -1;
				    }
				  else
				    {
				      i1 = 1;
				    }
				}
			      else
				{
				  i1 += di1;
				}

			      offsets[i + walker.edgeR + 1 - 1] = Index(i1, i2, 0);

			      if(DEBUG) std::cout << "Offset built: " << offsets[i + walker.edgeR + 1 - 1] << std::endl;
			    }
		      
			  i1 = 0;
			  i2 = 0;

			  for(int i = 0; i <= walker.edgeR; i++)
			    {
			      if(i < rightCornerR)
				{
				  i2++;
				}
			      else if(i == rightCornerR)
				{
				  ASSERT(turn == false, "Noooo!");
			      
				  i2++;

				  if(direction == NM)
				    {
				      i1 = -1;
				    }
				  else
				    {
				      i1 = 1;
				    }
				}
			      else
				{
				  i1 += di1;
				}

			      offsets[i + walker.edgeR + 2] = Index(i1, i2, 0);
			      if(DEBUG) std::cout << "Offset built: " << offsets[i + walker.edgeR + 2] << std::endl;
			    }
			}
		    }
		  
		  if(walker.edgeR > 0)
		    {
		      edgePropagate = true;

		      if(DEBUG)
			std::cout << "selectEvent: Found edge propagate with walker.edgeR = " << walker.edgeR << std::endl;
		    }

		  if(edgePropagate)
		    {
		      double rn = rng.getDouble();

		      std::map<Type, EdgeCDF<Square>>::iterator it = edgeCDFs.find(walker.type);

		      ASSERT(it != edgeCDFs.end(), "Trying to propagate a walker (edge) which does not have an EdgeCDF");

		      EdgeCDF<Square> &edgeCDF = it->second;

		      double dtEdge = edgeCDF.computeFirstExit(walker.edgeR, cornerPosition, rn);
		      double dtBreak = -log(rng.getDouble()) / ratePack.Dbreak;
		      double dtUp = -log(rng.getDouble()) / (2 * ratePack.Dup);

		      EdgeEventType edgeEventType;

		      ASSERT(cornerPosition < walker.edgeR * 2 + 3, "Corner index out of range");

		      if(DEBUG)
			std::cout << "selectEvent: Computing dt with rn = " << rn << std::endl;

		      if(dtBreak < dtEdge || dtUp < dtEdge)
			{
			  if(dtBreak < dtUp)
			    {
			      if(DEBUG)
				std::cout << "selectEvent: Break event selected dt = " << dtBreak << std::endl;

			      walker.exitTime = walker.t + dtBreak;
			      
			      edgeEventType = EdgeEventType::EDGE_BREAK;
			      edgeFunction = std::function<void ()>(std::bind(&KMC::edgePropagate, std::ref(*this), std::ref(walker), edgeEventType, offsets, cornerPosition));
			    }
			  else
			    {
			      if(DEBUG)
				std::cout << "selectEvent: Break event selected dt = " << dtBreak << std::endl;

			      walker.exitTime = walker.t + dtUp;
			      
			      edgeEventType = EdgeEventType::EDGE_UP;
			      edgeFunction = std::function<void ()>(std::bind(&KMC::edgePropagate, std::ref(*this), std::ref(walker), edgeEventType, offsets, cornerPosition));
			    }
			}
		      else
			{
			  if(DEBUG)
			    std::cout << "selectEvent: Diffuse selected dt = " << dtEdge << std::endl;

			  walker.exitTime = walker.t + dtEdge;

			  edgeEventType = EdgeEventType::EDGE_DIFFUSE;
			  edgeFunction = std::function<void ()>(std::bind(&KMC::edgePropagate, std::ref(*this), std::ref(walker), edgeEventType, offsets, cornerPosition));
			}
		    }
		}
	      
	      //If either edgeR == 0 or there is no propagate possible, we must do KMC
	      if(edgePropagate == false)
		{
		  //Check which edge diffusion events are possible
		  int dn = 0, dm = 0, dl = 0;
		  if(direction == MM || direction == MP)
		    {
		      if(direction == MM)
			dm = -1;
		      else
			dm = 1;

		      for(int dn = -1; dn <= 1; dn += 2)
			{
			  Index q = walker.index + Index(dn, dm, dl);

			  bool corner = false;

			  //Check to see if the walker can propagate around a corner
			  if(depositGrid.isAvailable(q))
			    {
			      std::pair<Walker &, bool> probe = walkerGrid.get(q);

			      if(probe.second == false)
				{
				  if(DEBUG) std::cout << "findR: MM/MP Scheduling: Considering corner-edge diffusion for walker " << walker << " to " << q << std::endl;

				  evmap.insert(EventChoiceMap::value_type(ratePack.Dedge, Index(dn, dm, dl)));

				  corner = true;
				}
			      else
				{
				  if(resample)
				    {
				      if(probe.first.event != events.end())
					events.erase(probe.first.event);
				  
				      findR(probe.first, false);
				    }
				}
			    }

			  //Check to see if the walker can propagate along an edge.
			  // Note, if a corner is possible, then there is no edge to propagate along -- propagating in this direction would represent a break from the edge, so reduce the diffusion rate in this direction appropriately!
			  // Unimplemented optimization: There is no need to check for deposits and walkers here -- findR would have taken care of that
			  q = walker.index + Index(dn, 0, dl);
			  if(depositGrid.isAvailable(q))
			    {
			      std::pair<Walker &, bool> probe = walkerGrid.get(q);

			      if(probe.second == false)
				{
				  double Dtot = ratePack.Dedge;
				  if(corner)
				    Dtot = ratePack.Dbreak;

				  if(DEBUG) std::cout << "findR: MM/MP Scheduling: Considering side-edge diffusion for walker " << walker << " to " << q << std::endl;

				  evmap.insert(EventChoiceMap::value_type(Dtot, Index(dn, 0, dl)));
				}
			      else
				{
				  if(resample)
				    {
				      if(probe.first.event != events.end())
					events.erase(probe.first.event);
				  
				      findR(probe.first, false);
				    }
				}
			    }
			}
		    }
		  else if(direction == NM || direction == NP)
		    {
		      if(direction == NM)
			dn = -1;
		      else
			dn = 1;

		      for(int dm = -1; dm <= 1; dm += 2)
			{
			  Index q = walker.index + Index(dn, dm, dl);

			  bool corner = false;
			    
			  //Check to see if the walker can propagate around a corner
			  if(depositGrid.isAvailable(q))
			    {
			      std::pair<Walker &, bool> probe = walkerGrid.get(q);

			      if(probe.second == false)
				{
				  if(DEBUG) std::cout << "findR: Scheduling: Considering corner-edge diffusion for walker " << walker << " to " << q << std::endl;

				  evmap.insert(EventChoiceMap::value_type(ratePack.Dedge, Index(dn, dm, dl)));

				  corner = true;
				}
			      else
				{
				  if(resample)
				    {
				      if(probe.first.event != events.end())
					events.erase(probe.first.event);
				  
				      findR(probe.first, false);
				    }
				}
			    }

			  //Check to see if the walker can propagate along an edge.
			  q = walker.index + Index(0, dm, dl);
			  if(depositGrid.isAvailable(q))
			    {
			      std::pair<Walker &, bool> probe = walkerGrid.get(q);

			      if(probe.second == false)
				{
				  double Dtot = ratePack.Dedge;
				  if(corner)
				    Dtot = ratePack.Dbreak;

				  if(DEBUG) std::cout << "findR: Scheduling: Considering side-edge diffusion for walker " << walker << " to " << q << std::endl;

				  evmap.insert(EventChoiceMap::value_type(Dtot, Index(0, dm, dl)));
				}
			      else
				{
				  if(resample)
				    {
				      if(probe.first.event != events.end())
					events.erase(probe.first.event);
				  
				      findR(probe.first, false);
				    }
				}
			    }
			}
		    }
		

		  //Now that the edge diffusion is taken care of, it is time to handle propagation up and away from the edge
		  { //Up the edge -- there are two directions up
		    int dn = 0, dm = 0;

		    //Directions for first possible hop
		    if(direction == MM)
		      {
			dn = 0;
			dm = -1;
		      }
		    else if(direction == MP)
		      {
			dn = 0;
			dm = 1;
		      }
		    else if(direction == NM)
		      {
			dn = -1;
			dm = 0;
		      }
		    else if(direction == NP)
		      {
			dn = 1;
			dm = 0;
		      }

		    if(walker.index.getl() % 2 == 1)
		      {
			dn--;
			dm--;
		      }

		    int tdn,
		      tdm;

		    if(dn == 1)
		      tdn = 2;
		    else if(dn == -1)
		      tdn = -1;
		    else
		      tdn = 0;

		    if(dm == 1)
		      tdm = 2;
		    else if(dm == -1)
		      tdm = -1;
		    else
		      tdm = 0;

		    double Dtot = 1.0;
		  
		    Dtot = ratePack.Dup;

		    //Double check there is nothing where we're trying to hop to
		    Index q = walker.index + Index(tdn, tdm, 1);
		    if(depositGrid.isAvailable(q))
		      {
			std::pair<Walker &, bool> probe = walkerGrid.get(q);

			if(probe.second == false)
			  {
			    evmap.insert(EventChoiceMap::value_type(Dtot, Index(tdn, tdm, 1)));
			  }
			else
			  {
			    if(resample)
			      {
				if(probe.first.event != events.end())
				  events.erase(probe.first.event);
				  
				findR(probe.first, false);
			      }
			  }
		      }

		    if(dn == 1 || dn == -1)
		      tdm++;
		    
		    if(dm == 1 || dm == -1)
		      tdn++;
		  
		    //Directions for second possible hop
		    /*if(direction == MM)
		      {
		      dn = 1;
		      dm = 1;
		      }
		      else if(direction == MP)
		      {
		      dn = 1;
		      dm = -1;
		      }
		      else if(direction == NM)
		      {
		      dn = 1;
		      dm = 1;
		      }
		      else if(direction == NP)
		      {
		      dn = -1;
		      dm = 1;
		      }

		      if(walker.index.getl() % 2 == 1)
		      {
		      dn--;
		      dm--;
		      }*/

		    q = walker.index + Index(tdn, tdm, 1);
		    if(depositGrid.isAvailable(q))
		      {
			std::pair<Walker &, bool> probe = walkerGrid.get(q);

			if(probe.second == false)
			  {
			    evmap.insert(EventChoiceMap::value_type(Dtot, Index(tdn, tdm, 1)));
			  }
			else
			  {
			    if(resample)
			      {
				if(probe.first.event != events.end())
				  events.erase(probe.first.event);
				  
				findR(probe.first, false);
			      }
			  }
		      }
		  }

		  { //Away from the edge
		    int dn = 0, dm = 0;

		    if(direction == MM)
		      {
			dn = 0;
			dm = 1;
		      }
		    else if(direction == MP)
		      {
			dn = 0;
			dm = -1;
		      }
		    else if(direction == NM)
		      {
			dn = 1;
			dm = 0;
		      }
		    else if(direction == NP)
		      {
			dn = -1;
			dm = 0;
		      }

		    double Dtot = ratePack.Dbreak;
		  
		    //Check to see if there is open space to propagate out into horizontally
		    // Optimization: If there was a second deposit, we would have deposited. If there was a walker, we would have deposited, so the only thing we need to check is that there is a deposit or walker to walk on
		    Index q = walker.index + Index(dn, dm, 0);
		    if(depositGrid.isAvailable(q))
		      {
			std::pair<Walker &, bool> probe = walkerGrid.get(q);

			if(probe.second == false)
			  {
			    Dtot *= ratePack.D;

			    evmap.insert(EventChoiceMap::value_type(Dtot, Index(dn, dm, 0)));
			  }
			else
			  {
			    if(resample)
			      {
				if(probe.first.event != events.end())
				  events.erase(probe.first.event);
				  
				findR(probe.first, false);
			      }
			  }
		      }
		    else
		      {
			int tdn,
			  tdm;

			if(dn == 1)
			  tdn = 2;
			else if(dn == -1)
			  tdn = -1;
			else
			  tdn = 0;

			if(dm == 1)
			  tdm = 2;
			else if(dm == -1)
			  tdm = -1;
			else
			  tdm = 0;

			if(walker.index.getl() % 2 == 1)
			  {
			    tdn--;
			    tdm--;
			  }

			dl = -1;
			//Check to see if there is a hole to hop down into
			q = walker.index + Index(tdn, tdm, -1);
			if(depositGrid.isAvailable(q))
			  {
			    std::pair<Walker &, bool> probe = walkerGrid.get(q);

			    if(probe.second == false)
			      {
				Dtot *= ratePack.Ddown;
		      
				evmap.insert(EventChoiceMap::value_type(Dtot, Index(tdn, tdm, -1)));
			      }
			    else
			      {
				if(resample)
				  {
				    if(probe.first.event != events.end())
				      events.erase(probe.first.event);
				  
				    findR(probe.first, false);
				  }
			      }
			  }

			if(dn == 1 || dn == -1)
			  tdm++;
		    
			if(dm == 1 || dm == -1)
			  tdn++;

			/*if(direction == MM)
			  {
			  dn = 1;
			  dm = 1;
			  }
			  else if(direction == MP)
			  {
			  dn = 1;
			  dm = -1;
			  }
			  else if(direction == NM)
			  {
			  dn = 1;
			  dm = 1;
			  }
			  else if(direction == NP)
			  {
			  dn = -1;
			  dm = 1;
			  }

			  if(walker.index.getl() % 2 == 1)
			  {
			  dn--;
			  dm--;
			  }*/

			//Check to see if there is a hole to hop down into
			q = walker.index + Index(tdn, tdm, -1);
			if(depositGrid.isAvailable(q))
			  {
			    std::pair<Walker &, bool> probe = walkerGrid.get(q);

			    if(probe.second == false)
			      {
				Dtot *= ratePack.Ddown;
		      
				evmap.insert(EventChoiceMap::value_type(Dtot, Index(tdn, tdm, -1)));
			      }
			    else
			      {
				if(resample)
				  {
				    if(probe.first.event != events.end())
				      events.erase(probe.first.event);
				  
				    findR(probe.first, false);
				  }
			      }
			  }
		      }
		  }
		}
	    }
	}
      //If the walker is not touching a deposit, propagates and jumps down are possible
      else// if(depositCount == 0)
	{
	  int dm = 0;
	  for(int dn = -1; dn <= 1; dn += 2)
	    {
	      double Dtot = 1.0;

	      // Check if there is support for a horizontal propagate
	      Index q = walker.index + Index(dn, dm, 0);
	      //std::cout << " Trying hop with " << q - walker.index << " to " << q << std::endl;
	      if(depositGrid.isAvailable(q))
		{
		  std::pair<Walker &, bool> probe = walkerGrid.get(q);

		  if(probe.second == false)
		    {
		      Dtot = ratePack.D;

		      evmap.insert(EventChoiceMap::value_type(Dtot, Index(dn, dm, 0)));
		    }
		  else
		    {
		      if(resample)
			{
			  if(probe.first.event != events.end())
			    events.erase(probe.first.event);
				  
			  findR(probe.first, false);
			}
		    }
		}
	      // Check to see if there is a hole to hop down into
	      else 
		{
		  int tdn = (dn == 1) ? 2 : -1,
		    tdm = dm;

		  if(walker.index.getl() % 2 == 1)
		    {
		      tdn--;
		      tdm--;
		    }

		  q = walker.index + Index(tdn, tdm, -1);
		  //std::cout << " Trying thop with " << q - walker.index << " to " << q << std::endl;
		  if(depositGrid.isAvailable(q))
		    {
		      std::pair<Walker &, bool> probe = walkerGrid.get(q);

		      if(probe.second == false)
			{
			  Dtot = ratePack.Ddown;
			    
			  evmap.insert(EventChoiceMap::value_type(Dtot, Index(tdn, tdm, -1)));
			}
		      else
			{
			  if(resample)
			    {
			      if(probe.first.event != events.end())
				events.erase(probe.first.event);
				  
			      findR(probe.first, false);
			    }
			}
		    }

		  tdm++;
		  q = walker.index + Index(tdn, tdm, -1);
		  //std::cout << " Trying hop with " << q - walker.index << " to " << q << std::endl;
		  if(depositGrid.isAvailable(q))
		    {
		      std::pair<Walker &, bool> probe = walkerGrid.get(q);

		      if(probe.second == false)
			{
			  Dtot = ratePack.Ddown;
			    
			  evmap.insert(EventChoiceMap::value_type(Dtot, Index(tdn, tdm, -1)));
			}
		      else
			{
			  if(resample)
			    {
			      if(probe.first.event != events.end())
				events.erase(probe.first.event);
				  
			      findR(probe.first, false);
			    }
			}
		    }
		}
	    }

	  int dn = 0;
	  for(int dm = -1; dm <= 1; dm += 2)
	    {
	      double Dtot = 1.0;

	      //Check if there is support for a horizontal propagate
	      Index q = walker.index + Index(dn, dm, 0);
	      //std::cout << " Trying hop with " << q - walker.index << " to " << q << std::endl;
	      if(depositGrid.isAvailable(q))
		{
		  std::pair<Walker &, bool> probe = walkerGrid.get(q);

		  if(probe.second == false)
		    {
		      Dtot = ratePack.D;
		  
		      evmap.insert(EventChoiceMap::value_type(Dtot, Index(dn, dm, 0)));
		    }
		  else
		    {
		      if(resample)
			{
			  if(probe.first.event != events.end())
			    events.erase(probe.first.event);
				  
			  findR(probe.first, false);
			}
		    }
		}
	      // Check to see if there is a hole to hop down into
	      else 
		{
		  int tdn = dn,
		    tdm = (dm == 1) ? 2 : -1;

		  if(walker.index.getl() % 2 == 1)
		    {
		      tdn--;
		      tdm--;
		    }

		  q = walker.index + Index(tdn, tdm, -1);
		  //std::cout << " Trying hop with " << q - walker.index << " to " << q << std::endl;
		  if(depositGrid.isAvailable(q))
		    {
		      std::pair<Walker &, bool> probe = walkerGrid.get(q);

		      if(probe.second == false)
			{
			  Dtot = ratePack.Ddown;
		      
			  evmap.insert(EventChoiceMap::value_type(Dtot, Index(tdn, tdm, -1)));
			}
		      else
			{
			  if(resample)
			    {
			      if(probe.first.event != events.end())
				events.erase(probe.first.event);
				  
			      findR(probe.first, false);
			    }
			}
		    }

		  tdn++;
		  q = walker.index + Index(tdn, tdm, -1);
		  //std::cout << " Trying hop with " << q - walker.index << " to " << q << std::endl;
		  if(depositGrid.isAvailable(q))
		    {
		      std::pair<Walker &, bool> probe = walkerGrid.get(q);

		      if(probe.second == false)
			{
			  Dtot = ratePack.Ddown;
		      
			  evmap.insert(EventChoiceMap::value_type(Dtot, Index(tdn, tdm, -1)));
			}
		      else
			{
			  if(resample)
			    {
			      if(probe.first.event != events.end())
				events.erase(probe.first.event);
				  
			      findR(probe.first, false);
			    }
			}
		    }
		}
	    }
	}

      // We deposited
      if(depositCount > 1/* || (depositCount == 1 && Dedge == 0.0)*/)
	{
	  walker.edgeR = 0;
	  walker.r = 0;

	  if(DEBUG) std::cout << "findR: Deposit detected for Walker " << walker << "\n";

	  deposit(walker);
	  if(walker.event != events.end())
	    events.erase(walker.event);

	  walker.event = events.insert(events.begin(), std::pair<double, std::function<void ()> >(0.0, std::function<void ()>(std::bind(&KMC::eraseWalker, std::ref(*this), std::ref(walker)))));

	  walker.deposited = true;
	}
      else if(edgePropagate)
	{
	  walker.event = events.insert(Event(walker.exitTime, edgeFunction));

	  if(DEBUG)
	    std::cout << "edgePropagate selected" << std::endl;
	}
      // It is time to select an event from the generated list
      else
	{
	  walker.edgeR = 0;
	  walker.r = 0;

	  if(walker.event != events.end())
	    events.erase(walker.event);

	  double sum = 0.0;
	  for(EventChoiceMap::iterator it = evmap.begin(); it != evmap.end(); it++)
	    {
	      if(DEBUG)
		{
		  std::cout << " Considering hop event with propensity: " << it->first << " to " << it->second << std::endl;
		}

	      sum += (*it).first;
	    }

	  double rn = rng.getDouble();
	  double dt = -log(rn) / sum;

	  if(sum == 0.0)
	    {
	      walker.event = events.end();
	      if(DEBUG)
		std::cout << RMAX << " findR: Creating null-event walker " << walker << std::endl;
	    }
	  else
	    {
	      walker.exitTime = std::max(lastDiffTime, lastRxnTime) + dt;

	      EventChoiceMap::iterator it = select(rng.getDouble(), evmap);

	      walker.event = events.insert(Event(walker.exitTime, std::function<void ()>(std::bind(&KMC::hop, std::ref(*this), std::ref(walker), it->second))));

	      if(DEBUG)
		std::cout << "findR: Selected event with exitTime = " << std::max(lastDiffTime, lastRxnTime) + dt << std::endl;
	    }
	}
    }
  //If we are not touching another walker or deposit, and we successfully chose an r >= 1, we must be free to propagate
  else
    {
      walker.edgeR = 0;

      double r = rng.getDouble();

      std::map< Type, CDF<Square> >::iterator it = cdfs.find(walker.type);

      ASSERT(it != cdfs.end(), "Trying to propagate a walker which does not have a CDF");

      CDF<Square> &cdf = it->second;

      double dt = cdf.computeFirstExit(walker.r, r);

      ASSERT(walker.r != 0, "walker.r should not be zero");
	    
      if(DEBUG) std::cout << "findFPTs: Walker " << walker;
      
      walker.exitTime = walker.t + dt;

      //std::max(lastDiffTime, lastRxnTime)
	    
      if(walker.event != events.end())
	events.erase(walker.event);       
	    
      walker.event = events.insert(Event(walker.exitTime, std::function<void ()>(std::bind(&KMC::propagate, std::ref(*this), std::ref(walker)))));
    }
}
