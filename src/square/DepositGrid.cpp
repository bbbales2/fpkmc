#include "DepositGrid.hpp"

template<>
typename DepositGrid<Square>::Distance DepositGrid<Square>::rebuildEdge(Layer<Distance> &dLayer, Index index){
  int n = index.getn(),
    m = index.getm(),
    l = index.getl();

  Distance distance(0, 0);

  bool supported = true;

  int oo;

  if(l % 2 == 1)
    oo = 0;
  else
    oo = -1;
    
  int r = 0;
  for(r = 0; r <= RMAX && supported; )
    {
      int w = -r + oo;
      for(int h = -r + oo; h <= r + oo; h++)
	{
	  Distance distance = dLayer(adjust(n + h, N), adjust(m + w, N));
	  if(distance.dn != 0 || distance.dm != 0)
	    {
	      supported = false;
	    }
	}

      int h = r + oo + 1;
      for(int w = -r + oo; w <= r + oo; w++)
	{
	  Distance distance = dLayer(adjust(n + h, N), adjust(m + w, N));
	  if(distance.dn != 0 || distance.dm != 0)
	    {
	      supported = false;
	    }
	}

      w = r + oo + 1;
      for(int h = r + oo + 1; h > -r + oo; h--)
	{
	  Distance distance = dLayer(adjust(n + h, N), adjust(m + w, N));
	  if(distance.dn != 0 || distance.dm != 0)
	    {
	      supported = false;
	    }
	}

      h = -r + oo;
      for(int w = r + oo + 1; w > -r + oo; w--)
	{
	  Distance distance = dLayer(adjust(n + h, N), adjust(m + w, N));
	  if(distance.dn != 0 || distance.dm != 0)
	    {
	      supported = false;
	    }
	}

      if(supported)
	r++;
    }

  return Distance(r, 0);
}

template<>
const std::set<Index> &DepositGrid<Square>::getFloorStencil(Index index)
{
  if(index.getl() % 2 == 0)
    {
      static std::set<Index> set =
	{
	  Index(0, 0, 0),
	  Index(-1, 0, 0),
	  Index(0, -1, 0),
	  Index(-1, -1, 0)
	};

      return set;
    }
  else
    {
      static std::set<Index> set =
	{
	  Index(0, 0, 0),
	  Index(1, 0, 0),
	  Index(0, 1, 0),
	  Index(1, 1, 0)
	};

      return set;
    }
}

template<>
bool DepositGrid<Square>::isAvailable(Index index)
{
  if(index.getl() < 0)
    return false;

  Distance distance = getDistanceToEdge(index - Index(0, 0, 1));

  if(distance.dn == 0 && distance.dm == 0)
    {
      return false;
    }
  else
    {
      distance = getDistanceToDeposit(index);
	
      if(distance.dn == 0 && distance.dm == 0)
	{
	  return false;
	}
      else
	{
	  return true;
	}
    }
}
