#include "WalkerGrid.hpp"

template<>
const std::set<Index> &WalkerGrid<Square>::getStencil(Index index)
{
  if(index.getl() % 2 == 0)
    {
      static std::set<Index> set =
	{
	  Index(-1, -1, 0),
	  Index(-1, 0, 0),
	  Index(-1, 1, 0),

	  Index(0, -1, 0),
	  Index(0, 1, 0),

	  Index(1, -1, 0),
	  Index(1, 0, 0),
	  Index(1, 1, 0),

	  Index(0, -1, -1),
	  Index(1, -1, -1),
	  Index(2, 0, -1),
	  Index(2, 1, -1),
	  Index(1, 2, -1),
	  Index(0, 2, -1),
	  Index(-1, 1, -1),
	  Index(-1, 0, -1),

	  Index(0, -1, 1),
	  Index(1, -1, 1),
	  Index(2, 0, 1),
	  Index(2, 1, 1),
	  Index(1, 2, 1),
	  Index(0, 2, 1),
	  Index(-1, 1, 1),
	  Index(-1, 0, 1)
	};

      return set;
    }
  else
    {
      static std::set<Index> set = 
	{
	  Index(-1, -1, 0),
	  Index(-1, 0, 0),
	  Index(-1, 1, 0),

	  Index(0, -1, 0),
	  Index(0, 1, 0),

	  Index(1, -1, 0),
	  Index(1, 0, 0),
	  Index(1, 1, 0),

	  Index(-1, -2, -1),
	  Index(0, -2, -1),
	  Index(1, -1, -1),
	  Index(1, 0, -1),
	  Index(0, 1, -1),
	  Index(-1, 1, -1),
	  Index(-2, 0, -1),
	  Index(-2, -1, -1),

	  Index(-1, -2, 1),
	  Index(0, -2, 1),
	  Index(1, -1, 1),
	  Index(1, 0, 1),
	  Index(0, 1, 1),
	  Index(-1, 1, 1),
	  Index(-2, 0, 1),
	  Index(-2, -1, 1)
	};

      return set;
    }
}
