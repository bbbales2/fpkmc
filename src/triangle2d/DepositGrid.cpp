#include "DepositGrid.hpp"

static inline int elements_on_level(int r)
{
  return r / 2 + 1;
}

template<>
const std::set<Index> &DepositGrid<Triangle2D>::getFloorStencil(Index index)
{
  static std::set<Index> iset = {};

  return iset;
}

template<>
typename DepositGrid<Triangle2D>::Distance DepositGrid<Triangle2D>::rebuildEdge(Layer<Distance> &dLayer, Index index)
{
  return Distance(RMAX, RMAX);
}

template<>
bool DepositGrid<Triangle2D>::isAvailable(Index index)
{
  Distance distance = getDistanceToEdge(index - Index(0, 0, 1));
    
  if(index.getl() != 0)
    {
      return false;
    }
  else
    {
      distance = getDistanceToDeposit(index);
      
      if(distance.dn == 0 && distance.dm == 0)
	{
	  return false;
	}
      else
	{
	  return true;
	}
    }
}
