#include <stdlib.h>
#include <stdio.h>
//#include <malloc.h>
#include <thread>
#include <fstream>
//#include <omp.h>
#include <tclap/CmdLine.h>

#include "util.hpp"
#include "defines.hpp"

#include "KMC.hpp"

#define GridT Triangle2D

typedef KMC<GridT> KMCT;

int main(int argc, char **argv)
{
  int R = 1, P = 1, N = 16, s = 0, threads = 8, RMAX = 7;

  double DoverF;    // Diffusion rate vs flux constant (actually: (D/a^2)/(F*a^2) )
  double DedgeoverF;    // Diffusion rate vs flux constant (actually: (D/a^2)/(F*a^2) )

  try
    {
      TCLAP::CmdLine cmd("KMC code", ' ', "0.0");

      TCLAP::ValueArg<int> RArg("r", "runs", "The number of runs in the simulation", false, 1, "unsigned int");
      cmd.add(RArg);

      TCLAP::ValueArg<int> PArg("p", "print", "The number of runs between printouts (not strictly followed -- depends on threading)", false, 1, "unsigned int");
      cmd.add(PArg);

      TCLAP::ValueArg<int> NArg("n", "grid", "The dimension of the grid (this must be a multiple of BLOCKSIZE (defined in WalkerGrid.hpp))", false, 16, "unsigned int");
      cmd.add(NArg);

      TCLAP::ValueArg<double> DoFArg("d", "dof", "D over F (D == 1)", false, 1.0e6, "double");
      cmd.add(DoFArg);

      TCLAP::ValueArg<double> DedgeoverFArg("e", "dedge", "Dedge over F", false, 1.0e3, "double");
      cmd.add(DedgeoverFArg);

      TCLAP::ValueArg<int> seedArg("s", "seed", "Seed offset. Runs are seeded as seed offset + run number", false, 0, "int");
      cmd.add(seedArg);

      TCLAP::ValueArg<int> threadsArg("t", "threads", "Number of threads to use in computation (this may effect printout numbers)", false, 1, "int");
      cmd.add(threadsArg);

      TCLAP::ValueArg<int> RMAXArg("m", "RMAX", "The maximum radius of the walkers", false, 7, "int");
      cmd.add(RMAXArg);

      // Parse the argv array.
      cmd.parse( argc, argv );

      R = RArg.getValue();
      P = PArg.getValue();
      N = NArg.getValue();
      DoverF = DoFArg.getValue();
      DedgeoverF = DedgeoverFArg.getValue();
      s = seedArg.getValue();
      threads = threadsArg.getValue();
      RMAX = RMAXArg.getValue();

      std::cout << "Executing with R = " << R << std::endl << " P = "  << P << std::endl << " N = "  << N << std::endl << " DoverF = "  << DoverF << std::endl << " DedgeoverF = "  << DedgeoverF << std::endl << " s = "  << s << std::endl << " threads = "  << threads << std::endl << " RMAX = " << RMAX << std::endl;
    }
  catch (TCLAP::ArgException &e)  // catch any exceptions
    {
      std::cerr << "error with input arguments: " << e.error() << " for arg " << e.argId() << std::endl;
      return -1;
    }

  ASSERT(DoverF != 0.0, "DoverF cannot be equal to zero");

  const double a = 1.0;           // lattice constant
  const double D = 1.0;           // Surface Diffusion rate
  double F = (D / DoverF);        // rate of influx of walkers onto surface (per lattice square per second)
  double Dedge = (F * DedgeoverF);
  double Dbreak = 0.0 * D;
  double Dup = 0.0;
  double Ddown = 0.0;
  double Dcorner = Dedge / 100.0;

  double C = 0.1;

 // double initTime = omp_get_wtime();

  KMCT *kmc = (KMCT *)calloc(threads, sizeof(KMCT));

  for(int t = 0; t < threads; t++)
    {
      new (kmc + t) KMCT(N, a, F, Dedge, Dbreak, Dup, Ddown, Dcorner, D, C, RMAX, t * (R / threads) + s, false);
    }

  //initTime = omp_get_wtime() - initTime;

  double outTime = 0.0;
 // double time = omp_get_wtime();
  std::vector<std::thread> thread_handles;
  for(int r = 0; r < R; )
    {
      char filename[500];
      
      for(int t = 0; t < threads && r + t < R; t++)
	{
	  thread_handles.push_back(std::thread(std::bind(&KMCT::run, std::ref(kmc[t]), 1)));
	}

      for(int t = 0; t < threads && r + t < R; t++)
	{
	  thread_handles[t].join();
	}

      thread_handles.clear();
      
      r += threads;

      //Every so often, we need to print output files
      if((r % P < threads && (r - threads) % P >= (P - threads)) || r >= R)
	{
	 // double tmp = omp_get_wtime();

	  printf("%d/%d\n", r, R);

	  DepositCounter<GridT> depositCounter;

	  for(int t = 0; t < threads; t++)
	    {
	      depositCounter.accumulate(kmc[t].depositCounter);
	    }

	  sprintf(filename, "output.%d.%d.%d.%d.txt", N, s, r, RMAX);

	  std::fstream stream(filename, std::fstream::out | std::fstream::trunc);
	  stream << depositCounter;
	  stream.close();

	 // outTime += omp_get_wtime() - tmp;
	}
    }
  //time = omp_get_wtime() - time;

 // printf("Init took %fs\nRun took %fs\nPrint out took %fs\n", initTime, time, outTime);

  for(int t = 0; t < threads; t++)
    {
      (kmc + t)->~KMC();
    }

  free(kmc);
}
