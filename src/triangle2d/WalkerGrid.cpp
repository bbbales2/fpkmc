#include "WalkerGrid.hpp"

template<>
const std::set<Index> &WalkerGrid<Triangle2D>::getStencil(Index index)
{
  static std::set<Index> set =
    {
      Index(-1, 0, 0),
      Index(-1, 1, 0),
      Index(0, 1, 0),
      Index(1, 0, 0),
      Index(1, -1, 0),
      Index(0, -1, 0)
    };

  return set;
}
