#!/usr/bin/env python
import sys, math, re
import numpy
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.axes as axes

if len(sys.argv) != 3:
    print "Arguments: ./convertToReal.py input.txt output.txt"
    exit()

try:
    filein = open(sys.argv[1], 'r')
except IOError as e:
    print "Problem opening '" + sys.argv[1] + "'"
    exit()

deposits = []
line = filein.readline()

try:
    N = int(line.split()[0])
    grid = line.split()[1]
except Exception as e:
    print "Problem parsing header"
    exit()

chemicals = ['H', 'O', 'N', 'C', 'S', 'P']

associationToChemicalMap = {}
sizes = {}

try:
    for line in filein.readlines():
        sline = line.split()
        
        if(len(sline) != 6):
            raise Exception
        
        [n, m, l, a, t, c] = [int(sline[0]), int(sline[1]), int(sline[2]), int(sline[3]), sline[4], int(sline[5])]

        if not a in associationToChemicalMap:
            sizes[a] = 1

            associationToChemicalMap[a] = chemicals[a % len(chemicals)]
        else:
            sizes[a] = sizes[a] + 1

        deposits.append([n, m, l, a, t, c]);
except Exception as e:
    print "Problem parsing file body"
    exit()

filein.close()

try:
    fileout = open(sys.argv[2], 'w')
except IOError as e:
    print "Problem opening '" + sys.argv[2] + "'"
    exit()

fileout.write("{0}\n".format(len(deposits)))
fileout.write('Something\n')

max_height = 0

for e in deposits:
    [xoff, yoff] = [0, 0] if (e[2] % 2 == 0) else [-1.0 / 2.0, -(1 - 1.0 / math.sqrt(3.0))]
  
    x = ((xoff + e[0] * 1.0 / 2.0 + e[1])% N)
    y = (yoff + e[0] * math.sqrt(3.0) / 2.0)
    z = (e[2] * math.sqrt(6) / 3)
    max_height = max(e[2], max_height)



    fileout.write("{3} {0} {1} {2}\n".format(x, y, z, associationToChemicalMap[e[3]]))

fileout.close()

