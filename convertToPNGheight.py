#!/usr/bin/env python
import sys, math, re, os,numpy, select, subprocess, StringIO, tempfile 
import matplotlib.pyplot, matplotlib.colors, matplotlib.cm

def printTacHeader(N):
    output = StringIO.StringIO()

    output.write(
        """
BEGIN_SCENE
	RESOLUTION 1024 768
CAMERA
	ZOOM 0.8
	ASPECTRATIO 1.0
	ANTIALIASING 0
	RAYDEPTH 6
	CENTER {3} -{10} {4}
	VIEWDIR 0.0 0.0 -1
	UPDIR 0.0 -1.0 0.0
END_CAMERA
BACKGROUND 0.5 0.5 0.5
LIGHT
	CENTER 0 -{4} {6}
	RAD 1.0
	COLOR 1.0 1.0 1.0
LIGHT
    CENTER {4} 0 {6}
    RAD 1.0
    COLOR 1.0 1.0 1.0
""".format(0.3,
           0.5,
           0.2,
           N / 2,
           2 * N / 2,
           N,
           N + 1,
           N * math.sqrt(3) / 2,
           N * math.sqrt(3) / 2 + 1,
           N / 4,
           N * math.sqrt(3) / 4))


    return output.getvalue()

associations = dict()
    
def convertLinesToTac(N, text, maxL):
    deposits = []
    maxL = 2*maxL
    cMaps = matplotlib.pyplot.get_cmap('Spectral')
    cNorm = matplotlib.colors.Normalize(vmin=0, vmax=maxL)
    scalarMap = matplotlib.cm.ScalarMappable(norm=cNorm, cmap=cMaps)
    print scalarMap.get_clim()

    output = StringIO.StringIO()

    try:
        for line in text.split('\n'):
            sline = line.split()
        
            if(len(sline) != 6):
                continue
        
            [n, m, l, a, t, c] = [int(sline[0]), int(sline[1]), int(sline[2]), int(sline[3]), sline[4], int(sline[5])]

            if a in associations:
                pass
            else:
                associations[a] = len(associations)

            deposits.append([n, m, l, associations[a], t, c]);
    except:
        print "error parsing file"
        raise


    max_height = 0
    ii = 0
    for e in deposits:
        ii = ii+1
        [xoff, yoff] = [0, 0] if (e[2] % 2 == 0) else [-0.5000, 0.2887]
        
        x = ((xoff + e[0] + e[1] * 1.0 / 2.0) % N)
        y = (yoff + -e[1] * math.sqrt(3) / 2)
        z = (e[2] * math.sqrt(6) / 3)
        max_height = max(e[2], max_height)
	colorValue = scalarMap.to_rgba(e[2])

        output.write(
	"""
SPHERE CENTER {xcoord:8.3f} {ycoord:8.3f} {zcoord:8.3f}
    RAD {radius}
    Texture Ambient {ambient} Diffuse {diffuse} Specular {specular} Opacity {opacity}
	Phong Plastic {plastic} Phong_size {phong_size}
	Color {colorR} {colorG} {colorB}
	TexFunc {texf}"""
    .format(xcoord=x, ycoord=y, zcoord=z, radius=0.5, ambient=0.3, diffuse=0.8, 
	specular=0.0, opacity=1.0, plastic=0.8, phong_size=45,
	colorR=colorValue[0], colorG=colorValue[1], colorB=colorValue[2], texf=0))

    return output.getvalue()

def findMaxL(frames):
    maxL = 0;
    f = open('frame.{0}.inc'.format(frames-1), 'r')
    fcontent = f.readlines()
    for line in fcontent:
	sline = line.split()
	maxL = max(maxL, int(sline[2]))
    return maxL;


if None:
#select.select([sys.stdin,], [], [], 0.0)[0]:
#    if len(sys.argv) < 2:
#        raise Exception("Not enough arguments. Need ./convertToPNG.py N")

#    N = int(sys.argv[1])

#    sys.stdout.write(printTacHeader(N) + convertLinesToTac(N, sys.stdin.read()))
    pass
else:
    try:
        file = open('N', 'r')
        N = int(file.read())
        file.close()
    except:
        N = 128
        pass

    file = open('R', 'r')
    R = int(file.read())
    file.close()
    
    file = open('frames', 'r')
    frames = int(file.read())
    file.close()

    for r in range(0, R):
        os.chdir('run.{0}'.format(r))
	maxL = findMaxL(frames)
        renderString = printTacHeader(N)
        for f in range(0, frames):
	    print('frame {0}'.format(f))
            fhandle = open('frame.{0}.inc'.format(f), 'r')
            renderString = renderString + convertLinesToTac(N, fhandle.read(),maxL)
            fhandle.close()
            
            [fnumber, fname] = tempfile.mkstemp()
            fhandle = open(fname, 'w')
            fhandle.write(renderString)
            fhandle.close()
        
            subprocess.call('tachyon -format PNG {0} -o height.{1}.png'.format(fname, f).split(), stdin = subprocess.PIPE, stdout = subprocess.PIPE)

            os.remove(fname)

        os.chdir('../')
