#!/usr/bin/python

import sys, os

if len(sys.argv) != 4:
    print("Arguments: ./buildSteps.py NX NDX output.dat")
    exit()
N = int(sys.argv[1])
DX = int(sys.argv[2])

try:
    fileout = open(sys.argv[3], 'w')
except IOError as e:
    print("Problem opening '" + sys.argv[4] + "'")
    exit()

fileout.write("{0:d} triangle\n".format(N))

yStep = N
k = 0
while yStep>k:
    for j in range(k,yStep):
	for i in range(N):
	    fileout.write("{0:d} {1:d} {2:d} {3:d} Ag 1\n".format(i,j,k,k))
    
    yStep -= DX
    k += 1

    
fileout.close()

